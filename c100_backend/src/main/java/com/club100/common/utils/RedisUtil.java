package com.club100.common.utils;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @auther: zhangqiang
 * @date: 2018/9/10 16:06
 * @description:
 */
@Component
public class RedisUtil {
  @Resource private StringRedisTemplate stringRedisTemplate;

  public String getString(String key) {
    return stringRedisTemplate.opsForValue().get(key);
  }

  public void setString(String key, String value, Long expire) {
    stringRedisTemplate.opsForValue().set(key, value, expire, TimeUnit.MINUTES);
  }

  public void setString(String key, String value, Long expire, TimeUnit timeUnit) {
    stringRedisTemplate.opsForValue().set(key, value, expire, timeUnit);
  }

  public void setString(String key, String value) {
    stringRedisTemplate.opsForValue().set(key, value);
  }

  /**
   * 入队列
   *
   * @param key
   * @param value
   */
  public void lPush(String key, String value) {
    stringRedisTemplate.opsForList().leftPush(key, value);
  }

  /**
   * 出队列(先进先出)
   *
   * @param key
   * @return
   */
  public String rPop(String key) {
    return stringRedisTemplate.opsForList().rightPop(key);
  }

  public void hSet(String h, String hKey, String hVal) {
    stringRedisTemplate.opsForHash().put(h, hKey, hVal);
  }

  public void convertAndSend(String channel, Object message) {
    stringRedisTemplate.convertAndSend(channel, message);
  }

  public int incr(String key, int liveTime) {
    RedisAtomicInteger entityIdCounter =
        new RedisAtomicInteger(key, stringRedisTemplate.getConnectionFactory());

    long expire = entityIdCounter.getExpire();
    // 初始设置过期时间
    if (expire < 0) {
      boolean setExpire = entityIdCounter.expire(liveTime, TimeUnit.SECONDS);
      System.out.println(setExpire + "过期时间为空，重新设置为" + liveTime + "秒");
    }
    Integer increment = entityIdCounter.addAndGet(1);
    return increment;
  }

  public StringRedisTemplate getStringRedisTemplate() {
    return this.stringRedisTemplate;
  }
}

package com.club100.common.constant;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/06 9:28 PM
 * @Description: 小程序支付常量。
 * @Version: 1.0
 */
public class WeChatMiniConstants {

    /**
     * 商户号 ID。
     */
    public static final String MERCHANT_ID = "1601325618";

    /**
     * 商户 API 密钥。
     */
    public static final String MERCHANT_KEY = "TAy8rytuN9IKKjDkP0MgityKn4ZX8fqu";

    /**
     * 商户 APIv3 密钥。
     */
    public static final String MERCHANT_KEY_V3 = "1bfca39a7123435ab5d2e9eedc9f1532";

    /**
     * 小程序 APP_ID。
     */
    public static final String MINI_APP_ID = "wxad24a51a97d67b92";

    /**
     * 小程序 APP_SECRET。
     */
    public static final String MINI_APP_SECRET = "08ab3c84f258f787d0302c6194b0850e";

}

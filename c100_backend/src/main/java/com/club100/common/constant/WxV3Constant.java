package com.club100.common.constant;

public class WxV3Constant {

  // 商户id
  public final static String MCH_ID = "1603269722";

  //ApiV3密钥
  public final static String ApiV3 = "d03548899f044c3fbdf4a67c3c39b15d";

  //APi密钥
  public final static String KeyPath = "/Users/hujiwei/Desktop/keystore/apiclient_key.pem";
  //Api证书路径
  public final static String CertPath = "/Users/hujiwei/Desktop/keystore/apiclient_cert.pem";
  //平台证书路径
  public final static String PlatCertPath = "/Users/hujiwei/Desktop/keystore/wx_platform_cert.pem";
}

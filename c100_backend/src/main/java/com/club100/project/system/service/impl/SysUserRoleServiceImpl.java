package com.club100.project.system.service.impl;

import com.club100.project.system.domain.SysUserRole;
import com.club100.project.system.mapper.SysUserRoleMapper;
import com.club100.project.system.service.ISysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/3/4 1:02 PM
 * @Description:
 * @Version: 1.0
 */
@Service
@Slf4j
public class SysUserRoleServiceImpl implements ISysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public int insertSysUserRole(SysUserRole sysUserRole) {
        return sysUserRoleMapper.insertSysUserRole(sysUserRole);
    }

}

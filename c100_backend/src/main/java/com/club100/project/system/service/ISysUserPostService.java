package com.club100.project.system.service;

import com.club100.project.system.domain.SysUserPost;

public interface ISysUserPostService {

    /**
     * 新增用户岗位信息
     *
     * @param sysUserPost
     * @return 结果
     */
    public int insertSysUserPost(SysUserPost sysUserPost);

}

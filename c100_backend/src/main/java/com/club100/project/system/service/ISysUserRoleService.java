package com.club100.project.system.service;

import com.club100.project.system.domain.SysUserRole;

public interface ISysUserRoleService {

    /**
     * 新增用户角色信息
     *
     * @param sysUserRole
     * @return 结果
     */
    public int insertSysUserRole(SysUserRole sysUserRole);

}

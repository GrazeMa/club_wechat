package com.club100.project.system.service.impl;

import com.club100.project.system.domain.SysUserPost;
import com.club100.project.system.mapper.SysUserPostMapper;
import com.club100.project.system.service.ISysUserPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/3/4 1:12 PM
 * @Description:
 * @Version: 1.0
 */
@Service
@Slf4j
public class SysUserPostServiceImpl implements ISysUserPostService {

    @Autowired
    private SysUserPostMapper sysUserPostMapper;

    @Override
    public int insertSysUserPost(SysUserPost sysUserPost) {
        return sysUserPostMapper.insertSysUserPost(sysUserPost);
    }

}

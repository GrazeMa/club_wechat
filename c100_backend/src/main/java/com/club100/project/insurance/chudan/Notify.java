package com.club100.project.insurance.chudan;

import com.club100.project.insurance.notify.CancelNotify;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notify implements Serializable {

  private int notifyType; //通知类型 出单：3
  private String sign; //签名，MD5（key+data）
  private CancelNotify IssueNotify; //出单通知信息
}

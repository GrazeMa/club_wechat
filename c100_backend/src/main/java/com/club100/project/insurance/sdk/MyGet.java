package com.club100.project.insurance.sdk;

import com.club100.project.wechat.utils.MyOkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * @description: 重写post
 * @author: Mr.Wang
 * @create: 2020-07-31 11:13
 **/
public class MyGet {

  public static String getBodyWithHeader(String url, String accessToken)  {
    try {
      Request request = (new Request.Builder()).url(url).get().addHeader("access-token", accessToken).build();

      Response response = MyOkHttpClient.client.newCall(request).execute();
      if (!response.isSuccessful()) {
      } else {
        return response.body().string();
      }
    } catch (IOException var7) {
      var7.printStackTrace();

    }
    return "";
  }
}

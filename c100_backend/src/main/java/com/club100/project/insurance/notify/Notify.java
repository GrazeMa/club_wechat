package com.club100.project.insurance.notify;

import com.club100.project.insurance.domain.PayNotify;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notify implements Serializable {

  int notifyType; //通知类型 退保：4 退保重出：5、部分被保人退保:6
  String sign; //签名，MD5（key+data）
  CancelNotify data; //退保/退保重出通知信息
}

package com.club100.project.insurance.sdk;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.club100.project.insurance.domain.*;
import com.club100.project.wechat.utils.OrderCoderUtil;

import java.util.Arrays;

public class ProjectSiteDataTest {

  public static void main(String[] args) throws Exception {
    //
//    new ProjectSiteDataTest().syncTmfProjectData();
//    new ProjectSiteDataTest().test1();
//    new ProjectSiteDataTest().test2();
//    new ProjectSiteDataTest().test4();
//    new ProjectSiteDataTest().test5();
    new ProjectSiteDataTest().test6();

  }

  public void syncTmfProjectData() throws Exception {
    String api = "simpleTrial";
    SimpleTrialReq simpleTrialReq = getRequestBody();
    String jsonStr = JSON.toJSONString(simpleTrialReq);

    doPostWithSign(api,jsonStr);
  }

  public SimpleTrialReq getRequestBody(){
    SimpleTrialReq simpleTrialReq = new SimpleTrialReq();
    simpleTrialReq.setTransNo("huize20171027100140000001");
    simpleTrialReq.setCaseCode("QX000000129531");
    simpleTrialReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    simpleTrialReq.setStartDate("2020-11-01");
    simpleTrialReq.setEndDate("2020-11-10");
    simpleTrialReq.setBirthday("1990-12-16");
    return simpleTrialReq;
  }


  public static void test1() throws Exception {
    String api = "simpleInsure";
    InsureReq insureReq = getInsureBody();
    String jsonStr = JSON.toJSONString(insureReq);

    doPostWithSign(api,jsonStr);
  }

  public static InsureReq getInsureBody(){
    InsureReq insureReq = new InsureReq();
    insureReq.setTransNo("huize20171027100140000003");
    insureReq.setCaseCode("QX000000129531");
    insureReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    insureReq.setStartDate("2020-11-01");
    insureReq.setEndDate("2020-11-10");

    Applicant applicant = new Applicant();
    applicant.setCName("胡继伟");
    applicant.setCardType(14);
    applicant.setCardCode("91310106MA1FYLFM0B");
    applicant.setMobile("13167105382");
    applicant.setEmail("hujiwei@yaoyanshe.com");
    applicant.setApplicantType(1);

    Insurant insurant = new Insurant();
    insurant.setInsurantId(100);
    insurant.setCName("胡继伟");
    insurant.setCardType(1);
    insurant.setCardCode("410381199012166617");
    insurant.setBirthday("1990-12-16");
    insurant.setSex(1);
    insurant.setSinglePrice(8900);
    insurant.setCount(1);
    //其他
    insurant.setRelationId(23);

    insureReq.setApplicant(applicant);
    insureReq.setInsurants(Arrays.asList(insurant));
    return insureReq;
  }

  public static void test2() throws Exception {
    String api = "onlinePay";
    OnlinePayReq onlinePayReq = getOnlinePayBody();
    String jsonStr = JSON.toJSONString(onlinePayReq);

    doPostWithSign(api,jsonStr);
  }


  public static OnlinePayReq getOnlinePayBody(){
    OnlinePayReq onlinePayReq = new OnlinePayReq();
    onlinePayReq.setTransNo("huize20171027100140000003");
    onlinePayReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    onlinePayReq.setInsureNums("20201001016721");
    onlinePayReq.setMoney(1);
    onlinePayReq.setGateway(21);
    onlinePayReq.setClientType(1);
    onlinePayReq.setCallBackUrl("http://www.baidu.com");

    return onlinePayReq;
  }

  public static void test4() throws Exception {
    String api = "downloadUrl";
    PolicyUrlReq policyUrlReq = downloadUrl();
    String jsonStr = JSON.toJSONString(policyUrlReq);

    doPostWithSign(api,jsonStr);
  }

  public static PolicyUrlReq downloadUrl(){
    PolicyUrlReq policyUrlReq = new PolicyUrlReq();
    policyUrlReq.setTransNo("huize20171027100140000007");
    policyUrlReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    policyUrlReq.setInsureNum("20201001016721");

    return policyUrlReq;
  }

  public static void test5() throws Exception {
    String api = "surrenderPolicy";
    SurrenderPolicyReq surrenderPolicyReq = surrenderPolicy();
    String jsonStr = JSON.toJSONString(surrenderPolicyReq);

    doPostWithSign(api,jsonStr);
  }


  public static LocalPayReq localPay(){
    LocalPayReq localPayReq = new LocalPayReq();
    localPayReq.setTransNo("huize20171027100140000007");
    localPayReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    localPayReq.setInsureNums("20201001015051");
    localPayReq.setMoney(1L);

    return localPayReq;
  }

  public static void test6() throws Exception {
    String api = "localPay";
    LocalPayReq localPayReq = localPay();
    String jsonStr = JSON.toJSONString(localPayReq);

    doPostWithSign(api,jsonStr);
  }


  public static SurrenderPolicyReq surrenderPolicy(){
    SurrenderPolicyReq surrenderPolicyReq = new SurrenderPolicyReq();
    surrenderPolicyReq.setTransNo("huize20171027100140000007");
    surrenderPolicyReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    surrenderPolicyReq.setInsureNum("20201001016721");

    return surrenderPolicyReq;
  }



  public static void doPostWithSign(String api,String jsonStr) throws Exception {
    //环境地址
    String baseUrl = BXConstants.TEST_CORE + api;
    //签名key
    String key = BXConstants.TEST_SIGN_KEY;
    //签名
    String sign = InsuranceUtil.MD5(key + jsonStr);

    String url = baseUrl + "?sign=" + sign;
    System.out.println("url:" + url);
    System.out.println("jsonStr:" + jsonStr);

    String response = MyPost.postBodyWithHeader(url,jsonStr, "application/json;charset=utf-8");
    System.out.println("response:" + response);
  }

}

package com.club100.project.insurance.chudan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PolicyInfo implements Serializable {

  String  productName; //产品名称
  String  planName; //计划名称
  String  applicant; //投保人姓名
  String  insurant; //被保人姓名，多人以英文逗号“,”分隔
  String  startDate; //起保日期 格式：yyyy-MM-dd
  String  endDate; //终保日期 格式：yyyy-MM-dd
  String  policyNum; //保险公司保单
}

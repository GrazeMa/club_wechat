package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NotifyResult implements Serializable {

  private String  state;  //接收状态 true：成功 false：失败
  private String  failMsg;  //失败原因
}

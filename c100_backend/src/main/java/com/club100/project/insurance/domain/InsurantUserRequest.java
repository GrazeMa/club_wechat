package com.club100.project.insurance.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsurantUserRequest implements Serializable {

  /**
   * 活动id
   */
  @ApiModelProperty("活动id")
  private Long activityId;

  /**
   * 被保险人姓名
   */
  @ApiModelProperty("被保险人姓名")
  private String userName;

  /**
   * 性别（0：女 1：男）
   */
  @ApiModelProperty("性别（0：女 1：男）")
  private Integer userSex;

  /**
   * 出生日期
   */
  @ApiModelProperty("出生日期")
  private String birthday;

  /**
   * 证件号
   */
  @ApiModelProperty("身份证号")
  private String cardCode;

  /**
   * 手机号
   */
  @ApiModelProperty("手机号")
  private String mobile;

}

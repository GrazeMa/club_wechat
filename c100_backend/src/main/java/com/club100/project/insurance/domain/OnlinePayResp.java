package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnlinePayResp {
  //在线支付返回

  private String  transNo;    //交易流水号，生成规则参考名词解释

  private int partnerId;    //开发者身份标识，获取方式参考名词解释

  private String insureNums;    //投保单号，多个使用“,”分隔

  private String gatewayUrl;    //网关地址(PC支付为二维码地址)
}

package com.club100.project.insurance.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.club100.common.exception.CustomException;
import com.club100.common.utils.bean.BeanUtils;
import com.club100.project.insurance.domain.*;
import com.club100.project.insurance.mapper.InsuranceOrderMapper;
import com.club100.project.insurance.sdk.BXConstants;
import com.club100.project.insurance.sdk.InsuranceUtil;
import com.club100.project.insurance.sdk.MyPost;
import com.club100.project.insurance.service.InsuranceService;
import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.domain.ClubOrder;
import com.club100.project.wechat.domain.DownloadInsurance;
import com.club100.project.wechat.mapper.ActivityMapper;
import com.club100.project.wechat.mapper.ClubOrderMapper;
import com.club100.project.wechat.utils.InsuranceResponseTemplate;
import com.club100.project.wechat.utils.OrderCoderUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@Service
public class InsuranceServiceImpl implements InsuranceService {

  /**
   * 整体流程：试算→承保→在线/本地支付→保单地址→投保单查询→退保。
   * 微信支付订单成功后开始进行保单流程
   */

  @Resource
  InsuranceOrderMapper insuranceOrderMapper;
  @Resource
  ClubOrderMapper clubOrderMapper;
  @Resource
  ActivityMapper activityMapper;
  @Autowired
  private JavaMailSender javaMailSender;
  // 邮件发送人。
  @Value("${club.mail.sender}")
  private String EMAIL_FROM;
  // 邮件主题
  @Value("${club.mail.insuranceSubject}")
  private String EMAIL_SUBJECT;
  // 邮件正文
  @Value("${club.mail.text}")
  private String EMAIL_TEXT;

  @Override
  public Map<String,Long>  saveUserInfo(InsurantUserRequest insurantUserRequest) throws Exception {
    Map<String,Long> returnMap = new HashMap<>();
    InsuranceOrder insuranceOrder = insuranceOrderMapper.selectByUserNameAndCardCode(insurantUserRequest.getUserName(),insurantUserRequest.getCardCode(),insurantUserRequest.getActivityId());
    //用户信息在当前活动已经存在则直接返回
    if(insuranceOrder != null){
      returnMap.put("insuranceId", insuranceOrder.getId());
      return returnMap;
    }
    Activity activity = Optional.ofNullable(activityMapper.selectByPrimaryKey(insurantUserRequest.getActivityId())).orElseThrow(()-> new CustomException("活动id错误"));

    insuranceOrder = new InsuranceOrder();
    BeanUtils.copyProperties(insurantUserRequest,insuranceOrder);
    //默认为方案1
    insuranceOrder.setCaseCode("QX000000129529");
    insuranceOrder.setStatus(0);
    insuranceOrder.setStartDate(activity.getActivityBeginTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    insuranceOrder.setEndDate(activity.getActivityEndTime().plusDays(1L).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    insuranceOrder.setCreateTime(new Date());
    insuranceOrder.setUpdateTime(new Date());

    checkUserInfo(insuranceOrder);
    insuranceOrderMapper.insert(insuranceOrder);

    returnMap.put("insuranceId", insuranceOrder.getId());
    return returnMap;
  }




  /**
   * 校验用户入保信息是否正确
   *
   * @param insuranceOrder
   * @throws Exception
   */
  private void checkUserInfo(InsuranceOrder insuranceOrder) throws Exception {
    SimpleTrialReq simpleTrialReq = new SimpleTrialReq();
    simpleTrialReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    simpleTrialReq.setCaseCode(insuranceOrder.getCaseCode());
    simpleTrialReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    simpleTrialReq.setStartDate(insuranceOrder.getStartDate());
    simpleTrialReq.setEndDate(insuranceOrder.getEndDate());
    simpleTrialReq.setBirthday(insuranceOrder.getBirthday());
    //试算
    InsuranceResponseTemplate simpleTrialRes = simpleTrial(simpleTrialReq);
    insuranceOrder.setSinglePrice(Long.parseLong(simpleTrialRes.getData().get("singlePrice").toString()));

    //承保
    InsuranceResponseTemplate simpleInsureRes = simpleInsure(getInsureBody(insuranceOrder));

    //更新保单号到数据库
    insuranceOrder.setInsureNum(simpleInsureRes.getData().get("insureNum").toString());
    insuranceOrder.setResponse(simpleInsureRes.getRespMsg());

  }

  /**
   * 支付保单信息
   *
   * @param
   * @throws Exception
   */
  @Override
  public void payInsurance(InsuranceOrder insuranceOrder,String outTradeNo) throws Exception {
    //本地支付
    InsuranceResponseTemplate localPayRes = localPay(getLocalPayBody(insuranceOrder));

    insuranceOrder.setResponse(localPayRes.getRespMsg());
    insuranceOrder.setStatus(1);
    insuranceOrder.setUpdateTime(new Date());
    insuranceOrder.setOutTradeNo(outTradeNo);

    insuranceOrderMapper.updateByPrimaryKeySelective(insuranceOrder);
  }


  /**
   * 开始保单流程
   *
   * @param insuranceOrder
   * @throws Exception
   */
  @Override
  public void simpleInsure(InsuranceOrder insuranceOrder) throws Exception {

    SimpleTrialReq simpleTrialReq = new SimpleTrialReq();
    simpleTrialReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    simpleTrialReq.setCaseCode(insuranceOrder.getCaseCode());
    simpleTrialReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    simpleTrialReq.setStartDate(insuranceOrder.getStartDate());
    simpleTrialReq.setEndDate(insuranceOrder.getEndDate());
    simpleTrialReq.setBirthday(insuranceOrder.getBirthday());
    //试算
    InsuranceResponseTemplate simpleTrialRes = simpleTrial(simpleTrialReq);
    insuranceOrder.setSinglePrice(Long.parseLong(simpleTrialRes.getData().get("singlePrice").toString()));

    //承保
    InsuranceResponseTemplate simpleInsureRes = simpleInsure(getInsureBody(insuranceOrder));

    //更新保单号到数据库
    insuranceOrder.setInsureNum(simpleInsureRes.getData().get("insureNum").toString());
    insuranceOrder.setResponse(simpleInsureRes.getRespMsg());


    InsuranceResponseTemplate localPayRes = localPay(getLocalPayBody(insuranceOrder));

    insuranceOrder.setResponse(localPayRes.getRespMsg());
    insuranceOrder.setStatus(1);
  }

  /**
   *
   * @param insuranceOrder
   * @return
   */
  private InsureReq getInsureBody(InsuranceOrder insuranceOrder){
    InsureReq insureReq = new InsureReq();
    insureReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    insureReq.setCaseCode(insuranceOrder.getCaseCode());
    insureReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    insureReq.setStartDate(insuranceOrder.getStartDate());
    insureReq.setEndDate(insuranceOrder.getEndDate());

    Applicant applicant = new Applicant();
    applicant.setCName("上海克勒布百体育发展有限公司");
    applicant.setCardType(14);
    applicant.setCardCode("91310106MA1FYLFM0B");
    applicant.setMobile("18616534694");
    applicant.setEmail("insurance@club100.cn");
    applicant.setApplicantType(1);

    Insurant insurant = new Insurant();
    insurant.setInsurantId(100);
    insurant.setCName(insuranceOrder.getUserName());
    insurant.setCardType(1);
    insurant.setCardCode(insuranceOrder.getCardCode());
    insurant.setBirthday(insuranceOrder.getBirthday());
    insurant.setSex(insuranceOrder.getUserSex());
    insurant.setSinglePrice(insuranceOrder.getSinglePrice());
    insurant.setCount(1);
    insurant.setMobile(insuranceOrder.getMobile());
    //其他
    insurant.setRelationId(23);

    insureReq.setApplicant(applicant);
    insureReq.setInsurants(Arrays.asList(insurant));
    return insureReq;
  }


  /**
   * 试算
   *
   * @param simpleTrialReq
   * @throws Exception
   */
  @Override
  public InsuranceResponseTemplate simpleTrial(SimpleTrialReq simpleTrialReq) throws Exception {
    String api = "simpleTrial";
    String jsonStr = JSON.toJSONString(simpleTrialReq);

    return doPostWithSign(api,jsonStr);
  }

  /**
   * 承保
   *
   * @param insureReq
   * @throws Exception
   */
  public InsuranceResponseTemplate simpleInsure(InsureReq insureReq) throws Exception {
    String api = "simpleInsure";
    String jsonStr = JSON.toJSONString(insureReq);

    return doPostWithSign(api,jsonStr);
  }


  /**
   * 本地支付
   *
   * @param localPayReq
   * @return
   * @throws Exception
   */
  public InsuranceResponseTemplate localPay(LocalPayReq localPayReq) throws Exception {
    String api = "localPay";
    String jsonStr = JSON.toJSONString(localPayReq);
    return doPostWithSign(api,jsonStr);
  }

  private LocalPayReq getLocalPayBody(InsuranceOrder insuranceOrder){
    LocalPayReq localPayReq = new LocalPayReq();
    localPayReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    localPayReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    localPayReq.setInsureNums(insuranceOrder.getInsureNum());
    localPayReq.setMoney(insuranceOrder.getSinglePrice());
    return localPayReq;
  }

  /**
   * 在线支付
   *
   * @param onlinePayReq
   * @throws Exception
   */
  public InsuranceResponseTemplate onlinePay(OnlinePayReq onlinePayReq) throws Exception {
    String api = "onlinePay";
    String jsonStr = JSON.toJSONString(onlinePayReq);

    return doPostWithSign(api,jsonStr);
  }

  /**
   * 下载保单
   *
   * @param policyUrlReq
   * @throws Exception
   */
  public InsuranceResponseTemplate downloadUrl(PolicyUrlReq policyUrlReq) throws Exception {
    String api = "downloadUrl";
    String jsonStr = JSON.toJSONString(policyUrlReq);

    return doPostWithSign(api,jsonStr);
  }

  private PolicyUrlReq downloadUrl(InsuranceOrder insuranceOrder){
    PolicyUrlReq policyUrlReq = new PolicyUrlReq();
    policyUrlReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    policyUrlReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    policyUrlReq.setInsureNum(insuranceOrder.getInsureNum());

    return policyUrlReq;
  }

  public static void main(String[] args) throws Exception {
    //
    InsuranceOrder insuranceOrder = new InsuranceOrder();
    insuranceOrder.setInsureNum("20201127020203");

//    InsuranceResponseTemplate insuranceResponseTemplate = new InsuranceServiceImpl().downloadUrl(new InsuranceServiceImpl().downloadUrl(insuranceOrder));

//    InsuranceResponseTemplate insuranceResponseTemplate = new InsuranceServiceImpl().downloadUrl(new InsuranceServiceImpl().downloadUrl(insuranceOrder));
//    System.out.println(JSON.toJSONString(insuranceResponseTemplate));

    SimpleTrialReq simpleTrialReq = new SimpleTrialReq();
    simpleTrialReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    simpleTrialReq.setCaseCode("QX000000129529");
    simpleTrialReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    simpleTrialReq.setStartDate("2020-12-05");
    simpleTrialReq.setEndDate("2020-12-13");
    simpleTrialReq.setBirthday("1990-12-16");

    new InsuranceServiceImpl().simpleTrial(simpleTrialReq);

//    InsuranceResponseTemplate localPayRes = new InsuranceServiceImpl().localPay(new InsuranceServiceImpl().getLocalPayBody(insuranceOrder));
//    System.out.println(JSON.toJSONString(localPayRes));

  }

  private void P_sendEmailWithAttachedFiles(String downloadUrl, String userEmail) {
    // Initialization.
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();
    try {
      MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
      helper.setFrom(EMAIL_FROM);
      helper.setTo(userEmail);
      helper.setSubject(EMAIL_SUBJECT);
      helper.setText("保单下载地址：" + downloadUrl);

    } catch (MessagingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }

    new Thread(() -> javaMailSender.send(mimeMessage)).start();

  }



  /**
   * 退保
   *
   * @param outTradeNo  保单号
   * @throws Exception
   */
  @Async
  @Override
  public void surrenderPolicy(String outTradeNo) throws Exception {
    String api = "surrenderPolicy";
    InsuranceOrder insuranceOrder = insuranceOrderMapper.selectByOutTradeNo(outTradeNo);

    if(insuranceOrder != null && insuranceOrder.getStatus() == 1){
      String jsonStr = JSON.toJSONString(getSurrenderPolicyBody(insuranceOrder.getInsureNum()));
      InsuranceResponseTemplate insuranceResponseTemplate = doPostWithSign(api,jsonStr);
      insuranceOrder.setStatus(2);
      insuranceOrder.setResponse(insuranceResponseTemplate.getRespMsg());
      insuranceOrder.setUpdateTime(new Date());

      insuranceOrderMapper.updateByPrimaryKeySelective(insuranceOrder);
    }
  }

  @Override
  public void surrenderPolicy(InsuranceOrder insuranceOrder) throws Exception {
    String api = "surrenderPolicy";

    if(insuranceOrder != null && insuranceOrder.getStatus() == 1){
      String jsonStr = JSON.toJSONString(getSurrenderPolicyBody(insuranceOrder.getInsureNum()));
      InsuranceResponseTemplate insuranceResponseTemplate = doPostWithSign(api,jsonStr);
      if (insuranceResponseTemplate.getRespCode() == 0) {
          insuranceOrder.setStatus(2);
        } else{
        insuranceOrder.setStatus(3);
      }
      insuranceOrder.setResponse(insuranceResponseTemplate.getRespMsg());
      insuranceOrder.setUpdateTime(new Date());
      insuranceOrderMapper.updateByPrimaryKeySelective(insuranceOrder);
    }
  }

  private SurrenderPolicyReq getSurrenderPolicyBody(String insureNum){
    SurrenderPolicyReq surrenderPolicyReq = new SurrenderPolicyReq();
    surrenderPolicyReq.setTransNo("huize20171027100140000007");
    surrenderPolicyReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    surrenderPolicyReq.setInsureNum(insureNum);

    return surrenderPolicyReq;
  }


  @Override
  public String downloadInsurance(DownloadInsurance downloadInsurance) throws Exception {
    ClubOrder clubOrder = clubOrderMapper.selectByPrimaryKey(downloadInsurance.getOrderId());
    if(clubOrder == null){
      throw new CustomException("订单信息错误");
    }
    if(clubOrder.getStatus() == 2){
      throw new CustomException("订单支付失败");
    }
    InsuranceOrder insuranceOrder = insuranceOrderMapper.selectByOutTradeNo(clubOrder.getOutTradeNo());
//    InsuranceOrder insuranceOrder = new InsuranceOrder();
//    insuranceOrder.setInsureNum("20201011010367");
    if(insuranceOrder == null || insuranceOrder.getStatus() != 1){
      throw new CustomException("保单不存在");
    }
    if(insuranceOrder.getStatus() == 0){
      throw new CustomException("没有入保");
    }
    if(insuranceOrder.getStatus() == 2){
      throw new CustomException("已经退保");
    }

    PolicyUrlReq policyUrlReq = new PolicyUrlReq();
    policyUrlReq.setTransNo(OrderCoderUtil.getInsuranceCode());
    policyUrlReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
    policyUrlReq.setInsureNum(insuranceOrder.getInsureNum());

    InsuranceResponseTemplate simpleInsureRes = downloadUrl(policyUrlReq);

    P_sendEmailWithAttachedFiles(simpleInsureRes.getData().get("fileUrl").toString(), downloadInsurance.getEmail());

    return simpleInsureRes.getData().get("fileUrl").toString();
  }


  /**
   * 统一post请求
   *
   * @param api       请求接口
   * @param jsonStr   请求内容转json
   * @throws Exception
   */
  public InsuranceResponseTemplate doPostWithSign(String api,String jsonStr) throws Exception {
    //环境地址
    String baseUrl = BXConstants.PRO_CORE + api;
    //签名key
    String key = BXConstants.PRO_SIGN_KEY;
    //签名
    String sign = InsuranceUtil.MD5(key + jsonStr);

    String url = baseUrl + "?sign=" + sign;
    System.out.println("url:" + url);
    System.out.println("jsonStr:" + jsonStr);

    String response = MyPost.postBodyWithHeader(url,jsonStr, "application/json;charset=utf-8");
    System.out.println("response:" + response);

    InsuranceResponseTemplate responseTemplate = JSON.parseObject(response, new TypeReference<InsuranceResponseTemplate>() {
    });
    if(responseTemplate.getRespCode() != 0){
      throw new CustomException(responseTemplate.getRespMsg());
    }
    return responseTemplate;
  }

}

package com.club100.project.insurance.chudan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IssueNotify implements Serializable {

int  partnerId;  //开发者身份标识

String  partnerUniqueKey;  //开发者用户标识

String  caseCode;  //方案代码

String  insureNum;  //投保单号

boolean  state;  //是否出单成功 true：成功 false：失败

List<PolicyInfo> policys;  //保单信息列表

String  failMsg;  //失败原因

Map<String,Object> otherInfo;  //其他信息

}

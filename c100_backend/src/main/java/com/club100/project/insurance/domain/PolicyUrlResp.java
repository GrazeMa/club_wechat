package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PolicyUrlResp {
  //获取保单文件下载地址 返回

  private String  transNo;    //交易流水号，生成规则参考名词解释

  private int partnerId;    //开发者身份标识，获取方式参考名词解释

  private String insureNum;    //投保单号

  private String fileUrl;    //文件下载地址
}

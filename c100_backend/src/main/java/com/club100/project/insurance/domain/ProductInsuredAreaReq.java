package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductInsuredAreaReq {
 //居住省市

  private String transNo;    //必填	交易流水号，生成规则参考名词解释
  private int partnerId;    //必填	开发者身份标识，获取方式参考名词解释
  private String caseCode;    //必填	方案代码，获取方式参考名词解释
  private String areaCode;     //	地区编码（用于查询下一级地区，顶级默认空）
}

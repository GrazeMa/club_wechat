package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Insurant {

  private int    insurantId;   //必填	被保人id，由开发者传递（同一单此字段不能重复，取值须在int范围内）
  private String cName;   //必填	中文名
;
  private int    cardType;   //必填	证件类型，取值参考附录1
  private String cardCode;   //必填	证件号
  private int    sex;   //必填	性别 0：女 1：男
  private String birthday;   //必填	出生日期 格式：yyyy-MM-dd
  private int    relationId;   //必填	与投保人关系，取值参考附录3
  private int    count;   //必填	购买份数
  private long   singlePrice;   //必填	产品单价（单位：分）
  private String mobile;   //	手机号码
}

package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Applicant implements Serializable {

  String  cName;    // 必填	中文名
  String  eName;    // 	拼音或英文名，境外旅游险必填
  int  cardType;    // 必填	证件类型，取值参考附录1
  String  cardCode;    // 必填	证件号
  int  sex;           // 必填	性别 0：女 1：男
  String  birthday;    // 必填	出生日期 格式：yyyy-MM-dd
  String  mobile;    // 必填	手机号码
  String  email;    // 必填	邮箱
  int  applicantType;    // 投保人类型 0：个人（默认） 1：公司
}

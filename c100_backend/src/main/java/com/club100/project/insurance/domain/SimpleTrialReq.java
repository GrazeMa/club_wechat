package com.club100.project.insurance.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SimpleTrialReq {

  @ApiModelProperty(hidden = true)
  private String  transNo;    //必填	交易流水号，生成规则参考名词解释

  @ApiModelProperty(hidden = true)
  private int  partnerId;    //必填	开发者身份标识，获取方式参考名词解释

  @ApiModelProperty(hidden = true)
  private int  subPartnerId;    //	开发者子标识，多层级账户使用

  @ApiModelProperty(hidden = true)
  private String  caseCode;    //必填	方案代码，获取方式参考名词解释
  @ApiModelProperty("被保人出生日期 格式：yyyy-MM-dd")
  private String  birthday;    //必填	被保人出生日期 格式：yyyy-MM-dd
  @ApiModelProperty("起保日期 格式：yyyy-MM-dd")
  private String   startDate;    //必填	起保日期 格式：yyyy-MM-dd
  @ApiModelProperty("终保日期 格式：yyyy-MM-dd")
  private String    endDate;    //必填	终保日期 格式：yyyy-MM-dd

  @ApiModelProperty(hidden = true)
  private Map<String, String> extMap;    //	试算扩展字段，特殊产品必填
}

package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttributeModule {

  private int    moduleId;   //模块id，取值参考附录6
  private String name;   //模块名称
  private String remark;   //模块说明
  private List<ProductAttribute> productAttributes;   //模块属性列表

}

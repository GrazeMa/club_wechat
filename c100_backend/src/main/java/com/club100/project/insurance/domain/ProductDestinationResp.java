package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDestinationResp {

  String  transNo;   //交易流水号
  int  partnerId;   //开发者身份标识
  String  caseCode;   //方案代码
  boolean  chooseOne;   //是否只能选择一个国家 true：是 false：否
  List<ProductDestination> destinations;//目的地信息列表

}

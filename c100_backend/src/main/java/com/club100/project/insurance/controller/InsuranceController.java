package com.club100.project.insurance.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.project.insurance.domain.InsurantUserRequest;
import com.club100.project.insurance.domain.SimpleTrialReq;
import com.club100.project.insurance.sdk.BXConstants;
import com.club100.project.insurance.service.InsuranceService;
import com.club100.project.wechat.domain.DownloadInsurance;
import com.club100.project.wechat.domain.NullDataTo;
import com.club100.project.wechat.utils.InsuranceResponseTemplate;
import com.club100.project.wechat.utils.OrderCoderUtil;
import com.club100.project.wechat.utils.ResponseTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * @author
 */
@Api(tags = "保险对接接口")
@RestController
@RequestMapping("/v1/insurance")
public class InsuranceController extends BaseController {

  @Resource
  InsuranceService insuranceService;

  @ApiOperation("保险对接接口->保存用户信息")
  @PostMapping("saveUserInfo")
  public ResponseTemplate<Map<String,Long>> order(@ApiIgnore HttpServletRequest httpServletRequest,
                                                  @RequestBody InsurantUserRequest request,
                                                  @ApiIgnore ResponseTemplate<Map<String,Long>> response) {
    try {
      response.setData(insuranceService.saveUserInfo(request));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @ApiOperation("保险对接接口->试算")
  @PostMapping("simpleTrial")
  public ResponseTemplate<InsuranceResponseTemplate> simpleTrial(@ApiIgnore HttpServletRequest httpServletRequest,
                                                      @RequestBody SimpleTrialReq simpleTrialReq,
                                                      @ApiIgnore ResponseTemplate<InsuranceResponseTemplate> response) {
    try {
      simpleTrialReq.setTransNo(OrderCoderUtil.getInsuranceCode());
      simpleTrialReq.setCaseCode("QX000000129529");
      simpleTrialReq.setPartnerId(Integer.valueOf(BXConstants.PARTNER_ID));
      response.setData(insuranceService.simpleTrial(simpleTrialReq));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @ApiOperation("保险对接接口->退保")
  @PostMapping("surrenderPolicy/{outTrialNo}")
  public ResponseTemplate<NullDataTo> surrenderPolicy(@ApiIgnore HttpServletRequest httpServletRequest,
                                                   @PathVariable("outTrialNo") String outTrialNo,
                                                   @ApiIgnore ResponseTemplate<NullDataTo> response) {
    try {
      insuranceService.surrenderPolicy(outTrialNo);
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @ApiOperation("保险对接接口->保单下载")
  @PostMapping("downloadInsurance")
  public ResponseTemplate<String> downloadInsurance(@ApiIgnore HttpServletRequest httpServletRequest,
                                                       @RequestBody DownloadInsurance downloadInsurance,
                                                      @ApiIgnore ResponseTemplate<String> response) {
    try {
      response.setData(insuranceService.downloadInsurance(downloadInsurance));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

}


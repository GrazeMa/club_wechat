package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notify implements Serializable {

  int notifyType; //通知类型 支付：2
  String sign; //签名，MD5（key+data）
  PayNotify data; //支付通知信息
}

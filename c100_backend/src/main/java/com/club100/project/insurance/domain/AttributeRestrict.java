package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttributeRestrict implements Serializable {

 private String name; //约束名称
 private int conditions; //约束条件 0：大于 1：大于等于 2：小于 3：小于等于 4：不等于 5：等于 6：包含 7：不包含 8：提示 9：隐藏 10：正则
 private String remind; //正则约束条件验证失败提示
 private int attributeType; //所限制属性的控件类型
 private String value; //约束值
 private int unit; //单位 0：无 1：份 2：万元 3：元 4：0元 5：00元 6：000元 7：岁 8：年 9：月 10：天
 private String apiName; //api接口请求参数名
}

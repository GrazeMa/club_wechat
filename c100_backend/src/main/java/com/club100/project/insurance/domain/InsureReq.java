package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsureReq {

  private String      transNo;    //必填	交易流水号，生成规则参考名词解释

  private int      partnerId;    //必填	开发者身份标识，获取方式参考名词解释

//  private int      subPartnerId;    //	开发者子标识，多层级账户使用

  private String      insureNum;    //	投保单号，用来修改保单(当传值时切保单未支付会以本次投保信息覆盖之前保单信息)

  private String      caseCode;    //必填	方案代码，获取方式参考名词解释

  private String      startDate;    //必填	起保时间  格式：yyyy-MM-dd

  private String      endDate;    //必填	终保时间  格式：yyyy-MM-dd

  private Applicant      applicant;    //必填	投保人信息

  private List<Insurant> insurants;    //必填	被保人信息列表

  private String  partnerUniqKey;    //	开发者用户唯一标识


//  private int  directPay;    //	必填	是否直接支付（1表示跳转到页面后，携保提供页面支付入口，0表示只跳转页面确认资料最终回到渠道自己页面处理支付）
//  private String  callbackUrl;    //必填	cps确认资料后跳转页面（如果directPay为1表示在携保页面支付后的跳转页面，0用户在携保页面确认资料后再回到的渠道页面（如果是app或小程序非浏览器环境接入需要考虑处理原生跳转路径问题））
}

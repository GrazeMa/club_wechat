package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SimpleTrialResp {

  String transNo;  //交易流水号

  int partnerId;  //开发者身份标识

  String caseCode;  //方案代码

  String startDate;  //起保日期 格式：yyyy-MM-dd

  String endDate;  //终保日期 格式：yyyy-MM-dd

  long singlePrice;  //产品支付单价（单位：分）

  long originalPrice;  //产品原价（单位：分）

  int buyQuot;  //被保人同一时间段购买限额（份数）
}

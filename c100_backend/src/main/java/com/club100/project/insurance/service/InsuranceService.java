package com.club100.project.insurance.service;

import com.club100.project.insurance.domain.InsuranceOrder;
import com.club100.project.insurance.domain.InsurantUserRequest;
import com.club100.project.insurance.domain.SimpleTrialReq;
import com.club100.project.wechat.domain.DownloadInsurance;
import com.club100.project.wechat.utils.InsuranceResponseTemplate;
import org.springframework.scheduling.annotation.Async;

import java.util.Map;

public interface InsuranceService {

  Map<String,Long> saveUserInfo(InsurantUserRequest insurantUserRequest) throws Exception;

  void payInsurance(InsuranceOrder insuranceOrder, String outTradeNo) throws Exception;

  void simpleInsure(InsuranceOrder insuranceOrder) throws Exception;

  InsuranceResponseTemplate simpleTrial(SimpleTrialReq simpleTrialReq) throws Exception;

  void surrenderPolicy(String insureNum) throws Exception;

  void surrenderPolicy(InsuranceOrder insuranceOrder) throws Exception;

  String downloadInsurance(DownloadInsurance downloadInsurance) throws Exception;
}

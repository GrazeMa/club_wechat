package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.PizzaGetter;
import com.club100.project.wechat.mapper.PizzaGetterMapper;
import com.club100.project.wechat.service.IPizzaGetterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 13:16:15
 * @Description: 披萨获得者服务实现类。
 */
@Slf4j
@Service
public class PizzaGetterServiceImpl implements IPizzaGetterService {

    @Autowired
    private PizzaGetterMapper pizzaGetterMapper;

    @Override
    public int insertPizzaGetter(PizzaGetter pizzaGetter) {
        return pizzaGetterMapper.insertPizzaGetter(pizzaGetter);
    }

    @Override
    public int updatePizzaGetter(PizzaGetter pizzaGetter) {
        return pizzaGetterMapper.updatePizzaGetter(pizzaGetter);
    }

    @Override
    public int selectPizzaGetterCount() {
        return pizzaGetterMapper.selectPizzaGetterCount();
    }

}

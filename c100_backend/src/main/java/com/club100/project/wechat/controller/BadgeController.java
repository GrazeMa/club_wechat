package com.club100.project.wechat.controller;

import com.club100.common.utils.poi.ExcelUtil;
import com.club100.framework.aspectj.lang.annotation.Log;
import com.club100.framework.aspectj.lang.enums.BusinessType;
import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.BadgeOrder;
import com.club100.project.wechat.service.IBadgeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/02/09 21:15:25
 * @Description: 徽章订单控制器。
 */
@RequestMapping("/v1/badge")
@RestController
public class BadgeController extends BaseController {

    // Badge order information.
    @Autowired
    private IBadgeOrderService badgeOrderService;

    @GetMapping("/listBadgeOrders")
    public TableDataInfo listBadgeOrders(BadgeOrder badgeOrder) {

        startPage();

        badgeOrder.setBadgeOrderState(1);
        List<BadgeOrder> badgeOrdersList = badgeOrderService.selectBadgeOrderList(badgeOrder);

        return getDataTable(badgeOrdersList);

    }

    @Log(title = "徽章订单管理", businessType = BusinessType.EXPORT)
    @GetMapping("/exportBadgeOrders")
    public AjaxResult exportBadgeOrders(BadgeOrder badgeOrder) {

        badgeOrder.setBadgeOrderState(1);
        List<BadgeOrder> badgeOrdersList = badgeOrderService.selectBadgeOrderList(badgeOrder);

        ExcelUtil<BadgeOrder> util = new ExcelUtil<>(BadgeOrder.class);
        return util.exportExcel(badgeOrdersList, "徽章订单数据");

    }

    /**
     * 根据主键徽章订单信息。
     */
    @RequestMapping(value = "/getBadgeById")
    public AjaxResult getBadgeById(@RequestBody Long badgeOrderId) {

        return AjaxResult.success(badgeOrderService.selectBadgeById(badgeOrderId));

    }

    /**
     * 修改徽章订单信息。
     */
    @RequestMapping(value = "/updateBadge")
    public Integer updateBadge(@RequestBody BadgeOrder badgeOrder) {

        return badgeOrderService.updateBadgeOrder(badgeOrder);

    }

}

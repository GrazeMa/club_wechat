package com.club100.project.wechat.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/8/13 14:31:06
 * @Description: 主键 ID 自动生成相关工具类。
 */
public class PrimaryKeyIdUtils {

    // 私有 PrimaryKeyIdUtils 构造类，让外界不能创建对象，只能静态调用方法。
    private PrimaryKeyIdUtils() {

        throw new UnsupportedOperationException("'PrimaryKeyIdUtils' can't be instantiated~");

    }

    public static String S_uuidAutoGenerator() {

        return UUID.randomUUID().toString();

    }

    public static String S_uuidWithoutHyphenAutoGenerator() {

        return UUID.randomUUID().toString().replaceAll("-", "");

    }

    private static long tmpID = 0;
    private static boolean tmpIDlocked = false;

    public static long S_SimpleLongAutoGenerator() {

        long ltime = 0;
        while (true) {
            if (tmpIDlocked == false) {
                tmpIDlocked = true;
                //当前：（年、月、日、时、分、秒、毫秒）*10000
                ltime = Long.valueOf(new SimpleDateFormat("yyMMddhhmmssSSS").format(new Date()).toString()) * 10000;
                if (tmpID < ltime) {
                    tmpID = ltime;
                } else {
                    tmpID = tmpID + 1;
                    ltime = tmpID;
                }
                tmpIDlocked = false;
                return ltime;
            }
        }

    }

}

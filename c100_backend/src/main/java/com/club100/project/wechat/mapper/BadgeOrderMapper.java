package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BadgeOrder;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/20 15:48:28
 * @Description: 徽章订单 Mapper 接口。
 */
public interface BadgeOrderMapper {

    BadgeOrder selectBadgeById(Long badgeOrderId);

    List<BadgeOrder> selectBadgeOrderList(BadgeOrder badgeOrder);

    BadgeOrder selectOneBadgeOrderByBadgeIdAndUserId(BadgeOrder badgeOrder);

    int insertBadgeOrder(BadgeOrder badgeOrder);

    int updateBadgeOrder(BadgeOrder badgeOrder);

    int updateBadgeOrderByOrderId(BadgeOrder badgeOrder);

}

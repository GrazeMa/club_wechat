package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/16 12:19:30
 * @Description: 活动图片返回值。
 */
@AllArgsConstructor
public class ReturnActivityPictureInfo implements Serializable {

    public Long returnActivityPictureInfoId;
    public String returnActivityNameInfo;
    public String[] returnActivityPictureInfo;
    
    public ReturnActivityPictureInfo() {}

    public String toJson(ReturnActivityPictureInfo returnActivityPictureInfo) {

        // ReturnActivityPictureInfo 对象转 JSON。
        return JSON.toJSONString(returnActivityPictureInfo);

    }

}

package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.SignUp;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 14:53:31
 * @Description: 报名 Mapper 接口。
 */
public interface SignUpMapper {

    int insertSignUp(SignUp signUp);

    SignUp selectSignUp(SignUp signUp);

}

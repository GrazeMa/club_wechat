package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RouteCorrectionPicture;
import com.club100.project.wechat.mapper.RouteCorrectionPictureMapper;
import com.club100.project.wechat.service.IRouteCorrectionPictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 14:11:45
 * @Description: 路线纠错图片服务实现类。
 */
@Slf4j
@Service
public class RouteCorrectionPictureServiceImpl implements IRouteCorrectionPictureService {

    @Autowired
    private RouteCorrectionPictureMapper routeCorrectionPictureMapper;

    @Override
    public int insertRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture) {
        return routeCorrectionPictureMapper.insertRouteCorrectionPicture(routeCorrectionPicture);
    }

    @Override
    public int insertRouteCorrectionPictureInBatch(List<RouteCorrectionPicture> routeCorrectionPictureList) {
        return routeCorrectionPictureMapper.insertRouteCorrectionPictureInBatch(routeCorrectionPictureList);
    }

    @Override
    public RouteCorrectionPicture selectRouteCorrectionPictureById(Long routeCorrectionPictureId) {
        return routeCorrectionPictureMapper.selectRouteCorrectionPictureById(routeCorrectionPictureId);
    }

    @Override
    public List<RouteCorrectionPicture> selectRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture) {
        return routeCorrectionPictureMapper.selectRouteCorrectionPicture(routeCorrectionPicture);
    }

}

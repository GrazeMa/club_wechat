package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.IActivityLogisticsService;
import com.club100.project.wechat.service.IActivityService;
import com.club100.project.wechat.service.ILogisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/30 18:26:39
 * @Description: 后勤保障控制器。
 */
@RequestMapping("/v1/activitylogistics")
@RestController
public class ActivityLogisticsController extends BaseController {

    // Activity logistics information.
    @Autowired
    private IActivityLogisticsService activityLogisticsService;
    // Activity information.
    @Autowired
    private IActivityService activityService;
    // Logistics information.
    @Autowired
    private ILogisticsService logisticsService;

    @RequestMapping("/addActivityLogisticsInBatch")
    public int addActivityLogisticsInBatch(@RequestBody BikeShop bikeShop) {

        List<ActivityLogistics> activityLogisticsList = new ArrayList<>();

        return activityLogisticsService.insertActivityLogisticsInBatch(activityLogisticsList);

    }

    /**
     * 查询活动后勤保障列表。
     */
    @GetMapping("/listActivityLogistics")
    public TableDataInfo listActivityLogistics(ActivityLogistics activityLogistics) {

        startPage();
        List<ReturnActivityLogisticsInfo> returnedActivityLogisticsInfoList = new ArrayList<>();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activityLogistics.setSysUserId(sysUserIdLong);
        List<ActivityLogistics> activityLogisticsList = activityLogisticsService.selectActivityLogisticsListWithPrivilege(activityLogistics);

        // 分组。
        Map<Long, List<ActivityLogistics>> groupByActivityIdMap = activityLogisticsList.stream().collect(Collectors.groupingBy(ActivityLogistics::getActivityId));

        // 遍历分组并处理数据。
        for (Map.Entry<Long, List<ActivityLogistics>> entryActivityLogistics : groupByActivityIdMap.entrySet()) {
            Long key = entryActivityLogistics.getKey();

            String activityNameString = P_GetActivityNameById(key);
            Long[] ids = new Long[activityLogisticsList.size()];
            for (int i = 0; i < activityLogisticsList.size(); i++) {
                ids[i]= activityLogisticsList.get(i).getLogisticsId();
            }

            ReturnActivityLogisticsInfo returnActivityLogisticsInfo = new ReturnActivityLogisticsInfo(key, activityNameString, ids);
            returnedActivityLogisticsInfoList.add(returnActivityLogisticsInfo);

        }

        return getDataTable(returnedActivityLogisticsInfoList);

    }

    /**
     * 删除活动后勤保障信息。
     */
    @DeleteMapping("/{returnActivityLogisticsInfoIds}")
    public AjaxResult deleteActivityLogistics(@PathVariable Long[] returnActivityLogisticsInfoIds) {

        return toAjax(activityLogisticsService.deleteActivityLogistics(returnActivityLogisticsInfoIds));

    }

    /**
     * 根据活动编号获取详细信息。
     */
    @RequestMapping(value = "/getActivityLogisticsById")
    public AjaxResult getActivityLogisticsById(@RequestBody Long returnActivityLogisticsInfoId) {

        ActivityLogistics searchedActivityLogisticsEntity = new ActivityLogistics();
        searchedActivityLogisticsEntity.setActivityId(returnActivityLogisticsInfoId);
        String activityNameString = P_GetActivityNameById(returnActivityLogisticsInfoId);

        List<ActivityLogistics> activityLogisticsList = activityLogisticsService.selectActivityLogisticsList(searchedActivityLogisticsEntity);

        Long[] ids = new Long[activityLogisticsList.size()];
        for (int i = 0; i < activityLogisticsList.size(); i++) {
            ids[i]= activityLogisticsList.get(i).getLogisticsId();
        }
        ReturnActivityLogisticsInfo returnActivityLogisticsInfo = new ReturnActivityLogisticsInfo(returnActivityLogisticsInfoId, activityNameString, ids);
        return AjaxResult.success(returnActivityLogisticsInfo);

    }

    @RequestMapping("/addActivityLogistics")
    public int addActivityLogistics(@RequestBody ReturnActivityLogisticsInfo returnActivityLogisticsInfo) {

        List<ActivityLogistics> insertedActivityLogisticsList = P_InsertActivityLogisticsInBatch(returnActivityLogisticsInfo, "insert");

        return activityLogisticsService.insertActivityLogisticsInBatch(insertedActivityLogisticsList);

    }

    @RequestMapping("/updateActivityLogistics")
    public int updateActivityLogistics(@RequestBody ReturnActivityLogisticsInfo returnActivityLogisticsInfo) {

        int deletedCount = activityLogisticsService.deleteActivityLogistics(Long.valueOf(returnActivityLogisticsInfo.returnActivityLogisticsInfoId));
        List<ActivityLogistics> insertedActivityLogisticsList;
        int insertRowsInt = 0;
        insertedActivityLogisticsList = P_InsertActivityLogisticsInBatch(returnActivityLogisticsInfo, "update");
        insertRowsInt = activityLogisticsService.insertActivityLogisticsInBatch(insertedActivityLogisticsList);

        return insertRowsInt;

    }

    /**
     * Get activity name by activity Id.
     *
     * @param activityId
     * @return
     */
    private String P_GetActivityNameById(long activityId) {

        Activity searchedActivityEntity = activityService.selectActivityById(activityId);
        if(searchedActivityEntity!=null) {
            return searchedActivityEntity.getActivityName();
        }

        return "";

    }

    /**
     * Get logistics name by logistics Id.
     *
     * @param logisticsId
     * @return
     */
    private String P_GetLogisticsNameById(long logisticsId) {

        Logistics searchedLogisticsEntity = logisticsService.selectLogisticsById(logisticsId);

        return searchedLogisticsEntity.getLogisticsName();

    }

    /**
     * insert activity logistics in batch.
     *
     * @param returnActivityLogisticsInfo
     * @return insertedActivityLogisticsList
     */
    private List<ActivityLogistics> P_InsertActivityLogisticsInBatch(ReturnActivityLogisticsInfo returnActivityLogisticsInfo, String actionString) {

        List<ActivityLogistics> insertedActivityLogisticsList = new ArrayList<>();

        for (int i = 0; i < returnActivityLogisticsInfo.returnActivityLogisticsInfo.length; i++) {
            ActivityLogistics eachInsertedActivityLogisticsEntity = new ActivityLogistics();
            if ("insert".equalsIgnoreCase(actionString)) {
                eachInsertedActivityLogisticsEntity.setActivityId(Long.valueOf(returnActivityLogisticsInfo.returnActivityNameInfo));
            } else if ("update".equalsIgnoreCase(actionString)) {
                eachInsertedActivityLogisticsEntity.setActivityId(Long.valueOf(returnActivityLogisticsInfo.returnActivityLogisticsInfoId));
            } else {
            }
            eachInsertedActivityLogisticsEntity.setLogisticsId(Long.valueOf(returnActivityLogisticsInfo.returnActivityLogisticsInfo[i]));

            insertedActivityLogisticsList.add(eachInsertedActivityLogisticsEntity);
        }

        return insertedActivityLogisticsList;

    }

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/19 22:45:27
 * @Description: 报名用户 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpUser extends BaseEntity implements Serializable {

    private Long signUpUserId; // Primary Key ID。

    private Long activityId; // 活动主键。
    private Long userId; // 用户主键。
    private String wxUserAvatarUrl; // 用户微信头像 URL。

    public String toJson(SignUpUser signUpUser) {

        // SignUpUser 对象转 JSON。
        return JSON.toJSONString(signUpUser);

    }

}

package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.SignUp;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 15:09:10
 * @Description: 报名服务类。
 */
public interface ISignUpService {

    /**
     * 插入一条报名信息。
     *
     * @param signUp
     * @return
     */
    int insertSignUp(SignUp signUp);

    /**
     * 查询一条报名信息。
     *
     * @param signUp
     * @return
     */
    SignUp selectSignUp(SignUp signUp);

}

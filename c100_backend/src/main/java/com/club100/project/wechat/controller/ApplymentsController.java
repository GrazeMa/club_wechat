package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author
 */
@Api(tags = "用户进件")
@RestController
@RequestMapping("/v1/Applyments")
public class ApplymentsController extends BaseController {

}


package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BadgeConsignee;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/18 16:09:28
 * @Description: 徽章收货人 Mapper 接口。
 */
public interface BadgeConsigneeMapper {

    List<BadgeConsignee> selectBadgeConsigneeList(BadgeConsignee badgeConsignee);

    BadgeConsignee selectBadgeConsigneeById(Long badgeConsigneeId);

    List<BadgeConsignee> selectBadgeConsigneeByUserId(Long userId);

    BadgeConsignee selectOneBadgeConsigneeByUserId(Long userId);

    int insertBadgeConsignee(BadgeConsignee badgeConsignee);

    int updateBadgeConsignee(BadgeConsignee badgeConsignee);

    int updateBadgeConsigneeByUserId(BadgeConsignee badgeConsignee);

}

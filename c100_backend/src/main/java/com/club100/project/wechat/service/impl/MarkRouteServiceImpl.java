package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;
import com.club100.project.wechat.mapper.MarkRouteMapper;
import com.club100.project.wechat.service.IMarkRouteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:42:19
 * @Description: 打卡路线服务实现类。
 */
@Slf4j
@Service
public class MarkRouteServiceImpl implements IMarkRouteService {

    @Autowired
    private MarkRouteMapper markRouteMapper;

    @Override
    public int insertMarkRoute(MarkRoute markRoute) {
        return markRouteMapper.insertMarkRoute(markRoute);
    }

    @Override
    public List<MarkRoute> selectMarkRouteByUserIDList(MarkRoute markRoute) {
        return markRouteMapper.selectMarkRouteByUserIDList(markRoute);
    }

    @Override
    public Integer selectMarkRouteCountList(MarkRoute markRoute) {
        return markRouteMapper.selectMarkRouteCountList(markRoute);
    }

    @Override
    public int selectMarkRouteCountByUserId(Long userId) {
        return markRouteMapper.selectMarkRouteCountByUserId(userId);
    }

    @Override
    public MarkRoute selectMarkRouteOne(MarkRoute markRoute) {
        return markRouteMapper.selectMarkRouteOne(markRoute);
    }

    @Override
    public List<MarkRoute> selectMarkRouteList(MarkRoute markRoute) {
        return markRouteMapper.selectMarkRouteList(markRoute);
    }

    @Override
    public Integer updateMarkRouteById(MarkRoute markRoute) {
        return markRouteMapper.updateMarkRouteById(markRoute);
    }

}

package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Route;
import com.club100.project.wechat.domain.RouteAndDetail;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 19:37:52
 * @Description: 路线和路线详情 Mapper 接口。
 */
public interface RouteAndDetailMapper {

    RouteAndDetail selectRouteAndDetail(RouteAndDetail routeAndDetail);

    List<RouteAndDetail> selectRouteAndDetail2(RouteAndDetail routeAndDetail);

}

package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RouteCorrectionPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 14:07:24
 * @Description: 路线纠错图片服务类。
 */
public interface IRouteCorrectionPictureService {

    /**
     * 插入路线纠错图片信息。
     *
     * @param routeCorrectionPicture
     * @return
     */
    int insertRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture);

    /**
     * 批量插入路线纠错图片信息。
     *
     * @param routeCorrectionPictureList
     * @return
     */
    int insertRouteCorrectionPictureInBatch(List<RouteCorrectionPicture> routeCorrectionPictureList);

    /**
     * 通过主键查询路线纠错图片信息。
     *
     * @param routeCorrectionPictureId
     * @return
     */
    RouteCorrectionPicture selectRouteCorrectionPictureById(Long routeCorrectionPictureId);

    /**
     * 查询路线纠错图片信息。
     *
     * @param routeCorrectionPicture
     * @return
     */
    List<RouteCorrectionPicture> selectRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture);

}

package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Card;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/16 17:39:43
 * @Description: 卡片服务类。
 */
public interface ICardService {
    /**
     * 插入卡片信息。
     *
     * @param card
     * @return
     */
    int insert(Card card);

    /**
     * 列举卡片信息（最多五条）。
     *
     * @param card
     * @return
     */
    List<Card> selectCardAtMostFiveList(Card card);

    /**
     * 列举卡片信息。
     *
     * @param card
     * @return
     */
    List<Card> page(Card card);

    /**
     * 删除卡片信息。
     *
     * @param id(s)
     * @return
     */
    int deleteByCardIds(Long... id);

    /**
     * 修改卡片信息。
     *
     * @param card
     * @return
     */
    int updateCard(Card card);

}

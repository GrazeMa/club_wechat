package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.mapper.CardMapper;
import com.club100.project.wechat.domain.Card;
import com.club100.project.wechat.service.ICardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/16 17:45:20
 * @Description: 卡片服务实现类。
 */
@Slf4j
@Service
public class CardServiceImpl implements ICardService {

    @Autowired
    private CardMapper cardMapper;

    @Override
    public List<Card> page(Card card) {
        return cardMapper.selectCardList(card);
    }

    @Override
    public int insert(Card card) {
        return cardMapper.insertCard(card);
    }

    @Override
    public List<Card> selectCardAtMostFiveList(Card card) {
        return cardMapper.selectCardAtMostFiveList(card);
    }

    @Override
    @Transactional
    public int deleteByCardIds(Long... cityId) {
        return cardMapper.deleteByCardIds(cityId);
    }

    @Override
    public int updateCard(Card card) {
        return cardMapper.updateCard(card);
    }

}

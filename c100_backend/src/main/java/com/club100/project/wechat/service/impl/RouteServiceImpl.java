package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Route;
import com.club100.project.wechat.mapper.RouteMapper;
import com.club100.project.wechat.service.IRouteService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 16:27:04
 * @Description: 路线服务实现类。
 */
@Slf4j
@Service
public class RouteServiceImpl implements IRouteService {

    @Autowired
    private RouteMapper routeMapper;

    @Override
    public List<Route> selectRouteList(Integer cityType, Integer routeDistance1Begin, Integer routeDistance1End, Integer routeDistance2Begin, Integer routeDistance2End, Integer routeDistance3Begin, Integer routeDistance3End, Integer routeDistance4Begin, Integer routeDistance4End, Integer routeDistance5Begin, Integer routeDistance5End, Integer routeDistance6Begin, Integer routeDistance6End, Integer[] routeTerrain, Integer[] routeType) {
        return routeMapper.selectRouteList(cityType, routeDistance1Begin, routeDistance1End, routeDistance2Begin, routeDistance2End, routeDistance3Begin, routeDistance3End, routeDistance4Begin, routeDistance4End, routeDistance5Begin, routeDistance5End, routeDistance6Begin, routeDistance6End, routeTerrain, routeType);
    }

    @Override
    public List<Route> selectByRouteIds(RowBounds rowBounds, Long... routeIds) {
        return routeMapper.selectByRouteIds(rowBounds, routeIds);
    }

    @Override
    public int insert(Route route) {
        return routeMapper.insertRoute(route);
    }

    @Override
    public int insertOneRoute(Route route) {
        return routeMapper.insertOneRoute(route);
    }

    @Override
    public List<Route> selectRouteAtMostTwoList(Route route) {
        return routeMapper.selectRouteAtMostTwoList(route);
    }

    @Override
    @Transactional
    public int deleteByRouteIds(Long... id) {
        return routeMapper.deleteByRouteIds(id);
    }

    @Override
    public Route getLatestRouteId() {
        return routeMapper.getLatestRouteId();
    }

    @Override
    public int updateRoute(Route route) {
        return routeMapper.updateRoute(route);
    }

}

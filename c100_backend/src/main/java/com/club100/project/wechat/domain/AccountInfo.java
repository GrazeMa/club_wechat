package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfo extends BaseEntity implements Serializable {

  /**
   * bank_account_type : 74
   * account_name : fTA4TXc8yMOwFCYeGPktOOSbOBei3KA8RAMO9h/5Y0ZM  R46viedrDurCbQTC/sCC9BWuG5oeR8flymK/Z4dN0/7XDSDfqT5Nggq9WwTL+O    ZdMorqTE6Z0G3f3Bi3c+GVvYOCZyVdoinksPEUwyosEtwupM3ufXSZT36DvsS8K9jOBnI  XjGaQtP94FFlh58WR0GuEkMt9KT2NuA+fE4KDScRzSZn3TL5Izdt+7anPS6  Uh4K7wPnUWHGxCQdZu0G4B4YjP7ync8UeLu4jAkCziq4lngnU3rKfFiqwMtyOA1  79x15za7+kWmo2hfaC7xumTqXR7/NyRHpFKXURQFcmmw==
   * account_bank : 工商银行
   * bank_address_code : 110000
   * bank_branch_id : 402713354941
   * bank_name : 施秉县农村信用合作联社城关信用社
   * account_number : d+xT+MQCvrLHUVDWv/8MR/dB7TkXM2YYZlokmXzFsWs35NXUot7C0N  cxIrUF5FnxqCJHkNgKtxa6RxEYyba1+VBRLnqKG2fSy/Y5qDN08Ej9zHCwJjq52Wg1VG8MR   ugli9YMI1fI83KGBxhuXyemgS/hqFKsfYGiOkJqjTUpgY5VqjtL2N4l4z11T0E  CB/aSyVXUysOFGLVfSrUxMPZy6jWWYGvT1+4P633f+R+ki1gT4WF/2KxZOYmli38 5ZgVhcR30mr4/G3HBcxi13zp7FnEeOsLlvBmI1PHN4C7Rsu3WL8sPndjXTd75kPkyjqnoMRrEEaYQE8ZRGYoeorwC+w==
   */

  private String bank_account_type;
  private String account_name;
  private String account_bank;
  private String bank_address_code;
  private String bank_branch_id;
  private String bank_name;
  private String account_number;
}

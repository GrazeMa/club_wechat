package com.club100.project.wechat.utils;

import com.club100.project.wechat.domain.*;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/22 12:29:30
 * @Description: 返回值工具类。
 */
public class ReturnUtils {
    /**
     * 成员变量、静态变量（类变量）。
     */
    static final Integer PARAMETER_IS_EMPTY_FAILURE_CODE = -1001;
    static final Integer PARAMETER_IS_NOT_REASONABLE_FAILURE_CODE = -1002;
    static final Integer NO_SUCH_INFORMATION_CODE = -1003;
    static final Integer CREATE_FAILURE_CODE = -2001;
    static final Integer UPDATE_FAILURE_CODE = -2002;
    static final Integer READ_FAILURE_CODE = -2003;
    static final Integer DELETE_FAILURE_CODE = -2004;
    static final String EMPTY_STRING = "";

    // 私有 ReturnUtils 构造类，让外界不能创建对象，只能静态调用方法。
    private ReturnUtils() {

        throw new UnsupportedOperationException("'ReturnUtils' can't be instantiated~");

    }

    public static String S_returnJsonInfo(Integer codeInteger, String messageString, String dataString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = codeInteger;
        returnInfo.message = messageString;
        returnInfo.data = dataString;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnSignUpUserJsonInfo(Integer signUpUserCountInteger, List<SignUpOrderUser> signUpUsersList) {

        // Initialization.
        ReturnSignUpUserInfo returnSignUpUserInfo = new ReturnSignUpUserInfo();
        // Assign variables.
        returnSignUpUserInfo.signUpUserCount = signUpUserCountInteger;
        returnSignUpUserInfo.signUpUsersList = signUpUsersList;

        return returnSignUpUserInfo.toJson(returnSignUpUserInfo);

    }

    public static String S_returnPizzaGetterUserJsonInfo(Integer pizzaGetterUserCountInteger, List<PizzaGetterUser> pizzaGetterUsersList) {

        // Initialization.
        ReturnPizzaGetterUserInfo returnPizzaGetterUserInfo = new ReturnPizzaGetterUserInfo();
        // Assign variables.
        returnPizzaGetterUserInfo.pizzaGetterUserCount = pizzaGetterUserCountInteger;
        returnPizzaGetterUserInfo.pizzaGetterUsersList = pizzaGetterUsersList;

        return returnPizzaGetterUserInfo.toJson(returnPizzaGetterUserInfo);

    }

    public static String S_returnParameterIsEmptyFailureInfo(String parameterAndTypeString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = PARAMETER_IS_EMPTY_FAILURE_CODE;
        returnInfo.message = ParameterIsEmptyString(parameterAndTypeString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnParameterIsNotReasonableFailureInfo(String parameterAndTypeString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = PARAMETER_IS_NOT_REASONABLE_FAILURE_CODE;
        returnInfo.message = ParameterIsNotReasonableString(parameterAndTypeString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnNoSuchInformationFailureInfo(String tableAndMethodNameString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = NO_SUCH_INFORMATION_CODE;
        returnInfo.message = NoSuchInformationString(tableAndMethodNameString); //
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnCreateFailureInfo(String tableAndMethodNameString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = CREATE_FAILURE_CODE;
        returnInfo.message = CreateFailureString(tableAndMethodNameString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnUpdateFailureInfo(String tableAndMethodNameString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = UPDATE_FAILURE_CODE;
        returnInfo.message = UpdateFailureString(tableAndMethodNameString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnReadFailureInfo(String tableAndMethodNameString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = READ_FAILURE_CODE;
        returnInfo.message = ReadFailureString(tableAndMethodNameString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    public static String S_returnDeleteFailureInfo(String tableAndMethodNameString) {

        // Initialization.
        ReturnInfo returnInfo = new ReturnInfo();
        // Assign variables.
        returnInfo.code = DELETE_FAILURE_CODE;
        returnInfo.message = DeleteFailureString(tableAndMethodNameString);
        returnInfo.data = EMPTY_STRING;

        return returnInfo.toJson(returnInfo);

    }

    private static String ParameterIsEmptyString(String parameterAndTypeString) {

        return "The parameter '" + parameterAndTypeString + "' is empty!!!";

    }

    private static String ParameterIsNotReasonableString(String parameterAndTypeString) {

        return "The parameter '" + parameterAndTypeString + "' is not reasonable!!!";

    }

    private static String NoSuchInformationString(String tableAndMethodNameString) {

        return "There's no such information (" + tableAndMethodNameString + ") in our database!!!";

    }

    private static String CreateFailureString(String tableAndMethodNameString) {

        return "The create operation '" + tableAndMethodNameString + "' failed!!!";

    }

    private static String UpdateFailureString(String tableAndMethodNameString) {

        return "The update operation '" + tableAndMethodNameString + "' failed!!!";

    }

    private static String ReadFailureString(String tableAndMethodNameString) {

        return "The read operation '" + tableAndMethodNameString + "' failed!!!";

    }

    private static String DeleteFailureString(String tableAndMethodNameString) {

        return "The delete operation '" + tableAndMethodNameString + "' failed!!!";

    }

}

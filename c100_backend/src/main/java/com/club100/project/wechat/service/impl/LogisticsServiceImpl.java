package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Logistics;
import com.club100.project.wechat.mapper.LogisticsMapper;
import com.club100.project.wechat.service.ILogisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 11:22:00
 * @Description: 后勤保障服务实现类。
 */
@Slf4j
@Service
public class LogisticsServiceImpl implements ILogisticsService {

    @Autowired
    private LogisticsMapper logisticsMapper;

    @Override
    public int insertLogistics(Logistics logistics) {
        return logisticsMapper.insertLogistics(logistics);
    }

    @Override
    public Logistics selectLogisticsById(Long logisticsId) {
        return logisticsMapper.selectLogisticsById(logisticsId);
    }

    @Override
    public Logistics selectLogisticsIdByName(String logisticsName) {
        return logisticsMapper.selectLogisticsIdByName(logisticsName);
    }

    @Override
    public List<Logistics> selectLogisticsList(Logistics logistics) {
        return logisticsMapper.selectLogisticsList(logistics);
    }

    @Override
    public int updateLogistics(Logistics logistics) {
        return logisticsMapper.updateLogistics(logistics);
    }

    @Override
    public int deleteByLogisticsIds(Long... logisticsId) {
        return logisticsMapper.deleteByLogisticsIds(logisticsId);
    }

    @Override
    public List<Logistics> selectLogisticsDropdownList() {
        return logisticsMapper.selectLogisticsDropdownList();
    }

}

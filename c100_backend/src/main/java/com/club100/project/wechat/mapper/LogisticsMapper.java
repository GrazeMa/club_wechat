package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Logistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 11:09:00
 * @Description: 后勤保障 Mapper 接口。
 */
public interface LogisticsMapper {

    int insertLogistics(Logistics logistics);

    Logistics selectLogisticsById(Long logisticsId);

    Logistics selectLogisticsIdByName(String logisticsName);

    List<Logistics> selectLogisticsList(Logistics logistics);

    int updateLogistics(Logistics logistics);

    int deleteByLogisticsIds(Long... logisticsId);

    List<Logistics> selectLogisticsDropdownList();

}

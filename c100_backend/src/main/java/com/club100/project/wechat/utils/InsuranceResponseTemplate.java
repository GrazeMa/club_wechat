package com.club100.project.wechat.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceResponseTemplate {

  public Integer respCode;

  public String respMsg;

  public Map<String,Object> data;

}

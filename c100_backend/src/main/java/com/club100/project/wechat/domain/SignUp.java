package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 14:49:02
 * @Description: 报名 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUp extends BaseEntity implements Serializable {

    private Long signUpId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long activityId; // 活动主键。
    private Long userId; // 用户主键。

    public String toJson(SignUp signUp) {

        // SignUp 对象转 JSON。
        return JSON.toJSONString(signUp);

    }

}

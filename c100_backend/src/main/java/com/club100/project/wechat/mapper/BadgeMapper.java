package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Badge;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 15:28:37
 * @Description: 徽章 Mapper 接口。
 */
public interface BadgeMapper {

    List<Badge> selectBadgeList(Badge badge);

    List<Badge> selectBadgeWithCityList(Badge badge);

    List<Badge> selectBadgeByCityTypeOnline(Integer cityType);

    List<Badge> selectBadgeByCityTypeOffline(Integer cityType);

    List<Badge> selectBadgeExcludeCityTypeOnline(Integer cityType);

    List<Badge> selectBadgeExcludeCityTypeOffline(Integer cityType);

    Badge selectBadgeById(Long badgeId);

    Badge selectBadgeByRouteId(Long routeId);

    int insertBadge(Badge badge);

}

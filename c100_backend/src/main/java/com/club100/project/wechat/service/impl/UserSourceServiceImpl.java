package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.UserSource;
import com.club100.project.wechat.mapper.UserSourceMapper;
import com.club100.project.wechat.service.IUserSourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/25 16:09:27
 * @Description: 用户来源服务实现类。
 */
@Slf4j
@Service
public class UserSourceServiceImpl implements IUserSourceService {

    @Autowired
    private UserSourceMapper userSourceMapper;

    @Override
    public int insertUserSource(UserSource userSource) {
        return userSourceMapper.insertUserSource(userSource);
    }

    @Override
    public UserSource selectUserSourceById(Long userSourceId) {
        return userSourceMapper.selectUserSourceById(userSourceId);
    }

    @Override
    public List<UserSource> selectUserSource(UserSource userSource) {
        return userSourceMapper.selectUserSource(userSource);
    }

    @Override
    public int updateUserSource(UserSource userSource) {
        return userSourceMapper.updateUserSource(userSource);
    }

}

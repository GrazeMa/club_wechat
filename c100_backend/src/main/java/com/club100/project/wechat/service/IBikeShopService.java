package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BikeShop;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 11:14:25
 * @Description: 车店服务类。
 */
public interface IBikeShopService {

    /**
     * 插入一条车店信息。
     *
     * @param bikeShop
     * @return
     */
    int insertBikeShop(BikeShop bikeShop);

    /**
     * 通过主键查询一条车店信息。
     *
     * @param bikeId
     * @return
     */
    BikeShop selectBikeShopById(Long bikeId);

    /**
     * 查询车店信息。
     *
     * @param bikeShop
     * @return
     */
    List<BikeShop> selectBikeShopList(BikeShop bikeShop);

    /**
     * 带权限查询车店信息。
     *
     * @param bikeShop
     * @return
     */
    List<BikeShop> selectBikeShopListWithSysUserId(BikeShop bikeShop);

    /**
     * 修改车店信息。
     *
     * @param bikeShop
     * @return
     */
    int updateBikeShop(BikeShop bikeShop);

    /**
     * 根据主键删除车店信息。
     *
     * @param bikeId(s)
     * @return
     */
    int deleteByBikeShopIds(Long... bikeId);

    /**
     * 获取车店信息单选下拉框选项。
     *
     * @param bikeShop
     * @return
     */
    List<BikeShop> selectBikeShopDropdownList(BikeShop bikeShop);

}

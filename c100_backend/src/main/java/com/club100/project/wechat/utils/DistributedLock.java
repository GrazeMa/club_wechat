package com.club100.project.wechat.utils;

public interface DistributedLock {

  /**
   * 获取锁
   *
   * @return
   * @throws InterruptedException
   * @author wanghui
   * @time 2018年5月26日 上午11:02:54
   */
  public boolean acquire();

  /**
   * 释放锁
   *
   * @author wanghui
   * @time 2018年5月26日 上午11:02:59
   */
  public void release();

}
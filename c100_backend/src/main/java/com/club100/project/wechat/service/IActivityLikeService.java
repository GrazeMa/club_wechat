package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityLike;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/03/01 19:47:56
 * @Description: 活动感兴趣服务类。
 */
public interface IActivityLikeService {

    /**
     * @param activityLike
     * @return
     * @Description 根据条件查询活动感兴趣个数。
     */
    int selectActivityLikeCountList(ActivityLike activityLike);

    /**
     * @param activityLike
     * @return
     * @Description 查询活动感兴趣信息。
     */
    List<ActivityLike> selectActivityLike(ActivityLike activityLike);

    /**
     * @param activityLike
     * @return
     * @Description 插入一条活动感兴趣信息。
     */
    int insertActivityLike(ActivityLike activityLike);

    /**
     * @param activityLike
     * @return
     * @Description 更新一条活动感兴趣信息。
     */
    int updateActivityLike(ActivityLike activityLike);

}

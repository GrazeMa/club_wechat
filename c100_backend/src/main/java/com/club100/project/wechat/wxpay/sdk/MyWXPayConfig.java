package com.club100.project.wechat.wxpay.sdk;

import com.alibaba.fastjson.JSON;
import com.club100.common.constant.WechatConstant;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class MyWXPayConfig extends WXPayConfig{

  @Override
  public String getAppID() {
    return WechatConstant.APPID;
  }

  @Override
  public String getMchID() {
    return WechatConstant.MCH_ID;
  }

  @Override
  public String getKey() {
    return WechatConstant.MCH_KEY;
  }


  @Override
  public InputStream getCertStream() {
//    String certPath = "apiclient_cert.p12";
    return Thread.currentThread().getContextClassLoader().getResourceAsStream("keystore/apiclient_cert2.p12");
  }

  /**
   * 获取证书 byte[]
   *
   * @return
   * @throws IOException
   */
  public byte[] getCertByte() throws IOException {
     return  IOUtils.toByteArray(getCertStream());
  }

  @Override
  public IWXPayDomain getWXPayDomain() {
    return new IWXPayDomain() {
      @Override
      public void report(String domain, long elapsedTimeMillis, Exception ex) {
        log.info("report");
      }

      @Override
      public DomainInfo getDomain(WXPayConfig config) {
        return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
      }
    };
  }


  /**
   * 获取沙箱密钥
   *
   * @return
   * @throws Exception
   */
  public static String getSignkey() throws Exception {
    MyWXPayConfig config = new MyWXPayConfig();
    WXPay pay = new WXPay(config,false,true);

    int readTimeoutMs = 5000;
    int connectTimeoutMs = 5000;
    Map<String, String> reqData = new HashMap<>();
    reqData.put("mch_id", config.getMchID());
    reqData.put("nonce_str", WXPayUtil.generateNonceStr());
    String urlSuffix = "/sandboxnew/pay/getsignkey";
    String string = pay.requestWithoutCert(urlSuffix, pay.fillRequestData(reqData), connectTimeoutMs,
            readTimeoutMs);
    Map<String, String> map = WXPayUtil.xmlToMap(string);
    return map.get("sandbox_signkey");
  }

  public static void main(String[] args) {
    //
    try {
      System.out.println(getSignkey());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 退款
   * @throws Exception
   */
  private static void testRefund() throws Exception{
    Map<String, String> reqData = new HashMap<>();
    reqData.put("out_trade_no", "000");
    reqData.put("out_refund_no", "000");
    reqData.put("total_fee", "100");
    reqData.put("refund_fee", "100");

    WXPay pay = new WXPay(new MyWXPayConfig());//关键在此构造
    Map<String, String> map = pay.refund(reqData);

    System.out.println(map);
  }
  /**
   * 沙箱退款
   * @throws Exception
   */
  private static void testRefundBySandBox() throws Exception{
    Map<String, String> reqData = new HashMap<>();
    reqData.put("out_trade_no", "000");
    reqData.put("out_refund_no", "000");
    reqData.put("total_fee", "100");
    reqData.put("refund_fee", "50");

    WXPay pay = new WXPay(new MyWXPayConfig(), false, true);//关键在此构造
    Map<String, String> map = pay.refund(reqData);
    System.out.println(JSON.toJSONString(map));
  }
  private static void testRefundQueryBySandBox() throws Exception{
    Map<String, String> reqData = new HashMap<>();
    reqData.put("out_trade_no", "000");

    WXPay pay = new WXPay(new MyWXPayConfig(), false, true);//关键在此构造
    Map<String, String> map = pay.refundQuery(reqData);
    System.out.println(JSON.toJSONString(map));
  }
}

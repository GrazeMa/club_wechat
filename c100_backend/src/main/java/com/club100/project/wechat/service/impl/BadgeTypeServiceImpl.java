package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BadgeType;
import com.club100.project.wechat.mapper.BadgeTypeMapper;
import com.club100.project.wechat.service.IBadgeTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/28 16:58:56
 * @Description: 徽章类型服务实现类。
 * @Version: 1.0
 */
@Slf4j
@Service
public class BadgeTypeServiceImpl implements IBadgeTypeService {

    @Autowired
    private BadgeTypeMapper badgeTypeMapper;

    @Override
    public List<BadgeType> selectBadgeTypeList(BadgeType badgeType) {
        return badgeTypeMapper.selectBadgeTypeList(badgeType);
    }

    @Override
    public BadgeType selectBadgeTypeById(Long badgeTypeId) {
        return badgeTypeMapper.selectBadgeTypeById(badgeTypeId);
    }

    @Override
    public List<BadgeType> selectBadgeTypeByBadgeId(Long badgeId) {
        return badgeTypeMapper.selectBadgeTypeByBadgeId(badgeId);
    }

    @Override
    public int insertBadgeType(BadgeType badgeType) {
        return badgeTypeMapper.insertBadgeType(badgeType);
    }

}

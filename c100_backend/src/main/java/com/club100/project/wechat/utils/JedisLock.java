package com.club100.project.wechat.utils;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
public class JedisLock implements DistributedLock {

  @Resource
  private StringRedisTemplate stringRedisTemplate;

  /**
   * 锁的键值
   */
  private String lockKey;
  /**
   * 锁超时，防止线程在入锁以后，无限的执行等待
   */
  private int expireMsecs = 10 * 1000;
  /**
   * 锁等待，防止线程饥饿
   */
  private int timeoutMsecs = 10 * 1000;
  /**
   * 是否已经获取锁
   */
  private boolean locked = false;

  /**
   * 获取指定键值的锁
   *
   * @param lockKey 锁的键值
   */
  public JedisLock(String lockKey) {
    this.lockKey = lockKey;
  }

  /**
   * 获取指定键值的锁,同时设置获取锁超时时间
   *
   * @param lockKey      锁的键值
   * @param timeoutMsecs 获取锁超时时间
   */
  public JedisLock(String lockKey, int timeoutMsecs) {
    this.lockKey = lockKey;
    this.timeoutMsecs = timeoutMsecs;
  }

  /**
   * 获取指定键值的锁,同时设置获取锁超时时间和锁过期时间
   *
   * @param lockKey      锁的键值
   * @param timeoutMsecs 获取锁超时时间
   * @param expireMsecs  锁失效时间
   */
  public JedisLock(String lockKey, int timeoutMsecs, int expireMsecs) {
    this.lockKey = lockKey;
    this.timeoutMsecs = timeoutMsecs;
    this.expireMsecs = expireMsecs;
  }

  public String getLockKey() {
    return lockKey;
  }

  /**
   * @return true if lock is acquired, false acquire timeouted
   * @throws InterruptedException in case of thread interruption
   */
  @Override
  public synchronized boolean acquire() {
    int timeout = timeoutMsecs;
    System.out.println("进入" + Thread.currentThread().getName());
    try {
      while (timeout >= 0) {
        long expires = System.currentTimeMillis() + expireMsecs + 1;
        String expiresStr = String.valueOf(expires); //锁到期时间

        if (stringRedisTemplate.opsForValue().setIfAbsent(lockKey, expiresStr)) {
          // lock acquired
          locked = true;
          setLockExpireTime();
          return true;
        }

        String currentValueStr = stringRedisTemplate.opsForValue().get(lockKey); //redis里的时间
        if (currentValueStr != null && Long.parseLong(currentValueStr) < System.currentTimeMillis()) {
          //判断是否为空，不为空的情况下，如果被其他线程设置了值，则第二个条件判断是过不去的
          // lock is expired

          String oldValueStr = stringRedisTemplate.opsForValue().getAndSet(lockKey, expiresStr);
          //获取上一个锁到期时间，并设置现在的锁到期时间，
          //只有一个线程才能获取上一个线上的设置时间，因为jedis.getSet是同步的
          if (oldValueStr != null && oldValueStr.equals(currentValueStr)) {
            //如过这个时候，多个线程恰好都到了这里，但是只有一个线程的设置值和当前值相同，他才有权利获取锁
            // lock acquired
            locked = true;
            setLockExpireTime();
            return true;
          }
        }
        timeout -= 100;
        Thread.sleep(100);
      }
    } catch (Exception e) {
      log.error("",e);
    }
    System.out.println("推出" + Thread.currentThread().getName());
    return false;
  }

  /**
   * 释放锁
   */
  @Override
  public synchronized void release() {

    try {
      if (locked) {
        String currentValueStr = stringRedisTemplate.opsForValue().get(lockKey); //redis里的时间
        //校验是否超过有效期，如果不在有效期内，那说明当前锁已经失效，不能进行删除锁操作
        if (currentValueStr != null && Long.parseLong(currentValueStr) > System.currentTimeMillis()) {
          stringRedisTemplate.delete(lockKey);
          locked = false;
        }
      }
    } catch (Exception e) {
      log.error("释放锁失败",e);
    }
  }

  private void setLockExpireTime() {
    stringRedisTemplate.expire(lockKey, expireMsecs, TimeUnit.MILLISECONDS);
  }

//  public static void main(String[] args) {
//    ApplicationContext applicationContext = SpringApplication.run(ServerApplication.class, new String[]{});
//    SpringContextUtil.setApplicationContext(applicationContext);
//    for (int i = 0; i < 30; i++) {
//        new Thread(() -> {
//          System.out.println("进入线程-"+Thread.currentThread().getName()+System.currentTimeMillis());
//          JedisLock jedisLock=new JedisLock("testLock");
//          Boolean bl=jedisLock.acquire();
//          System.out.println(Thread.currentThread().getName()+"获取线程："+bl+System.currentTimeMillis());
//          try {
//            Thread.sleep(15000);
//          }
//          catch (Exception e){}
//          jedisLock.release();
//          System.out.println(Thread.currentThread().getName()+"释放结束"+System.currentTimeMillis()
//          );
//        }).start();
//    }
//  }

}

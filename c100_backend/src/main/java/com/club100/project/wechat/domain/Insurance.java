package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/11 21:04:32
 * @Description: 骑行意外险 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Insurance extends BaseEntity implements Serializable {

    private Long insuranceId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private String insuranceContent; // 骑行意外险内容。

    public String toJson(Insurance insurance) {

        // Insurance 对象转 JSON。
        return JSON.toJSONString(insurance);

    }

}

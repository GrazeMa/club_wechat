package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/30 19:26:49
 * @Description: 二维码参数统计 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Source extends BaseEntity implements Serializable {

    private Long sourceId; // Primary Key ID。

    private Integer sourceCount; // 数量。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private String parameterDescription; // 参数解释。
    private String parameterName; // 参数名称。
    private String parameterValue; // 参数值。

    public String toJson(Source source) {

        // Source 对象转 JSON。
        return JSON.toJSONString(source);

    }

}

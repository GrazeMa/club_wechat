package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/14 12:10:07
 * @Description: 退款政策 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RefundPolicy {

    private Long refundPolicyId; // Primary Key ID。

    private Double refundPercentage; // 退款政策百分比。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private LocalDateTime policyDate; // 退款政策时间。
    private Long activityId; // 活动主键。
    private Long sysUserId;
    private String activityName;

    public String toJson(RefundPolicy refundPolicy) {

        // RefundPolicy 对象转 JSON。
        return JSON.toJSONString(refundPolicy);

    }

}

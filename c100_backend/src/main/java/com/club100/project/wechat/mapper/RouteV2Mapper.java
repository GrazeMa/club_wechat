package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RouteV2;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 16:25:55
 * @Description: 路线V2 Mapper 接口。
 */
public interface RouteV2Mapper {

    List<RouteV2> selectRouteV2AtMostTwoList(RouteV2 routeV2);

    List<RouteV2> selectRouteV2List(@Param("cityType") Integer cityType, @Param("routeDistance1Begin") Integer routeDistance1Begin, @Param("routeDistance1End") Integer routeDistance1End, @Param("routeDistance2Begin") Integer routeDistance2Begin, @Param("routeDistance2End") Integer routeDistance2End, @Param("routeDistance3Begin") Integer routeDistance3Begin, @Param("routeDistance3End") Integer routeDistance3End, @Param("routeDistance4Begin") Integer routeDistance4Begin, @Param("routeDistance4End") Integer routeDistance4End, @Param("routeDistance5Begin") Integer routeDistance5Begin, @Param("routeDistance5End") Integer routeDistance5End, @Param("routeDistance6Begin") Integer routeDistance6Begin, @Param("routeDistance6End") Integer routeDistance6End, @Param("routeTerrain") Integer[] routeTerrain, @Param("routeType") Integer[] routeType);

}

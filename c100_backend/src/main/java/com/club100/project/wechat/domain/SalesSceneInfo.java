package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesSceneInfo extends BaseEntity implements Serializable {

  /**
   * store_name : 爱烧烤
   * store_url : http://www.qq.com
   * store_qr_code : jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_PvWkfSCJ3z  VIn001D8daLC-ehEuo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ
   */

  private String store_name;
  private String store_url;
  private String store_qr_code;
  private String merchant_shortname;
}

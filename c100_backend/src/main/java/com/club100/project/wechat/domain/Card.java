package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/16 17:33:19
 * @Description: 主页卡片 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Card extends BaseEntity implements Serializable {

    private Long cardId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private String cardTitle; // 卡片标题。
    private String cardContent; // 卡片内容。
    private String cardPicUrl; // 卡片图片 URL。
    private String cardLink; // 卡片跳转 URL。

    public String toJson(Card card) {

        // Card 对象转 JSON。
        return JSON.toJSONString(card);

    }

}

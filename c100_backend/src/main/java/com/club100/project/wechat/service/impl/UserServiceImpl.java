package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.User;
import com.club100.project.wechat.mapper.UserMapper;
import com.club100.project.wechat.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 20:20:05
 * @Description: 用户服务实现类。
 */
@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectUser(User user) {
        return userMapper.selectUser(user);
    }

    @Override
    public int updateUserCity(User user) {
        return userMapper.updateUserCity(user);
    }

    @Override
    public int updateUserMarkTimes(User user) {
        return userMapper.updateUserMarkTimes(user);
    }

    @Override
    public int insert(User user) {
        return userMapper.insertUser(user);
    }

    @Override
    public User selectOneById(User user) {
        return userMapper.selectOneById(user);
    }

    @Override
    @Transactional
    public int deleteByUserIds(Long... id) {
        return userMapper.deleteByUserIds(id);
    }

}

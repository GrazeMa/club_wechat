package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityLogistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 12:11:57
 * @Description: 活动后勤保障关联服务类。
 */
public interface IActivityLogisticsService {

    /**
     * 插入一条活动后勤保障关联信息。
     *
     * @param activityLogistics
     * @return
     */
    int insertActivityLogistics(ActivityLogistics activityLogistics);

    /**
     * 批量插入活动后勤保障关联信息。
     *
     * @param activityLogisticsList
     * @return
     */
    int insertActivityLogisticsInBatch(List<ActivityLogistics> activityLogisticsList);

    /**
     * 查询活动后勤保障关联信息。
     *
     * @param activityLogistics
     * @return
     */
    List<ActivityLogistics> selectActivityLogisticsList(ActivityLogistics activityLogistics);

    /**
     * 查询活动后勤保障关联信息。
     *
     * @param activityLogistics
     * @return
     */
    List<ActivityLogistics> selectActivityLogisticsListWithPrivilege(ActivityLogistics activityLogistics);

    /**
     * 删除活动后勤保障关联信息。
     *
     * @param activityId
     * @return
     */
    int deleteActivityLogistics(Long... activityId);

}

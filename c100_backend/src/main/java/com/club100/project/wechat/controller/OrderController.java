package com.club100.project.wechat.controller;

import com.club100.common.utils.poi.ExcelUtil;
import com.club100.framework.aspectj.lang.annotation.Log;
import com.club100.framework.aspectj.lang.enums.BusinessType;
import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.system.domain.SysRole;
import com.club100.project.wechat.domain.Order;
import com.club100.project.wechat.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/03 21:51:07
 * @Description: 订单控制器。
 */
@RequestMapping("/v1/order")
@RestController
public class OrderController extends BaseController {

    // Order information.
    @Autowired
    private IOrderService orderService;

    @GetMapping("/listOrders")
    public TableDataInfo listOrders(Order order) {

        startPage();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        order.setSysUserId(sysUserIdLong);

        List<Order> ordersList = orderService.selectOrderList(order);

        return getDataTable(ordersList);

    }

    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @GetMapping("/exportOrders")
    public AjaxResult exportOrders(Order order) {

        Long sysUserIdLong = P_GetLoginSystemUserId();
        order.setSysUserId(sysUserIdLong);

        List<Order> ordersList = orderService.selectOrderList(order);
        ExcelUtil<Order> util = new ExcelUtil<>(Order.class);
        return util.exportExcel(ordersList, "订单数据");

    }

}

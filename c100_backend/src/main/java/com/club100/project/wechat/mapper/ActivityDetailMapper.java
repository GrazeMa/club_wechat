package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityDetail;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/21 17:41:10
 * @Description: 活动详情 Mapper 接口。
 */
public interface ActivityDetailMapper {

    ActivityDetail selectActivityDetailById(ActivityDetail activityDetail);

}

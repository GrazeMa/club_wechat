package com.club100.project.wechat.wxpay.sdk;

import com.club100.common.constant.WeChatMiniConstants;
import com.club100.common.constant.WechatConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/11 1:36 PM
 * @Description: 将自己的参数传入 WXPayConfig。
 * @Version: 1.0
 */
@Component("MyWeChatMiniPayConfig")
@Slf4j
public class MyWeChatMiniPayConfig extends WXPayConfig {

    @Override
    public String getAppID() {
        return WeChatMiniConstants.MINI_APP_ID; // 小程序 ID 。
    }

    @Override
    public String getMchID() {
        return WeChatMiniConstants.MERCHANT_ID; // 商户 ID。
    }

    @Override
    public String getKey() {
        return WeChatMiniConstants.MERCHANT_KEY; // 商户证书位置设置的密钥。
    }

    @Override
    public InputStream getCertStream() {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream("keystore/business/apiclient_cert.p12");
    }

    /**
     * 获取证书 byte[]
     *
     * @return
     * @throws IOException
     */
    public byte[] getCertByte() throws IOException {
        return IOUtils.toByteArray(getCertStream());
    }

    @Override
    public IWXPayDomain getWXPayDomain() {
        return new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {
                log.info(" == report. == ");
            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
    }

    /*private byte[] certData;

    public MyWeChatMiniPayConfig() throws Exception {

        InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("keystore/apiclient_cert.p12"); // 读取存放在 resource 里的证书。
        this.certData = IOUtils.toByteArray(certStream);
        certStream.close();

    }

    @Override
    public String getAppID() {
        return WeChatMiniConstants.MINI_APP_ID; // 小程序 ID 。
    }

    @Override
    public String getMchID() {
        return WeChatMiniConstants.MERCHANT_ID; // 商户 ID。
    }

    @Override
    public String getKey() {
        return WeChatMiniConstants.MERCHANT_KEY; // 商户证书位置设置的密钥。
    }

    @Override
    public InputStream getCertStream() {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream("keystore/apiclient_cert.p12");
    }

    @Override
    public IWXPayDomain getWXPayDomain() {
        // 必须实例化，否则 WxPay 初始化失败。
        return new IWXPayDomain() {

            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {
                log.info(" == report == ");
            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }

        };
    }*/

}

/*
@Component("myConfig")
public class MyConfig extends WXPayConfig {
    private byte[] certData;

    public MyConfig() throws Exception {
        InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("apiclient_cert.p12");//读取方才存放在resource西边的证书
        this.certData = IOUtils.toByteArray(certStream);
        certStream.close();
    }

    @Override
    public String getAppID() {
        return "你自己的小程序Id号";
    }

    public String getMchID() {
        return "你自己的商户ID号";
    }

    public String getKey() {
        return "刚才在商户证书位置设置的密钥";
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    @Override
    IWXPayDomain getWXPayDomain() {
        //必须实例化，否则WxPay初始化失败
        IWXPayDomain iwxPayDomain = new IWXPayDomain() {
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
        return iwxPayDomain;
    }
}*/

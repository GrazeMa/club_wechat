package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/10 11:51:43
 * @Description: 退款政策返回值。
 */
@AllArgsConstructor
public class ReturnRefundPolicyInfo implements Serializable {

    public Long returnRefundPolicyInfoId;
    public String returnActivityNameInfo;
    public String[] returnRefundPolicyInfo;

    public String toJson(ReturnRefundPolicyInfo returnRefundPolicyInfo) {

        // ReturnRefundPolicyInfo 对象转 JSON。
        return JSON.toJSONString(returnRefundPolicyInfo);

    }

}

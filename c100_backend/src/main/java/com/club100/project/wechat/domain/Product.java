package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 17:04:36
 * @Description: 商品 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product extends BaseEntity implements Serializable {

    private Long productId; // Primary Key ID。

    private Boolean ifChoose; // 是否必选。
    private Double productPrice; // 商品价格。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long activityId; // 活动主键。
    private String productName; // 商品名称。
    private Long sysUserId;
    private String activityName;

    public String toJson(Product product) {

        // Product 对象转 JSON。
        return JSON.toJSONString(product);

    }

}

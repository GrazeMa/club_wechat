package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/26 11:17:29
 * @Description: 披萨文件 Entity。
 */
@NoArgsConstructor
@AllArgsConstructor
public class PizzaFile extends BaseEntity implements Serializable {

    public Integer max_age;
    public String fileid;

    public String toJson(PizzaFile pizzaFile) {

        // PizzaFile 对象转 JSON。
        return JSON.toJSONString(pizzaFile);

    }

}

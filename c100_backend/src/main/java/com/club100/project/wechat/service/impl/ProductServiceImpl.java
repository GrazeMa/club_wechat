package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Product;
import com.club100.project.wechat.mapper.ProductMapper;
import com.club100.project.wechat.service.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/12 23:33:27
 * @Description: 商品服务实现类。
 */
@Slf4j
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public int insertProduct(Product product) {
        return productMapper.insertProduct(product);
    }

    @Override
    public int insertProductInBatch(List<Product> productList) {
        return productMapper.insertProductInBatch(productList);
    }

    @Override
    public List<Product> selectProductList(Product product) {
        return productMapper.selectProductList(product);
    }

    @Override
    public List<Product> selectProductListWithPrivilege(Product product) {
        return productMapper.selectProductListWithPrivilege(product);
    }

    @Override
    public int deleteProduct(Long... activityId) {
        return productMapper.deleteProduct(activityId);
    }

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 20:35:02
 * @Description: 城市 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City extends BaseEntity implements Serializable {

    private Long cityId; // Primary Key ID。

    /*private Date createTime; // 城市创建时间。
    private Date updateTime; // 城市修改时间。*/
    private Integer citySequence; // 城市展示顺序。
    private Integer state; // 状态（1：可用；2：废弃；3：上线准备。）。
    private String cityNameCH; // 城市名（中文）。
    private String cityNameEN; // 城市名（英语）。
    private String cityPictureUrl; // 城市图片链接 URL。

    public String toJson(City city) {

        // City 对象转 JSON。
        return JSON.toJSONString(city);

    }

}

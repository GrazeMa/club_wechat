package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 14:55:22
 * @Description: 收藏路线 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavoriteRoute extends BaseEntity implements Serializable {

    private Long favoriteRouteId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 收藏路线状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主表主键。
    private Long userId; // 用户主键。

    public String toJson(FavoriteRoute favoriteRoute) {

        // FavoriteRoute 对象转 JSON。
        return JSON.toJSONString(favoriteRoute);

    }

}

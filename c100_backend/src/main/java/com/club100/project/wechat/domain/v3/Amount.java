package com.club100.project.wechat.domain.v3;

import lombok.Data;

@Data
public class Amount {

  private int total;
  private String currency;
}

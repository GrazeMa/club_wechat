package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RouteAndDetail;
import com.club100.project.wechat.mapper.RouteAndDetailMapper;
import com.club100.project.wechat.service.IRouteAndDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 19:41:35
 * @Description: 路线和路线详情服务实现类。
 */
@Slf4j
@Service
public class RouteAndDetailServiceImpl implements IRouteAndDetailService {

    @Autowired
    private RouteAndDetailMapper routeAndDetailMapper;

    @Override
    public RouteAndDetail selectRouteAndDetail(RouteAndDetail routeAndDetail) {
        return routeAndDetailMapper.selectRouteAndDetail(routeAndDetail);
    }

    @Override
    public List<RouteAndDetail> selectRouteAndDetail2(RouteAndDetail routeAndDetail) {
        return routeAndDetailMapper.selectRouteAndDetail2(routeAndDetail);
    }

}

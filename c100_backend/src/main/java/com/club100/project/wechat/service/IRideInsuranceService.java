package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Insurance;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/11 21:25:34
 * @Description: 骑行意外险服务类。
 */
public interface IRideInsuranceService {

    /**
     * 插入一条骑行意外险信息。
     *
     * @param insurance
     * @return
     */
    int insertInsurance(Insurance insurance);

    /**
     * 通过主键查询一条骑行意外险信息。
     *
     * @param insuranceId
     * @return
     */
    Insurance selectInsuranceById(Long insuranceId);

}

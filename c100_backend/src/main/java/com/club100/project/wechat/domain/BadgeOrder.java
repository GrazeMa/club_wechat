package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.aspectj.lang.annotation.Excel;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/20 15:17:56
 * @Description: 徽章订单 Entity。
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeOrder extends BaseEntity implements Serializable {

    @Excel(name = "订单序号", cellType = Excel.ColumnType.NUMERIC)
    private Long badgeOrderId; // Primary Key ID。

    //    @Excel(name = "时间")
    //    private Date createTime;
    @Excel(name = "收货人信息")
    private String consigneeInfo; // 收货人信息。
    private Double badgePrice; // 徽章价格。
    private Integer badgeOrderState; // 订单状态（0：生成待支付订单；1：已成功支付；2：支付失败。）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long badgeId; // 徽章主键 ID。
    private Long productId; // 产品 ID。
    private Long userId; // 用户主键 ID。
    @Excel(name = "徽章名称")
    private String badgeName; // 徽章名称。
    private String badgePictureUrl; // 徽章图片链接 URL。
    @Excel(name = "徽章类型")
    private String badgeTypeName; // 徽章类型名。
    private String badgeTypePictureUrl; // 徽章图片链接 URL。
    //    @Excel(name = "收货人地址")
    private String consigneeAddress; // 收货人详细地址。
    //    @Excel(name = "区域")
    private String consigneeDistrict; // 收货人所在地区。
    private String consigneeName; // 收货人姓名。
    private String consigneeMobile; // 收货人手机号。
    private String outTradeNo; // 订单号。
    @Excel(name = "数量")
    private Integer badgeNumber; // 徽章个数。
    @Excel(name = "价格")
    private Double totalFeeYuan; // 支付金额（单位：元）。
    @Excel(name = "快递公司")
    private String packageName; // 快递公司。
    @Excel(name = "快递单号")
    private String packageOrderNumber; // 快递单号。
    private String productDescription; // 产品描述。
    private String totalFeeString; // 支付金额（单位：分）。
    private String tradeType; // 交易类型。

    public String toJson(BadgeOrder badgeOrder) {

        // BadgeOrder 对象转 JSON。
        return JSON.toJSONString(badgeOrder);

    }

}

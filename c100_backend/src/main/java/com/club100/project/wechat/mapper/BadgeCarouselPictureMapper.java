package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BadgeCarouselPicture;

import java.util.List;

public interface BadgeCarouselPictureMapper {

    List<BadgeCarouselPicture> selectBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture);

    List<BadgeCarouselPicture> selectBadgeCarouselPictureByBadgeId(Long badgeId);

    int insertBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture);

    int insertBadgeCarouselPictureInBatch(List<BadgeCarouselPicture> badgeCarouselPictureList);

    int deleteBadgeCarouselPicture(Long... badgeId);

}

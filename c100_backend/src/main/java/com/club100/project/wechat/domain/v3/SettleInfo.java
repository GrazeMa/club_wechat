package com.club100.project.wechat.domain.v3;

import lombok.Data;

@Data
public class SettleInfo {

  private boolean profit_sharing;
  private int subsidy_amount;
}

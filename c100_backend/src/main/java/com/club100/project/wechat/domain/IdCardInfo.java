package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdCardInfo extends BaseEntity implements Serializable {

  /**
   * id_card_copy : jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_PvWkfSCJ3zVIn001D8daLC-eh  Euo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ
   * id_card_national : 47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf-4KGpr  rKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4
   * id_card_name : pVd1HJ6zyvPedzGaV+X3qtmrq9bb9tPROvwia4ibL+F6mfjb  zQIzfb3HHLEjZ4YiR/cJiCrZxnAqi+pjeKIEdkwzXRAI7FUhr fPK3SNjaBTEu9GmsugMIA9r3x887Q+ODuC8HH2nzAn7NGpE/e3yiHgWhk0ps5k5DP/2qIdG dONoDzZelrxCl/NWWNUyB93K9F+jC1JX2IMttdY+aQ6zBlw0 xnOiNW6Hzy7UtC+xriudjD5APomty7/mYNxLMpRSvWKIjOv/69bDnuC4EL5Kz4jBHLiCyOb+t I0m2qhZ9evAM+Jv1z0NVa8MRtelw/wDa4SzfeespQO/0kjiwfqdfg==
   * id_card_number : UZFETyabYFFlgvGh6R4vTzDELiPas3jC94/srAbSaPaWSM+1rh  GLcsybPLeJdVOpCeDxNBMDK+/N0nOiZZ4ka9+5RgzvA2rJx+NztYUbN    209rq0Y/NP50T9yk0m6A4xUoFWgk/8qteRHtP5VHahNhSh8nHo31V33t8edSlN9HJG6diRj7  p5JPImDyM1q56+p25edl3+cOtuZtj8TJDl/hB+GaWve9X1WUpkZbKlJgB  xp+XhaW707k9XrILvfD+rSGTOeU/ev4/OiEb5W4WPGJ+3iLoQvhnz3+aQZX9+gn9uRz  WcHu2Kr17fhsM+MRkgVcwzI2UqhR9iuGTunRPRVFg==
   * id_card_valid_time : 2026-06-06
   */

  private String id_card_copy;
  private String id_card_national;
  private String id_card_name;
  private String id_card_number;
  private String id_card_valid_time;
}

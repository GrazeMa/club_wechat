package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/19 15:19:28
 * @Description: 徽章轮播图 Entity。
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeCarouselPicture extends BaseEntity implements Serializable {

    private Long badgeCarouselPictureId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long badgeId; // 徽章主键 ID。
    private String badgePictureUrl; // 徽章轮播图片链接 URL。

    public String toJson(BadgeCarouselPicture badgeCarouselPicture) {

        // BadgeCarouselPicture 对象转 JSON。
        return JSON.toJSONString(badgeCarouselPicture);

    }

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/25 13:56:01
 * @Description: 用户来源 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSource extends BaseEntity implements Serializable {

    private Long userSourceId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Integer totalAmount; // 用户来源总数。
    private String qrCodeParameterName; // 小程序码参数名。
    private String qrCodeParameterValue; // 小程序码参数值。

    public String toJson(UserSource userSource) {

        // UserSource 对象转 JSON。
        return JSON.toJSONString(userSource);

    }

}

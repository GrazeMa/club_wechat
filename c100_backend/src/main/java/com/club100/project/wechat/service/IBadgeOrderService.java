package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BadgeOrder;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/20 15:53:56
 * @Description: 徽章订单服务类。
 */
public interface IBadgeOrderService {

    /**
     * @param badgeOrderId
     * @return
     * @Description 根据主键查询徽章订单信息。
     */
    BadgeOrder selectBadgeById(Long badgeOrderId);

    /**
     * @param badgeOrder
     * @return
     * @Description 查询徽章订单信息。
     */
    List<BadgeOrder> selectBadgeOrderList(BadgeOrder badgeOrder);

    /**
     * @param badgeOrder
     * @return
     * @Description 根据条件查询一条徽章订单信息。
     */
    BadgeOrder selectOneBadgeOrderByBadgeIdAndUserId(BadgeOrder badgeOrder);

    /**
     * @param badgeOrder
     * @return
     * @Description 插入一条徽章订单信息。
     */
    int insertBadgeOrder(BadgeOrder badgeOrder);

    /**
     * @param badgeOrder
     * @return
     * @Description 根据主键更新一条徽章订单信息。
     */
    int updateBadgeOrder(BadgeOrder badgeOrder);

    /**
     * @param badgeOrder
     * @return
     * @Description 根据订单号更新一条徽章订单信息。
     */
    int updateBadgeOrderByOrderId(BadgeOrder badgeOrder);

}

package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Order;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/03 22:12:15
 * @Description: 订单 Mapper 接口。
 */
public interface OrderMapper {

    List<Order> selectOrderList(Order order);

}

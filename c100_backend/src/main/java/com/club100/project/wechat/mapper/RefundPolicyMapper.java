package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RefundPolicy;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/14 12:16:17
 * @Description: 退款政策 Mapper 接口。
 */
public interface RefundPolicyMapper {

    int insertRefundPolicy(RefundPolicy refundPolicy);

    int insertRefundPolicyInBatch(List<RefundPolicy> refundPolicyList);

    List<RefundPolicy> selectRefundPolicyList(RefundPolicy refundPolicy);

    List<RefundPolicy> selectRefundPolicyListWithPrivilege(RefundPolicy refundPolicy);

    int deleteRefundPolicy(Long... activityId);

}

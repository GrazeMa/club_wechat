package com.club100.project.wechat.controller;

import com.alibaba.fastjson.JSONObject;
import com.club100.framework.web.controller.BaseController;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.*;
import com.club100.project.wechat.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/20 19:39:15
 * @Description: 微信小程序接口控制器。
 */
@Api("活动接口")
@RequestMapping("/v1/club_wechat")
@RestController
@Slf4j
public class WXController extends BaseController {

    /*
    成员变量、静态变量（类变量）。
     */
    static final Integer INTEGER_NUMBER_0 = 0;
    static final Integer INTEGER_NUMBER_1 = 1;
    static final Integer INTEGER_NUMBER_2 = 2;
    static final Integer INTEGER_NUMBER_3 = 3;
    static final Integer INTEGER_NUMBER_4 = 4;
    static final String EMPTY_STRING = "";

    /*
     * 属性中的变量。
     */
    // 小程序 APP_ID。
    @Value("${wechat.app_id}")
    private String APP_ID;
    // 小程序 APP_SECRET。
    @Value("${wechat.app_secret}")
    private String APP_SECRET;
    // 小程序 GRANT_TYPE。
    @Value("${wechat.grant_type}")
    private String GRANT_TYPE;
    // 小程序 GRANT_TYPE。
    @Value("${wechat.credential}")
    private String CLIENT_CREDENTIAL;
    // 邮件发送人。
    @Value("${club.mail.sender}")
    private String EMAIL_FROM;
    // 邮件主题
    @Value("${club.mail.subject}")
    private String EMAIL_SUBJECT;
    // 邮件正文
    @Value("${club.mail.text}")
    private String EMAIL_TEXT;


    @Autowired
    private QiniuService qiniuService;

    /*
    All services.
     */
    // Activity detail information.
    @Autowired
    private IActivityDetailService activityDetailService;
    // Activity like information.
    @Autowired
    private IActivityLikeService activityLikeService;
    // Logistics of activity information.
    @Autowired
    private IActivityLogisticsLogisticsService activityLogisticsLogisticsService;
    // Activity picture information.
    @Autowired
    private IActivityPictureService activityPictureService;
    // Activity route information.
    @Autowired
    private IActivityRouteService activityRouteService;
    // Activity information.
    @Autowired
    private IActivityService activityService;
    // Badge carousel picture information.
    @Autowired
    private IBadgeCarouselPictureService badgeCarouselPictureService;
    // Badge consignee information.
    @Autowired
    private IBadgeConsigneeService badgeConsigneeService;
    // Badge order information.
    @Autowired
    private IBadgeOrderService badgeOrderService;
    // Badge information.
    @Autowired
    private IBadgeService badgeService;
    // Badge type information.
    @Autowired
    private IBadgeTypeService badgeTypeService;
    // Badge user information.
    @Autowired
    private IBadgeUserService badgeUserService;
    // Bike shop information.
    @Autowired
    private IBikeShopService bikeShopService;
    // Main card information.
    @Autowired
    private ICardService cardService;
    // City information.
    @Autowired
    private ICityService cityService;
    // Disclaimer information.
    @Autowired
    private IDisclaimerService disclaimerService;
    // Favorite route information.
    @Autowired
    private IFavoriteRouteService favoriteRouteService;
    // Main inspire information.
    @Autowired
    private IInspireService inspireService;
    // Leader information.
    @Autowired
    private ILeaderService leaderService;
    // Mark picture information.
    @Autowired
    private IMarkPictureService markPictureService;
    // Mark route information.
    @Autowired
    private IMarkRouteService markRouteService;
    // Mark route user information.
    @Autowired
    private IMarkRouteUserService markRouteUserService;
    // My order and activity information.
    @Autowired
    private IOrderActivityService orderActivityService;
    // Pizza backgrounds information.
    @Autowired
    private IPizzaBackgroundsService pizzaBackgroundsService;
    // Pizza getter information.
    @Autowired
    private IPizzaGetterService pizzaGetterService;
    // Pizza getter users information.
    @Autowired
    private IPizzaGetterUserService pizzaGetterUserService;
    // Product information.
    @Autowired
    private IProductService productService;
    // Refund policy information.
    @Autowired
    private IRefundPolicyService refundPolicyService;
    // Ride insurance information.
    @Autowired
    private IRideInsuranceService rideInsuranceService;
    // Main route information.
    @Autowired
    private IRouteService routeService;
    // Main routeV2 information.
    @Autowired
    private IRouteV2Service routeV2Service;
    // Main route and route detail information.
    @Autowired
    private IRouteAndDetailService routeAndDetailService;
    // Route correction picture information.
    @Autowired
    private IRouteCorrectionPictureService routeCorrectionPictureService;
    // Route correction information.
    @Autowired
    private IRouteCorrectionService routeCorrectionService;
    // Route detail information.
    @Autowired
    private IRouteDetailService routeDetailService;
    // Route picture information.
    @Autowired
    private IRoutePictureService routePictureService;
    // Sign up activity information.
    @Autowired
    private ISignUpActivityService signUpActivityService;
    // Sign up order user information.
    @Autowired
    private ISignUpOrderUserService signUpOrderUserService;
    // Sign up information.
    @Autowired
    private ISignUpService signUpService;
    // Sign up user information.
    @Autowired
    private ISignUpUserService signUpUserService;
    // Source information.
    @Autowired
    private ISourceService sourceService;
    // User information.
    @Autowired
    private IUserService userService;

    /*
    All dependencies.
     */
    // Send emails.
    @Autowired
    private JavaMailSender javaMailSender;
    // GET or POST request.
    @Autowired
    private RestTemplate restTemplate;

    //    private String PREFIX = "/v1/club_wechat/";

    public String getAuthData(String code) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(code)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("code(String)");

        String url = "https://api.weixin.qq.com/sns/jscode2session?appid={1}&secret={2}&grant_type={3}&js_code={4}";
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class, APP_ID, APP_SECRET, GRANT_TYPE, code);

        return entity.getBody();

    }

    @ResponseBody
//    @RequestMapping(value = "/decodeUserInfo", method = RequestMethod.POST)
    @RequestMapping(value = "/decodeUserInfo")
    @Transactional
    /**
     * @Fixme 此方法代码不够简洁，需要优化（抽公共方法等）。
     */
    public String decodeUserInfo(@Param("userCity") Integer userCity, @Param("encryptedData") String encryptedData, @Param("iv") String iv, @Param("code") String code) throws UnsupportedEncodingException {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(code)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("code(String)");

        // Recover the data.
        encryptedData = URLEncoder.encode(encryptedData, "UTF-8").replace("%3D", "=").replace("%2F", "/");
        iv = URLEncoder.encode(iv, "UTF-8").replace("%3D", "=").replace("%2F", "/");

        //////////////// 1、向微信服务器 使用登录凭证 code 获取 session_key 和 openid ////////////////
        //请求参数
        String params = "appid=" + APP_ID + "&secret=" + APP_SECRET + "&js_code=" + code + "&grant_type=" + GRANT_TYPE;
        //发送请求
        String sr = HttpRequestUtils.sendGet("https://api.weixin.qq.com/sns/jscode2session", params);
        //解析相应内容（转换成json对象）
        JSONObject json = JSONObject.parseObject(sr);
        //获取会话密钥（session_key）
        String session_key = json.getString("session_key").trim();
        //用户的唯一标识（openid）
        String openid = json.getString("openid").trim();
        log.info(" === openid: {}", openid);

        User searchedUserEntity = new User();
        searchedUserEntity.setWxOpenId(openid);
        User ifExistUserEntity = userService.selectUser(searchedUserEntity);
//        System.out.println("========ifExistUserEntity： " + ifExistUserEntity);
        if (EmptyUtils.isNotEmpty(ifExistUserEntity)) {
//            System.out.println("=====UserId===: " + ifExistUserEntity.getUserId());
//            System.out.println("=====OpenId===: " + ifExistUserEntity.getWxOpenId());
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, String.valueOf(ifExistUserEntity.getUserId()));
        }

        long returnedUserId = PrimaryKeyIdUtils.S_SimpleLongAutoGenerator();
        //////////////// 2、对encryptedData加密数据进行AES解密 ////////////////
        try {
            String result = AesUtil.decrypt(encryptedData, session_key, iv, "UTF-8");

            if (EmptyUtils.isNotEmpty(result)) {
                JSONObject userInfoJSON = JSONObject.parseObject(result);
                User insertUserEntity = new User();
                insertUserEntity.setUserId(returnedUserId);
                insertUserEntity.setUserCity(userCity);
                insertUserEntity.setMarkTimes(0);
                insertUserEntity.setState(1);
                insertUserEntity.setWxUserCity(userInfoJSON.getString("city"));
                insertUserEntity.setWxOpenId(userInfoJSON.getString("openId"));
                insertUserEntity.setWxUnionId(userInfoJSON.getString("unionId"));
                insertUserEntity.setWxUserAvatarUrl(userInfoJSON.getString("avatarUrl"));
                insertUserEntity.setWxUserCountry(userInfoJSON.getString("country"));
                insertUserEntity.setWxUserGender(userInfoJSON.getInteger("gender"));
                insertUserEntity.setWxUserProvince(userInfoJSON.getString("province"));
                insertUserEntity.setWxNickName(userInfoJSON.getString("nickName"));
                // Insert
                int insert = userService.insert(insertUserEntity);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, String.valueOf(returnedUserId));

    }

    @RequestMapping(value = "/chooseCity")
    @ResponseBody
    public String chooseCity(Integer userCity, Long userId) {

        // Judge if the parameter is empty.
//        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        if (EmptyUtils.isNotEmpty(userId)) {
            // Initialization.
            User updatedUserEntity = new User();
            // Assign variables.
            updatedUserEntity.setUserId(userId);
            updatedUserEntity.setUserCity(userCity);
            // Update user's city.
            int updatedRowsInt = userService.updateUserCity(updatedUserEntity);

            // Return results.
            // Update failure.
            if (!Objects.equals(INTEGER_NUMBER_1, updatedRowsInt))
                return ReturnUtils.S_returnUpdateFailureInfo("club_user(chooseCity)");
        }
        // Update success.
        /*// Initialization.
        User searchedUserEntity = new User();
        // Assign variables.
        searchedUserEntity.setUserId(userId);
        // Search for the user's information.
        User returnedUser = userService.selectOneById(searchedUserEntity);
        // Read failure.
        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(SUCCESS_CODE_200, EMPTY_MESSAGE_STRING, returnedUser.toJson(returnedUser));*/
        // Initialization.
        Card searchedCardEntity = new Card();
        // Assign variables.
        if (userCity > 0) searchedCardEntity.setCityType(userCity);
        // Search for the card's information (At most 5).
        List<Card> atMostFiveCardsList = cardService.selectCardAtMostFiveList(searchedCardEntity);
        // Slim the picture.
//        atMostFiveCardsList.forEach(eachCardEntity -> eachCardEntity.setCardPicUrl(eachCardEntity.getCardPicUrl() + "?imageslim"));
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(atMostFiveCardsList));

    }

    @RequestMapping(value = "/slideToMainRoute")
    @ResponseBody
    public String slideToMainRoute(Integer userCity) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        // Initialization.
        Route searchedRouteEntity = new Route();
        // Assign variables.
        if (userCity > 0) searchedRouteEntity.setCityType(userCity);
        // Search for the route's information (At most 2).
        List<Route> atMostTwoRoutesList = routeService.selectRouteAtMostTwoList(searchedRouteEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(atMostTwoRoutesList));

    }

    @ApiOperation(value = "首页滑动至活动部分", httpMethod = "GET")
    @RequestMapping(value = "/slideToMainActivity")
    @ResponseBody
    public String slideToMainActivity(Integer userCity) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        // Initialization.
        Activity searchedActivityEntity = new Activity();
        // Assign variables.
        if (userCity > 0) searchedActivityEntity.setCityType(userCity);

        // Search for the activities' information (At most 2).
        List<Activity> returnedActivitiesList = activityService.selectActivityAtMostTwoList(searchedActivityEntity);

        // Add activity days information.
        // Define the date time formatter.
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        returnedActivitiesList.forEach(eachActivityEntity -> {
            eachActivityEntity.setActivityDays(Integer.parseInt(String.valueOf((LocalDate.parse(eachActivityEntity.getActivityEndTime().format(dateTimeFormatter)).toEpochDay()) - LocalDate.parse(eachActivityEntity.getActivityBeginTime().format(dateTimeFormatter)).toEpochDay())) + 1);
            eachActivityEntity.setBikeShopName(bikeShopService.selectBikeShopById(eachActivityEntity.getBikeShopId()).getBikeShopName());
        });

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedActivitiesList));

    }

    /*@ApiOperation(value = "V2_首页第二屏获取两个路线接口", httpMethod = "GET")
    @RequestMapping(value = "/slideToMainRouteV2")
    @ResponseBody
    public String slideToMainRouteV2(Integer userCity) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        // Initialization.
        RouteV2 searchedRouteV2Entity = new RouteV2();
        // Assign variables.
        if (userCity > 0) searchedRouteV2Entity.setCityType(userCity);
        // Search for the route's information (At most 2).
        List<RouteV2> returnedTwoRoutesV2List = routeV2Service.selectRouteV2AtMostTwoList(searchedRouteV2Entity);
        // Read failure.
//        if (null == atMostTwoRoutesV2List)
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedTwoRoutesV2List));

    }*/

    /*@RequestMapping(value = "/slideToMainInspire")
    @ResponseBody
    public String slideToMainInspire(Integer userCity) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        // Initialization.
        Inspire searchedInspireEntity = new Inspire();
        // Assign variables.
        if (userCity > 0) searchedInspireEntity.setCityType(userCity);
        // Search for the inspire's information (At most 2).
        List<Inspire> atMostTwoInspiresList = inspireService.selectInspireAtMostTwoList(searchedInspireEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(atMostTwoInspiresList));

    }*/

    @RequestMapping(value = "/slideToMainInspire")
    @ResponseBody
    public String slideToMainInspire() {

        /*// Search for the inspire's information (At most 2).
        List<Inspire> atMostTwoInspiresList = inspireService.selectInspireAtMostTwoList();
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(atMostTwoInspiresList));*/

        return "";

    }

    @RequestMapping(value = "/routeList")
    @ResponseBody
    public String routeList(Integer[] routeDistance, Integer[] routeTerrain, Integer[] routeType, Integer userCity) {

        // Initialization.
        Integer routeDistance1Begin = null;
        Integer routeDistance1End = null;
        Integer routeDistance2Begin = null;
        Integer routeDistance2End = null;
        Integer routeDistance3Begin = null;
        Integer routeDistance3End = null;
        Integer routeDistance4Begin = null;
        Integer routeDistance4End = null;
        Integer routeDistance5Begin = null;
        Integer routeDistance5End = null;
        Integer routeDistance6Begin = null;
        Integer routeDistance6End = null;

        // Judge if the parameter is empty or Initialize them.
        if (EmptyUtils.isNotEmpty(routeDistance)) {
            if (Arrays.asList(routeDistance).contains(1)) {
                routeDistance1Begin = 0;
                routeDistance1End = 5;
            }
            if (Arrays.asList(routeDistance).contains(2)) {
                routeDistance2Begin = 5;
                routeDistance2End = 15;
            }
            if (Arrays.asList(routeDistance).contains(3)) {
                routeDistance3Begin = 15;
                routeDistance3End = 35;
            }
            if (Arrays.asList(routeDistance).contains(4)) {
                routeDistance4Begin = 35;
                routeDistance4End = 90;
            }
            if (Arrays.asList(routeDistance).contains(5)) {
                routeDistance5Begin = 90;
                routeDistance5End = 200;
            }
            if (Arrays.asList(routeDistance).contains(6)) {
                routeDistance6Begin = 200;
                routeDistance6End = Integer.MAX_VALUE;
            }
        }
        if (EmptyUtils.isEmpty(routeTerrain)) routeTerrain = new Integer[]{0};
        if (EmptyUtils.isEmpty(routeType)) routeType = new Integer[]{0};
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        /*// Search 'cityType' by 'userId'.
        // Initialization.
        User searchedUserEntity = new User();
        // Assign variables.
        searchedUserEntity.setUserId(userId);
        // Search for the user's information.
        User returnedUser = userService.selectOneById(searchedUserEntity);
        // Read failure.
        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(routeList)");
        // Read success.
        int cityType = returnedUser.getUserCity();*/

        // Search all the routes (with or without conditions).
        // Initialization.
        Route searchedRouteEntity = new Route();
        // Assign variables.
        searchedRouteEntity.setCityType(userCity);
        // Search for the route's information (with or without conditions).
        List<Route> returnedRouteLists = routeService.selectRouteList(userCity, routeDistance1Begin, routeDistance1End, routeDistance2Begin, routeDistance2End, routeDistance3Begin, routeDistance3End, routeDistance4Begin, routeDistance4End, routeDistance5Begin, routeDistance5End, routeDistance6Begin, routeDistance6End, routeTerrain, routeType);
        returnedRouteLists.forEach(eachRouteEntity -> {
            Badge searchedBadgeEntity = badgeService.selectBadgeByRouteId(eachRouteEntity.getRouteId());
            if (null == searchedBadgeEntity) { // The route doesn't have a badge.
                eachRouteEntity.setIfBadgeExist(false);
            } else { // The route has already have a badge.
                eachRouteEntity.setIfBadgeExist(true);
            }
        });
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        //
        returnedRouteLists.sort((routeEntityA, routeEntityB) -> Boolean.compare(routeEntityB.getIfBadgeExist(), routeEntityA.getIfBadgeExist()));

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedRouteLists));

    }

    @RequestMapping(value = "/inspireList_temp")
    @ResponseBody
    /**
     * @Fixme 等筛选需求确定了加几个字段
     */
    public String inspireList_temp(Integer userCity) {

        // Judge if the parameter is empty or Initialize them.
        if (EmptyUtils.isEmpty(userCity)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userCity(Integer)");

        /*// Search 'cityType' by 'userId'.
        // Initialization.
        User searchedUserEntity = new User();
        // Assign variables.
        searchedUserEntity.setUserId(userId);
        // Search for the user's information.
        User returnedUser = userService.selectOneById(searchedUserEntity);
        // Read failure.
        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(inspireList)");
        // Read success.
        int cityType = returnedUser.getUserCity();*/

        // Search all the inspires (with or without conditions).
        // Initialization.
        Inspire searchedInspireEntity = new Inspire();
        // Assign variables.
        searchedInspireEntity.setCityType(userCity);
        // Search for the inspire's information (with or without conditions).
        List<Inspire> returnedInspireLists = inspireService.selectInspireList();
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedInspireLists));

    }

    @RequestMapping(value = "/inspireList")
    @ResponseBody
    /**
     * @Fixme 等筛选需求确定了加几个字段
     */
    public String inspireList() {

        /*// Search all the inspires (with or without conditions).
        // Initialization.
        Inspire searchedInspireEntity = new Inspire();
        // Assign variables.
        searchedInspireEntity.setCityType(userCity);
        // Search for the inspire's information (with or without conditions).
        List<Inspire> returnedInspireLists = inspireService.selectInspireList();
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedInspireLists));*/

        return "";

    }

    @RequestMapping(value = "/routeDetail")
    @ResponseBody
    public String routeDetail(Long routeId, String userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");

        // Initialization.
        RouteAndDetail searchedRouteAndDetailEntity = new RouteAndDetail();
        // Assign variables.
        searchedRouteAndDetailEntity.setRouteId(routeId);
        // Search for the route and route detail's information.
        RouteAndDetail returnedRouteAndDetail = routeAndDetailService.selectRouteAndDetail(searchedRouteAndDetailEntity);
        // Read failure.
        if (null == returnedRouteAndDetail)
            return ReturnUtils.S_returnReadFailureInfo("club_route(or club_route_detail)(routeDetail)");
        // Read success.

        // Add route badge into entity.
        Badge searchedBadgeEntity = badgeService.selectBadgeByRouteId(routeId);
        if (null == searchedBadgeEntity) {
            returnedRouteAndDetail.setRouteBadgePictureUrl("");
        } else {
            returnedRouteAndDetail.setRouteBadgePictureUrl(searchedBadgeEntity.getBadgePictureUrl());
        }

        // Add route pictures into entity.
        // Initialization.
        RoutePicture searchedRoutePictureEntity = new RoutePicture();
        // Assign variables.
        searchedRoutePictureEntity.setRouteId(routeId);
        List<RoutePicture> routePicturesList = routePictureService.selectRoutePicture(searchedRoutePictureEntity);
        // Concatenate route pictures' url string with '@@'.
        String concatenatedString = "";
        if (0 < routePicturesList.size())
            concatenatedString = P_concatenateStringWithCharacters(routePicturesList, "@@");
        returnedRouteAndDetail.setRoutePictureUrl(concatenatedString);

        // Add recently rider into entity.
        // Initialization
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setRouteId(routeId);
        // Search for at most six marked users' information.
        List<MarkRouteUser> markRouteUsersList = markRouteUserService.selectMarkedUserAtMostSixList(searchedMarkRouteEntity);
        returnedRouteAndDetail.setRecentlyRider(markRouteUsersList);

        // Check if the route has favorite.
        if (EmptyUtils.isEmpty(userId) || "null".equals(userId)) {
            returnedRouteAndDetail.setIfFavorite(false);
        } else {
            // Initialization.
            FavoriteRoute searchedFavoriteRouteEntity = new FavoriteRoute();
            // Assign variables.
            searchedFavoriteRouteEntity.setRouteId(routeId);
            searchedFavoriteRouteEntity.setUserId(Long.valueOf(userId));
            // Search if the route is favorite or not.
            int selectCountInt = favoriteRouteService.selectFavoriteRouteCountList(searchedFavoriteRouteEntity);
            // The user has favorite the route.
            returnedRouteAndDetail.setIfFavorite(!Objects.equals(INTEGER_NUMBER_0, selectCountInt));
        }

        // Count route clicks.
        int updateRowsInt = P_countRouteClicks(returnedRouteAndDetail.getRouteClickCount(), routeId);
        // Update failure.
        if (!Objects.equals(INTEGER_NUMBER_1, updateRowsInt))
            return ReturnUtils.S_returnUpdateFailureInfo("club_route(routeDetail)");
        // Update success.

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedRouteAndDetail.toJson(returnedRouteAndDetail));

    }

    @RequestMapping(value = "/moreRecentlyRider")
    @ResponseBody
    public String moreRecentlyRider(Long routeId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");

        // Initialization
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setRouteId(routeId);
        // Search for all marked users' information.
        List<MarkRouteUser> returnedUsersList = markRouteUserService.selectMarkedUserList(searchedMarkRouteEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedUsersList));

    }

    @RequestMapping(value = "/favoriteRoute")
    @ResponseBody
    public String favoriteRoute(Long routeId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        FavoriteRoute searchedFavoriteRouteEntity = new FavoriteRoute();
        // Assign variables.
        searchedFavoriteRouteEntity.setRouteId(routeId);
        searchedFavoriteRouteEntity.setUserId(userId);
        // Search if the route is favorite or not.
        int selectCountInt = favoriteRouteService.selectFavoriteRouteCountList(searchedFavoriteRouteEntity);
        // The user has favorite the route.

        // Delete the favorite route.
        if (!Objects.equals(INTEGER_NUMBER_0, selectCountInt)) {
            FavoriteRoute deletedFavoriteRouteEntity = new FavoriteRoute();
            deletedFavoriteRouteEntity.setRouteId(routeId);
            deletedFavoriteRouteEntity.setUserId(userId);
            int deletedCount = favoriteRouteService.deleteFavoriteRoute(deletedFavoriteRouteEntity);
//            System.out.println("===deletedCount: " + deletedCount);
            /**
             * @Fixme 加一些判断。
             */
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, String.valueOf(INTEGER_NUMBER_1));
        }

        // The user hasn't favorite the route.

        // Insert the favorite record.
        int insertRowsInt = favoriteRouteService.insertFavoriteRoute(searchedFavoriteRouteEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_favorite_route(favoriteRoute)");
        // Insert success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, "0");

    }

    /*@RequestMapping(value = "/downloadGpx")
    @ResponseBody
    public String downloadGpx(Long routeId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");

        // Initialization.
        RouteDetail searchedRouteDetailEntity = new RouteDetail();
        // Assign variables.
        searchedRouteDetailEntity.setRouteId(routeId);
        // Search for the route detail's information.
        RouteDetail routeDetail = routeDetailService.selectRouteDetail(searchedRouteDetailEntity);
        // Read failure.
        if (null == routeDetail) return ReturnUtils.S_returnReadFailureInfo("club_route_detail(downloadGpx)");
        // Read success.
        // 'GPX' file url is missing.
        if (routeDetail.getGpxUrl().isEmpty())
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        // 'GPX' file url is ready.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, routeDetail.getGpxUrl());

    }*/

    @RequestMapping(value = "/sendEmailWithGpxFiles")
    @ResponseBody
    public String sendEmailWithGpxFiles(String downloadUrl, String userEmail) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(downloadUrl))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("downloadUrl(String)");
        if (EmptyUtils.isEmpty(userEmail)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userEmail(String)");

        // Send email with attached file(s).
        P_sendEmailWithAttachedFiles(downloadUrl, userEmail);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @RequestMapping(value = "/markRoute")
    @ResponseBody
    public String markRoute(Long routeId, Long userId, @RequestBody Map<Integer, String> picsMap) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(picsMap))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("picsMap(Map<Integer, String>)");

        // Make the parameter reasonable.
        if (picsMap.size() < 1 || picsMap.size() > 9)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("picsMap(Map<Integer, String>)");

        // Initialization.
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setIsNewestMark(1);
        searchedMarkRouteEntity.setRouteId(routeId);
        searchedMarkRouteEntity.setUserId(userId);
        // Search if the route is marked or not(Just check the latest one).
        MarkRoute ifEmptyMarkRouteEntity = markRouteService.selectMarkRouteOne(searchedMarkRouteEntity);
        if (EmptyUtils.isNotEmpty(ifEmptyMarkRouteEntity)) {
            // The user has marked the route.
            // Update the latest record.
            // Initialization.
            MarkRoute updatedMarkRouteEntity = new MarkRoute();
            // Assign variables.
            updatedMarkRouteEntity.setIsNewestMark(2);
            updatedMarkRouteEntity.setMarkRouteId(ifEmptyMarkRouteEntity.getMarkRouteId());
            int updatedRowsInt = markRouteService.updateMarkRouteById(updatedMarkRouteEntity);
            // Update failure.
            if (!Objects.equals(INTEGER_NUMBER_1, updatedRowsInt))
                return ReturnUtils.S_returnUpdateFailureInfo("club_mark_route(markRoute)");
            // Update success.
        }

        // Insert the mark record.
        int insertRowsInt = markRouteService.insertMarkRoute(searchedMarkRouteEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_mark_route(markRoute)");
        // Insert success.

        // Insert the pictures in batch.
        // Initialization & Assign variables.
        List<MarkPicture> insertedMarkPictureList = new ArrayList<>();
        for (Map.Entry<Integer, String> eachPicMap : picsMap.entrySet()) {
            MarkPicture insertedMarkPictureEntity = new MarkPicture();
            insertedMarkPictureEntity.setMarkPictureSequence(eachPicMap.getKey());
            insertedMarkPictureEntity.setMarkPictureUrl(eachPicMap.getValue());
            insertedMarkPictureEntity.setRouteId(routeId);
            insertedMarkPictureEntity.setUserId(userId);
            insertedMarkPictureList.add(insertedMarkPictureEntity);
        }
        int insertInBatchRowsInt = markPictureService.insertMarkPictureInBatch(insertedMarkPictureList);
        // Insert failure.
        if (insertInBatchRowsInt < INTEGER_NUMBER_1)
            return ReturnUtils.S_returnCreateFailureInfo("club_mark_picture(markRoute)");
        // Insert success.

        // Update the 'markTimes' in 'user' table.
        // Select the user's 'markTimes' information.
        User searchedUserEntity = new User();
        searchedUserEntity.setUserId(userId);
        User updatedUserEntity = userService.selectOneById(searchedUserEntity);
        updatedUserEntity.setMarkTimes(updatedUserEntity.getMarkTimes() + 1);
        userService.updateUserMarkTimes(updatedUserEntity);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    /*@RequestMapping(value = "/markRoute")
    @ResponseBody
    public String markRoute(Long routeId, Long userId, @RequestBody Map<Integer, String> picsMap) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(picsMap))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("picsMap(Map<Integer, String>)");

        // Make the parameter reasonable.
        if (picsMap.size() < 1 || picsMap.size() > 9)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("picsMap(Map<Integer, String>)");

        // Initialization.
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setRouteId(routeId);
        searchedMarkRouteEntity.setUserId(userId);
        // Search if the route is marked or not.
        Integer selectCountInt = markRouteService.selectMarkRouteCountList(searchedMarkRouteEntity);
        if (EmptyUtils.isEmpty(selectCountInt)) selectCountInt = 0;
        // The user has marked the route.
        if (!Objects.equals(INTEGER_NUMBER_0, selectCountInt))
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        // The user hasn't marked the route.

        // Insert the mark record.
        int insertRowsInt = markRouteService.insertMarkRoute(searchedMarkRouteEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_mark_route(markRoute)");
        // Insert success.

        // Insert the pictures in batch.
        // Initialization & Assign variables.
        List<MarkPicture> insertedMarkPictureList = new ArrayList<>();
        for (Map.Entry<Integer, String> eachPicMap : picsMap.entrySet()) {
            MarkPicture insertedMarkPictureEntity = new MarkPicture();
            insertedMarkPictureEntity.setMarkPictureSequence(eachPicMap.getKey());
            insertedMarkPictureEntity.setMarkPictureUrl(eachPicMap.getValue());
            insertedMarkPictureEntity.setRouteId(routeId);
            insertedMarkPictureEntity.setUserId(userId);
            insertedMarkPictureList.add(insertedMarkPictureEntity);
        }
        int insertInBatchRowsInt = markPictureService.insertMarkPictureInBatch(insertedMarkPictureList);
        // Insert failure.
        if (insertInBatchRowsInt < INTEGER_NUMBER_1)
            return ReturnUtils.S_returnCreateFailureInfo("club_mark_picture(markRoute)");
        // Insert success.

        // Update the 'markTimes' in 'user' table.
        // Select the user's 'markTimes' information.
        User searchedUserEntity = new User();
        searchedUserEntity.setUserId(userId);
        User updatedUserEntity = userService.selectOneById(searchedUserEntity);
        updatedUserEntity.setMarkTimes(updatedUserEntity.getMarkTimes() + 1);
        userService.updateUserMarkTimes(updatedUserEntity);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }*/

    @RequestMapping(value = "/userInfo")
    @ResponseBody
    public String userInfo(Long userId) {
//        System.out.println("------userInfo------");
        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        User searchedUserEntity = new User();
//        System.out.println("=======userId: " + userId);
        // Assign variables.
        searchedUserEntity.setUserId(userId);
        // Search for the user's information.
        User returnedUser = userService.selectOneById(searchedUserEntity);
        // Read failure.
        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedUser.toJson(returnedUser));

    }

    @RequestMapping(value = "/listFavoriteRoute")
    @ResponseBody
    /**
     * @Fixme 暂无高级分页
     */
    public String listFavoriteRoute(Integer pageNo, Integer pageSize, Long userId) {

        // Judge if the parameter is empty or Initialize them.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(pageNo)) pageNo = 1;
        if (EmptyUtils.isEmpty(pageSize)) pageSize = 1;

        // Make the parameter reasonable.
        if (pageNo < 1 || pageNo > 100000)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("pageNo(Integer)");
        if (pageSize < 1 || pageSize > 1000)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("pageSize(Integer)");

        // Initialization.
        FavoriteRoute searchedFavoriteRouteEntity = new FavoriteRoute();
        // Assign variables.
        searchedFavoriteRouteEntity.setUserId(userId);
        // Search for the favorite routes' information.
        List<FavoriteRoute> favoriteRoutesList = favoriteRouteService.selectFavoriteRouteList(searchedFavoriteRouteEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        // 'favoriteRoutesList' is empty.
        if (favoriteRoutesList.isEmpty())
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        // 'favoriteRoutesList' is not empty.
        // Initialization.
        List<Long> longArrayList = new ArrayList<>();


        /**
         * @Fixme 按照时间进行倒序排序。
         */
//        System.out.println("favoriteRoutesList: " + favoriteRoutesList);

        /*
        * e.g.
        //排序
        //单字段排序，根据id排序
        userList.sort(Comparator.comparing(User::getId));
        //多字段排序，根据id，年龄排序
        userList.sort(Comparator.comparing(User::getId).thenComparing(User::getAge));
        *
        * */


        // Get 'routeId(s)' from 'favoriteRoutesList'.
        favoriteRoutesList.stream().forEach(eachRoute -> {
            longArrayList.add(eachRoute.getRouteId());
        });

        Long[] longArray = longArrayList.toArray(new Long[longArrayList.size()]);
        List<Route> returnedRoutesList = routeService.selectByRouteIds(new RowBounds((pageNo - 1) * pageSize, pageSize), longArray);

//        System.out.println("===returnedRoutesList: " + returnedRoutesList);
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedRoutesList));

    }

    @RequestMapping(value = "/listMarkRoute")
    @ResponseBody
    /**
     * @Fixme 暂无高级分页
     */
    public String listMarkRoute(Integer pageNo, Integer pageSize, Long userId) {

        // Judge if the parameter is empty or Initialize them.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(pageNo)) pageNo = 1;
        if (EmptyUtils.isEmpty(pageSize)) pageSize = 1;

        // Make the parameter reasonable.
        if (pageNo < 1 || pageNo > 100000)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("pageNo(Integer)");
        if (pageSize < 1 || pageSize > 1000)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("pageSize(Integer)");

        // Initialization.
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setUserId(userId);
        // Search for the mark routes' information.
        List<MarkRoute> markRoutesList = markRouteService.selectMarkRouteByUserIDList(searchedMarkRouteEntity);

        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        // 'markRoutesList' is empty.
        if (markRoutesList.isEmpty()) return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        // 'markRoutesList' is not empty.
        // Initialization.
        List<Long> longArrayList = new ArrayList<>();
        // Get 'routeId(s)' from 'markRoutesList'.
        markRoutesList.stream().forEach(eachRoute -> {
            longArrayList.add(eachRoute.getRouteId());
        });
        Long[] longArray = longArrayList.toArray(new Long[longArrayList.size()]);
        List<Route> returnedRoutesList = routeService.selectByRouteIds(new RowBounds((pageNo - 1) * pageSize, pageSize), longArray);
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedRoutesList));

    }

    @RequestMapping(value = "/getFirstMarkPictureUrl")
    @ResponseBody
    public String getFirstMarkPictureUrl(Long routeId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        MarkPicture searchedMarkPictureEntity = new MarkPicture();
        searchedMarkPictureEntity.setRouteId(routeId);
        searchedMarkPictureEntity.setUserId(userId);
        MarkPicture markPicture = markPictureService.selectLastPicture(searchedMarkPictureEntity);
        if (EmptyUtils.isEmpty(markPicture))
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

        markPicture.setMarkTime(LocalDate.now().toString());

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, markPicture.toJson(markPicture));

    }

    @RequestMapping(value = "/uploadCard")
    @ResponseBody
    public String uploadCard() {
        return "";
    }

    @RequestMapping(value = "/uploadRoute")
    @ResponseBody
    public String uploadRoute() {
        return "";
    }

    @RequestMapping(value = "/uploadInspire")
    @ResponseBody
    public String uploadInspire() {
        return "";
    }

    @ApiOperation(value = "活动列表接口", httpMethod = "GET")
    @RequestMapping(value = "/activityList")
    @ResponseBody
    public String activityList(Integer[] activityDays, Integer[] activityStates, Integer[] activityFeeType, Integer userCity) {

        // Initialization.
        Integer singleDay = null;
        Integer multiDays = null;
        Integer notStarted = null;
        Integer inProgress = null;
        Integer hasEnded = null;
        Integer free = null;
        Integer charge = null;
        LocalDate currentDate = LocalDate.now();

        // Judge if the parameter is empty or Initialize them.
        if (EmptyUtils.isNotEmpty(activityDays)) {
            if (Arrays.asList(activityDays).contains(1)) {
                singleDay = 1;
            }
            if (Arrays.asList(activityDays).contains(2)) {
                multiDays = 1;
            }
        }
        if (EmptyUtils.isNotEmpty(activityStates)) {
            if (Arrays.asList(activityStates).contains(1)) {
                notStarted = 1;
            }
            if (Arrays.asList(activityStates).contains(2)) {
                inProgress = 1;
            }
            if (Arrays.asList(activityStates).contains(3)) {
                hasEnded = 1;
            }
        }
        if (EmptyUtils.isNotEmpty(1)) {
            if (Arrays.asList(activityFeeType).contains(1)) {
                free = 1;
            }
            if (Arrays.asList(activityFeeType).contains(2)) {
                charge = 1;
            }
        }
        if (EmptyUtils.isEmpty(userCity)) userCity = 0;

        // Initialization.
        Activity searchedActivityEntity = new Activity();

        // Search for the activities' information.
        List<Activity> returnedActivitiesList = activityService.selectActivityListWithConditions(searchedActivityEntity, singleDay, multiDays, notStarted, inProgress, hasEnded, currentDate, free, charge, userCity);

        // Add activity days information.
        // Define the date time formatter.
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        returnedActivitiesList.forEach(eachActivityEntity -> {
            eachActivityEntity.setActivityDays(Integer.parseInt(String.valueOf((LocalDate.parse(eachActivityEntity.getActivityEndTime().format(dateTimeFormatter)).toEpochDay()) - LocalDate.parse(eachActivityEntity.getActivityBeginTime().format(dateTimeFormatter)).toEpochDay())) + 1);
            eachActivityEntity.setBikeShopName(bikeShopService.selectBikeShopById(eachActivityEntity.getBikeShopId()).getBikeShopName());
        });

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedActivitiesList));

    }

    @ApiOperation(value = "车店活动列表接口", httpMethod = "GET")
    @RequestMapping(value = "/activityShopList")
    @ResponseBody
    public String activityShopList(Integer tabSequence, Long bikeShopId) {

        // Initialization.
        LocalDate currentDate = LocalDate.now();

        // Judge if the parameter is empty or Initialize them.
        /**
         * tabSequence 从 0 开始，目前是 0，1，2 三个选项。
         */
        if (EmptyUtils.isEmpty(tabSequence)) tabSequence = 0;
        if (0 > tabSequence) tabSequence = 0;
        if (3 < tabSequence) tabSequence = 3;
        if (EmptyUtils.isEmpty(bikeShopId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("bikeShopId(Long)");

        // Search for the activities' information.
        List<Activity> returnedActivitiesList = activityService.selectActivityListWithOrders(tabSequence, currentDate, bikeShopId);

        // Add activity days information.
        // Define the date time formatter.
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        returnedActivitiesList.forEach(eachActivityEntity -> eachActivityEntity.setActivityDays(Integer.parseInt(String.valueOf((LocalDate.parse(eachActivityEntity.getActivityEndTime().format(dateTimeFormatter)).toEpochDay()) - LocalDate.parse(eachActivityEntity.getActivityBeginTime().format(dateTimeFormatter)).toEpochDay())) + 1));

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedActivitiesList));

    }

    @ApiOperation(value = "活动详情接口", httpMethod = "GET")
    @RequestMapping(value = "/activityDetail")
    @ResponseBody
    public String activityDetail(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        ActivityDetail searchedActivityDetailEntity = new ActivityDetail();
        // Assign variables.
        searchedActivityDetailEntity.setActivityId(activityId);

        // Search for the activity and detail's information.
        ActivityDetail returnedActivityDetailEntity = activityDetailService.selectActivityDetailById(searchedActivityDetailEntity);
        // There's no such activity in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedActivityDetailEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_activity(activityDetail)");
        // Read successfully. We've found the activity and detail.

        // Add activity pictures into entity.
        // Initialization.
        ActivityPicture searchedActivityPictureEntity = new ActivityPicture();
        // Assign variables.
        searchedActivityPictureEntity.setActivityId(activityId);
        List<ActivityPicture> activityPicturesList = activityPictureService.selectActivityPicture(searchedActivityPictureEntity);
        // Concatenate activity pictures' url string with '@@'.
        String concatenatedString = "";
        if (0 < activityPicturesList.size())
            concatenatedString = P_concatenateStringWithCharacters2(activityPicturesList, "@@");

        returnedActivityDetailEntity.setActivityDetailPictureUrls(concatenatedString);

        // Add activity likes into entity.
        // Initialization.
        ActivityLike searchedActivityLikeEntity = new ActivityLike();
        searchedActivityLikeEntity.setActivityId(activityId);

        int selectCountInt = activityLikeService.selectActivityLikeCountList(searchedActivityLikeEntity);
        returnedActivityDetailEntity.setActivityLikeCount(selectCountInt);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedActivityDetailEntity.toJson(returnedActivityDetailEntity));

    }

    @ApiOperation(value = "报名活动接口", httpMethod = "GET")
    @RequestMapping(value = "/signUp")
    @ResponseBody
    public String signUp(Long activityId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        SignUp searchedSignUpEntity = new SignUp();
        // Assign variables.
        searchedSignUpEntity.setActivityId(activityId);
        searchedSignUpEntity.setUserId(userId);

        // Search if the user has signed up.
        SignUp ifEmptySignUpEntity = signUpService.selectSignUp(searchedSignUpEntity);
        // The user has signed up the activity.
        if (EmptyUtils.isNotEmpty(ifEmptySignUpEntity))
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        // The user hasn't signed up the activity.

        // Insert the sign up information.
        int insertRowsInt = signUpService.insertSignUp(searchedSignUpEntity);

        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_sign_up(signUp)");
        // Insert success.

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @ApiOperation(value = "我的活动列表接口", httpMethod = "GET")
    @RequestMapping(value = "/listMyActivity")
    @ResponseBody
    public String listMyActivity(Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        OrderActivity searchedOrderActivityEntity = new OrderActivity();
        // Assign variables.
        searchedOrderActivityEntity.setUserId(userId);

        // Search for my activity.
        List<OrderActivity> returnedorderActivitiesList = orderActivityService.selectOrderActivityList(searchedOrderActivityEntity);

        if (null == returnedorderActivitiesList)
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedorderActivitiesList));

    }

    /*@ApiOperation(value = "获取报名用户信息接口", httpMethod = "GET")
    @RequestMapping(value = "/getSignUpUserInfo")
    @ResponseBody
    public String getSignUpUserInfo(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        SignUpUser searchedSignUpUserEntity = new SignUpUser();
        // Assign variables.
        searchedSignUpUserEntity.setActivityId(activityId);

        // Search for the sign up users' information.
        List<SignUpUser> returnedSignUpUsersList = signUpUserService.selectSignUpUser(searchedSignUpUserEntity);

        // Read failure.
//        if (null == returnedProductsList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, ReturnUtils.S_returnSignUpUserJsonInfo(returnedSignUpUsersList.size(), returnedSignUpUsersList));

    }*/

    @ApiOperation(value = "获取报名用户信息接口", httpMethod = "GET")
    @RequestMapping(value = "/getSignUpUserInfo")
    @ResponseBody
    public String getSignUpUserInfo(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        SignUpOrderUser searchedSignUpOrderUserEntity = new SignUpOrderUser();
        // Assign variables.
        searchedSignUpOrderUserEntity.setActivityId(activityId);

        // Search for the sign up users' information.
        List<SignUpOrderUser> returnedSignUpOrderUsersList = signUpOrderUserService.selectSignUpOrderUser(searchedSignUpOrderUserEntity);

        int signUpOrderUsersCountInt = signUpOrderUserService.selectSignUpOrderUserCount(searchedSignUpOrderUserEntity);

        // Read failure.
//        if (null == returnedProductsList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, ReturnUtils.S_returnSignUpUserJsonInfo(signUpOrderUsersCountInt, returnedSignUpOrderUsersList));

    }

    @ApiOperation(value = "获取车店信息接口", httpMethod = "GET")
    @RequestMapping(value = "/getBikeShopInfo")
    @ResponseBody
    public String getBikeShopInfo(Long bikeShopId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(bikeShopId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("bikeShopId(Long)");

        // Search for the bike shop's information.
        BikeShop returnedBikeShopEntity = bikeShopService.selectBikeShopById(bikeShopId);

        // There's no such bike shop in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedBikeShopEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_bike_shop(getBikeShopInfo)");
        // Read successfully. We've found the bike shop.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedBikeShopEntity.toJson(returnedBikeShopEntity));

    }

    @ApiOperation(value = "获取领队信息接口", httpMethod = "GET")
    @RequestMapping(value = "/getLeaderInfo")
    @ResponseBody
    public String getLeaderInfo(Long leaderId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(leaderId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("leaderId(Long)");

        // Search for the leader's information.
        Leader returnedLeaderEntity = leaderService.selectLeaderById(leaderId);
        // There's no such leader in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedLeaderEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_leader(getLeaderInfo)");
        // Read successfully. We've found the leader.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedLeaderEntity.toJson(returnedLeaderEntity));

    }

    @ApiOperation(value = "获取商品接口", httpMethod = "GET")
    @RequestMapping(value = "/listProducts")
    @ResponseBody
    public String listProducts(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        Product searchedProductEntity = new Product();
        // Assign variables.
        searchedProductEntity.setActivityId(activityId);

        // Search for the products' information.
        List<Product> returnedProductsList = productService.selectProductList(searchedProductEntity);

        // Read failure.
//        if (null == returnedProductsList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedProductsList));

    }

    @ApiOperation(value = "获取退款政策接口", httpMethod = "GET")
    @RequestMapping(value = "/listRefundPolicies")
    @ResponseBody
    public String listRefundPolicies(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        RefundPolicy searchedRefundPolicyEntity = new RefundPolicy();
        // Assign variables.
        searchedRefundPolicyEntity.setActivityId(activityId);

        // Search for the refund policies' information.
        List<RefundPolicy> returnedRefundPoliciesList = refundPolicyService.selectRefundPolicyList(searchedRefundPolicyEntity);

        // Read failure.
        // if (null == returnedRefundPoliciesList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedRefundPoliciesList));

    }

    @ApiOperation(value = "获取后勤保障接口", httpMethod = "GET")
    @RequestMapping(value = "/listLogistics")
    @ResponseBody
    public String listLogistics(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        ActivityLogisticsLogistics searchedActivityLogisticsLogisticsEntity = new ActivityLogisticsLogistics();
        // Assign variables.
        searchedActivityLogisticsLogisticsEntity.setActivityId(activityId);

        // Search for the refund policies' information.
        List<ActivityLogisticsLogistics> returnedActivityLogisticsLogisticsList = activityLogisticsLogisticsService.selectActivityLogisticsLogisticsList(searchedActivityLogisticsLogisticsEntity);

        // Read failure.
        // if (null == returnedActivityLogisticsLogisticsList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedActivityLogisticsLogisticsList));

    }

    @ApiOperation(value = "获取免责声明接口", httpMethod = "GET")
    @RequestMapping(value = "/getDisclaimer")
    @ResponseBody
    public String getDisclaimer() {

        // Initialization.
        long disclaimerId = 1L;

        // Search for the disclaimer's information.
        Disclaimer returnedDisclaimerEntity = disclaimerService.selectDisclaimerById(disclaimerId);

        // There's no such disclaimer in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedDisclaimerEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_disclaimer(getDisclaimer)");
        // Read successfully. We've found the disclaimer.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedDisclaimerEntity.toJson(returnedDisclaimerEntity));

    }

    @ApiOperation(value = "获取骑行意外险接口", httpMethod = "GET")
    @RequestMapping(value = "/getInsurance")
    @ResponseBody
    public String getInsurance() {

        // Initialization.
        long insuranceId = 1L;

        // Search for the insurance's information.
        Insurance returnedInsuranceEntity = rideInsuranceService.selectInsuranceById(insuranceId);

        // There's no such insurance in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedInsuranceEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_insurance(getInsurance)");
        // Read successfully. We've found the insurance.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedInsuranceEntity.toJson(returnedInsuranceEntity));

    }

    @ApiOperation(value = "路线报错接口", httpMethod = "POST")
    @RequestMapping(value = "/routeCorrection")
    @ResponseBody
    public String routeCorrection(Long routeId, Long userId, String content, @RequestBody Map<Integer, String> picsMap) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(content)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("content(String)");
        if (EmptyUtils.isEmpty(picsMap))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("picsMap(Map<Integer, String>)");

        // Make the parameter reasonable.
        if (content.length() > 1000) return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("content(String)");
        if (picsMap.size() > 3)
            return ReturnUtils.S_returnParameterIsNotReasonableFailureInfo("picsMap(Map<Integer, String>)");

        // Initialization.
        RouteCorrection searchedRouteCorrectionEntity = new RouteCorrection();
        // Assign variables.
        searchedRouteCorrectionEntity.setCorrectionContent(content);
        searchedRouteCorrectionEntity.setUserId(userId);

        // Insert the route correction record.
        int insertRowsInt = routeCorrectionService.insertRouteCorrection(searchedRouteCorrectionEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_route_correction(routeCorrection)");
        // Insert success.

        if (picsMap.size() > 0) {
            // Insert the pictures in batch.
            // Initialization & Assign variables.
            List<RouteCorrectionPicture> insertedRouteCorrectionPictureList = new ArrayList<>();
            for (Map.Entry<Integer, String> eachPicMap : picsMap.entrySet()) {
                RouteCorrectionPicture insertedRouteCorrectionPictureEntity = new RouteCorrectionPicture();
                insertedRouteCorrectionPictureEntity.setRouteCorrectionId(searchedRouteCorrectionEntity.getRouteCorrectionId());
                insertedRouteCorrectionPictureEntity.setCorrectionPictureSequence(eachPicMap.getKey());
                insertedRouteCorrectionPictureEntity.setCorrectionPictureUrl(eachPicMap.getValue());
                insertedRouteCorrectionPictureList.add(insertedRouteCorrectionPictureEntity);
            }
            int insertInBatchRowsInt = routeCorrectionPictureService.insertRouteCorrectionPictureInBatch(insertedRouteCorrectionPictureList);
            // Insert failure.
            if (insertInBatchRowsInt < INTEGER_NUMBER_1)
                return ReturnUtils.S_returnCreateFailureInfo("club_route_correction_picture(routeCorrection)");
            // Insert success.
        }

        /*// Send a email to notify us.
        // Initialization.
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(EMAIL_FROM);
            helper.setTo("490982326@qq.com");
            helper.setSubject(EMAIL_SUBJECT);
            helper.setText(EMAIL_TEXT);
            URL url = new URL("https://images.club100.cn/lianghu_page1.jpg");
            helper.addAttachment("pic.jpg", new ByteArrayResource(IOUtils.toByteArray(url.openStream())));
            helper.addAttachment("pic2.jpg", new ByteArrayResource(IOUtils.toByteArray(url.openStream())));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Thread(() -> javaMailSender.send(mimeMessage)).start();*/

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @ApiOperation(value = "获取活动多日路线接口", httpMethod = "GET")
    @RequestMapping(value = "/getActivityRoutes")
    @ResponseBody
    public String getActivityRoutes(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        ActivityRoute searchedActivityRouteEntity = new ActivityRoute();
        // Assign variables.
        searchedActivityRouteEntity.setActivityId(activityId);

        // Search for the activity routes' information.
        List<ActivityRoute> returnedActivityRoutesList = activityRouteService.selectActivityRoute(searchedActivityRouteEntity);

        // Read failure.
        // if (null == returnedActivityRoutesList)

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(returnedActivityRoutesList));

    }

    @ApiOperation(value = "下载活动多日 GPX 文件接口", httpMethod = "GET")
    @RequestMapping(value = "/downloadActivityGpxfiles")
    @ResponseBody
    public String downloadActivityGpxfiles(Long activityId, String userEmail) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");
        if (EmptyUtils.isEmpty(userEmail)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userEmail(String)");

        // Find the gpx files' link url.
        // Initialization.
        ActivityRoute searchedActivityRouteEntity = new ActivityRoute();
        // Assign variables.
        searchedActivityRouteEntity.setActivityId(activityId);

        // Search for the activity routes' information.
        List<ActivityRoute> returnedActivityRoutesList = activityRouteService.selectActivityRoute(searchedActivityRouteEntity);
        // Read failure.
        // if (null == returnedActivityRoutesList)
        // Read success.

        // Send email to the user.
        // Initialization.
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(EMAIL_FROM);
            helper.setTo(userEmail);
            helper.setSubject(EMAIL_SUBJECT);
            helper.setText(EMAIL_TEXT);
            returnedActivityRoutesList.forEach(eachActivityRouteEntity -> {
                try {
                    helper.addAttachment("Route Day " + eachActivityRouteEntity.getRouteDayN() + " GPX file.gpx", new ByteArrayResource(IOUtils.toByteArray(new URL(eachActivityRouteEntity.getRouteGpxFileUrl()).openStream())));
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Thread(() -> javaMailSender.send(mimeMessage)).start();

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @ApiOperation(value = "获取城市接口", httpMethod = "GET")
    @RequestMapping(value = "/getCitiesList")
    @ResponseBody
    public String getCitiesList() {

        List<City> citiesList = cityService.selectCityListOrderById();

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(citiesList));

    }

    @ApiOperation(value = "Pizza_获得披萨活动详细信息", httpMethod = "GET")
    @RequestMapping(value = "/pizzaGetDetailInformation")
    @ResponseBody
    public String pizzaGetDetailInformation() {

        // Initialization.
        long pizzaBackgroundsId = 1L;

        // Search for the pizza backgrounds' information.
        PizzaBackgrounds returnedPizzaBackgroundsEntity = pizzaBackgroundsService.selectPizzaBackgroundsById(pizzaBackgroundsId);

        // There's no such pizza backgrounds in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedPizzaBackgroundsEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_pizza_backgrounds(pizzaGetDetailInformation)");
        // Read successfully. We've found the pizza backgrounds.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedPizzaBackgroundsEntity.toJson(returnedPizzaBackgroundsEntity));

    }

    @ApiOperation(value = "Pizza_已获得披萨用户", httpMethod = "GET")
    @RequestMapping(value = "/pizzaGetterUserList")
    @ResponseBody
    public String pizzaGetterUserList() {

        // Search for the sign up users' information.
        List<PizzaGetterUser> returnedPizzaGetterUsersList = pizzaGetterUserService.selectPizzaGetterUser();

        // Read failure.
//        if (null == returnedProductsList)

        int pizzaGetterCountInt = pizzaGetterService.selectPizzaGetterCount();

        // Read success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, ReturnUtils.S_returnPizzaGetterUserJsonInfo(pizzaGetterCountInt, returnedPizzaGetterUsersList));

    }

    @ApiOperation(value = "Pizza_判断弹窗是否展示", httpMethod = "GET")
    @RequestMapping(value = "/pizzaCheckPopupWindow")
    @ResponseBody
    public String pizzaCheckPopupWindow(Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        PopupWindow returnedPopupWindowEntity = new PopupWindow();
        int selectRowsInt = markRouteService.selectMarkRouteCountByUserId(userId);
        Boolean popFlag = (2 == selectRowsInt);
//        returnedPopupWindowEntity.setPopFlag(popFlag); // The pizza activity is opened.
        returnedPopupWindowEntity.setPopFlag(false); // The pizza activity is closed.

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedPopupWindowEntity.toJson(returnedPopupWindowEntity));

    }

    @ApiOperation(value = "Pizza_输入微信 ID 提交按钮", httpMethod = "GET")
    @RequestMapping(value = "/pizzaSubmitWechatId")
    @ResponseBody
    public String pizzaSubmitWechatId(Long userId, String wechatId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(wechatId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("wechatId(String)");

        // Initialization.
        PizzaGetter returnedPizzaGetterEntity = new PizzaGetter();
        returnedPizzaGetterEntity.setUserId(userId);
        returnedPizzaGetterEntity.setWeChatId(wechatId);
        int insertRowsInt = pizzaGetterService.insertPizzaGetter(returnedPizzaGetterEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_pizza_getter(pizzaSubmitWechatId)");
        // Insert success.
        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedPizzaGetterEntity.toJson(returnedPizzaGetterEntity));

    }

    @ApiOperation(value = "统计用户来源接口", httpMethod = "GET")
    @RequestMapping(value = "/getSource")
    @ResponseBody
    public String getSource(String sourceValue) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(sourceValue))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("sourceValue(String)");

        // Initialization.
        Source searchedSourceEntity = new Source();
        searchedSourceEntity.setParameterValue(sourceValue);

        List<Source> sourcesList = sourceService.selectSourceList(searchedSourceEntity);
        // Read failure.
        if (null == sourcesList) return ReturnUtils.S_returnReadFailureInfo("club_source(getSource)");
        // Read success.

        // Initialization.
        Source updatedSourceEntity = new Source();
        updatedSourceEntity.setSourceId(sourcesList.get(0).getSourceId());
        updatedSourceEntity.setSourceCount((sourcesList.get(0).getSourceCount() + 1));

        // Add source count.
        int updateRowsInt = sourceService.updateSource(updatedSourceEntity);
        // Update failure.
        if (!Objects.equals(INTEGER_NUMBER_1, updateRowsInt))
            return ReturnUtils.S_returnUpdateFailureInfo("club_source(getSource)");
        // Update success.

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @ApiOperation(value = "返回活动二维码图片 URL", httpMethod = "GET")
    @RequestMapping(value = "/getPagesQrCodeUrl")
    @ResponseBody
    public String getPagesQrCodeUrl(Long id, String pathUrl, String type) {

//        pages/index/index?type=event&id=[id]
        if (("pages/index/index".equals(pathUrl)) && ("event".equals(type))) {
            Activity activity = activityService.selectActivityQrcodeUrl(id);
            String activityQrcodeUrlString = activity.getActivityQrcodeUrl();
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, activityQrcodeUrlString);
        }

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

    @ApiOperation(value = "Badge_我的徽章", httpMethod = "GET")
    @RequestMapping(value = "/listRouteBadge")
    @ResponseBody
    public String listRouteBadge(Integer cityType, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(cityType)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("cityType(Integer)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        Badge searchedBadgeEntity = new Badge();
        searchedBadgeEntity.setCityType(cityType);
        searchedBadgeEntity.setState(1);
//        List<Badge> badgesList = badgeService.selectBadgeWithCityList(searchedBadgeEntity);

        List<Badge> badgesList = badgeService.selectBadgeByCityTypeOnline(cityType);
        List<Badge> badgesList2 = badgeService.selectBadgeExcludeCityTypeOnline(cityType);
        List<Badge> badgesList3 = badgeService.selectBadgeByCityTypeOffline(cityType);
        List<Badge> badgesList4 = badgeService.selectBadgeExcludeCityTypeOffline(cityType);
        badgesList.addAll(badgesList2);
        badgesList.addAll(badgesList3);
        badgesList.addAll(badgesList4);

        Map<Integer, String> citiesHashMap = P_getCityMap();

        badgesList.forEach(eachBadgeEntity -> {
            // Return city name directly.
            eachBadgeEntity.setCityName(citiesHashMap.get(eachBadgeEntity.getCityType()));

            // Initialization.
            BadgeUser searchedBadgeUserEntity = new BadgeUser();
            searchedBadgeUserEntity.setBadgeId(eachBadgeEntity.getBadgeId());
            searchedBadgeUserEntity.setUserId(userId);
            searchedBadgeUserEntity.setState(1);

            BadgeUser ifExistBadgeUserEntity = badgeUserService.selectOneBadgeUser(searchedBadgeUserEntity);
            // The user doesn't have the badge: false.
            // The user has had the badge: true.
            eachBadgeEntity.setIfUserGetBadge(null != ifExistBadgeUserEntity);

            // 徽章购买状态（0：敬请期待；1：购买实体徽章；2：购买记录；3：未获得；4：实体徽章。）。
            // Priority：4 > 3 > 0 > 1 > 2
            if (!eachBadgeEntity.getIfUserGetBadge()) { // The user doesn't have the badge.
                if (eachBadgeEntity.getIfBadgeOnline()) { // The badge is ready.
                    eachBadgeEntity.setBadgeBuyState(INTEGER_NUMBER_4);
                } else { // The badge is not ready.
                    eachBadgeEntity.setBadgeBuyState(INTEGER_NUMBER_3);
                }
            } else { // The user has had the badge.
                if (!eachBadgeEntity.getIfBadgeOnline()) { // The badge is not ready.
                    eachBadgeEntity.setBadgeBuyState(INTEGER_NUMBER_0);
                } else { // The badge is ready.
                    // Initialization.
                    BadgeOrder searchedBadgeOrderEntity = new BadgeOrder();
                    searchedBadgeOrderEntity.setBadgeId(eachBadgeEntity.getBadgeId());
                    searchedBadgeOrderEntity.setUserId(userId);
                    searchedBadgeOrderEntity.setBadgeOrderState(1);

                    BadgeOrder badgeOrder = badgeOrderService.selectOneBadgeOrderByBadgeIdAndUserId(searchedBadgeOrderEntity);
                    if (null == badgeOrder) { // The user hadn't bought the badge.
                        eachBadgeEntity.setBadgeBuyState(INTEGER_NUMBER_1);
                    } else { // The user had bought the badge.
                        eachBadgeEntity.setBadgeBuyState(INTEGER_NUMBER_2);
                    }
                }
            }
        });

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(badgesList));

    }

    @ApiOperation(value = "Badge_用户打卡获得徽章", httpMethod = "GET")
    @RequestMapping(value = "/getRouteBadge")
    @ResponseBody
    public String getRouteBadge(Long routeId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(routeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("routeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        Badge searchedBadgeEntity = badgeService.selectBadgeByRouteId(routeId);
        // There is no badge for this route now.
        if (null == searchedBadgeEntity)
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

        // The route has a badge.
        // Initialization.
        BadgeUser searchedBadgeUserEntity = new BadgeUser();
        searchedBadgeUserEntity.setBadgeId(searchedBadgeEntity.getBadgeId());
        searchedBadgeUserEntity.setUserId(userId);
        searchedBadgeUserEntity.setState(1);

        BadgeUser ifExistBadgeUserEntity = badgeUserService.selectOneBadgeUser(searchedBadgeUserEntity);
        if (null == ifExistBadgeUserEntity) { // The user doesn't have the badge before.
            int insertRowsInt = badgeUserService.insertBadgeUser(searchedBadgeUserEntity);
            // Insert failure.
            if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
                return ReturnUtils.S_returnCreateFailureInfo("club_badge_user(getRouteBadge)");
            // Insert success.
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, searchedBadgeEntity.toJson(searchedBadgeEntity));
        } else { // The user has had the badge.
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);
        }

    }

    @ApiOperation(value = "BadgePay_点击 购买实体徽章 按钮接口", httpMethod = "GET")
    @RequestMapping(value = "/getBadgePayInfo")
    @ResponseBody
    public String getBadgePayInfo(Long badgeId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(badgeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("badgeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        Badge searchedBadgeEntity = badgeService.selectBadgeById(badgeId);
        if (null == searchedBadgeEntity) return ReturnUtils.S_returnReadFailureInfo("club_badge(getBadgeInfo)");

        Map<Integer, String> citiesHashMap = P_getCityMap();

        searchedBadgeEntity.setCityName(citiesHashMap.get(searchedBadgeEntity.getCityType()));

        List<BadgeCarouselPicture> badgeCarouselPicturesList = badgeCarouselPictureService.selectBadgeCarouselPictureByBadgeId(badgeId);
        // Concatenate badge carousel pictures' url string with '@@'.
        String concatenatedString = "";
        if (0 < badgeCarouselPicturesList.size())
            concatenatedString = P_concatenateStringWithCharacters3(badgeCarouselPicturesList, "@@");

        searchedBadgeEntity.setBadgeCarouselPictureUrl(concatenatedString);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, searchedBadgeEntity.toJson(searchedBadgeEntity));

    }

    @ApiOperation(value = "BadgePay_点击 购买记录 按钮接口", httpMethod = "GET")
    @RequestMapping(value = "/getMyBadgePayRecord")
    @ResponseBody
    public String getMyBadgePayRecord(Long badgeId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(badgeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("badgeId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        // Initialization.
        BadgeOrder searchedBadgeOrderEntity = new BadgeOrder();
        searchedBadgeOrderEntity.setBadgeId(badgeId);
        searchedBadgeOrderEntity.setUserId(userId);

        BadgeOrder returnedBadgeOrderEntity = badgeOrderService.selectOneBadgeOrderByBadgeIdAndUserId(searchedBadgeOrderEntity);
        if (null == returnedBadgeOrderEntity)
            return ReturnUtils.S_returnReadFailureInfo("club_badge_order(getMyBadgePayRecord)");

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedBadgeOrderEntity.toJson(returnedBadgeOrderEntity));

    }

    @ApiOperation(value = "BadgePay_点击 立刻购买 按钮接口检查收货人信息", httpMethod = "GET")
    @RequestMapping(value = "/checkIfExistConsigneeInfo")
    @ResponseBody
    public String checkIfExistConsigneeInfo(Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        BadgeConsignee searchedBadgeConsigneeEntity = badgeConsigneeService.selectOneBadgeConsigneeByUserId(userId);
        if (null == searchedBadgeConsigneeEntity)
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, searchedBadgeConsigneeEntity.toJson(searchedBadgeConsigneeEntity));

    }

    @ApiOperation(value = "BadgePay_点击 立刻购买 按钮接口获取款式", httpMethod = "GET")
    @RequestMapping(value = "/listBadgeType")
    @ResponseBody
    public String listBadgeType(Long badgeId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(badgeId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("badgeId(Long)");

        List<BadgeType> badgeTypesList = badgeTypeService.selectBadgeTypeByBadgeId(badgeId);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(badgeTypesList));

    }

    @ApiOperation(value = "BadgePay_列举收货人的地址", httpMethod = "GET")
    @RequestMapping(value = "/listConsigneeInfo")
    @ResponseBody
    public String listConsigneeInfo(Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        List<BadgeConsignee> badgeConsigneesList = badgeConsigneeService.selectBadgeConsigneeByUserId(userId);

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, JsonUtils.S_listToJsonArray(badgeConsigneesList));

    }

    @ApiOperation(value = "BadgePay_点击完成存储或修改收货人信息", httpMethod = "GET")
    @RequestMapping(value = "/saveOrUpdateConsigneeInfo")
    @ResponseBody
    public String saveOrUpdateConsigneeInfo(Long badgeConsigneeId, Long userId, String consigneeAddress, String consigneeDistrict, String consigneeMobile, String consigneeName) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");
        if (EmptyUtils.isEmpty(consigneeAddress))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("consigneeAddress(String)");
        if (EmptyUtils.isEmpty(consigneeDistrict))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("consigneeDistrict(String)");
        if (EmptyUtils.isEmpty(consigneeMobile))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("consigneeMobile(String)");
        if (EmptyUtils.isEmpty(consigneeName))
            return ReturnUtils.S_returnParameterIsEmptyFailureInfo("consigneeName(String)");

        // Initialization.
        BadgeConsignee insertedOrUpdatedBadgeConsigneeEntity = new BadgeConsignee();
        insertedOrUpdatedBadgeConsigneeEntity.setConsigneeAddress(consigneeAddress);
        insertedOrUpdatedBadgeConsigneeEntity.setConsigneeDistrict(consigneeDistrict);
        insertedOrUpdatedBadgeConsigneeEntity.setConsigneeMobile(consigneeMobile);
        insertedOrUpdatedBadgeConsigneeEntity.setConsigneeName(consigneeName);
        insertedOrUpdatedBadgeConsigneeEntity.setUserId(userId);

        if (EmptyUtils.isEmpty(badgeConsigneeId)) { // The user didn't save its information. Insert it into our database.
            int insertRowsInt = badgeConsigneeService.insertBadgeConsignee(insertedOrUpdatedBadgeConsigneeEntity);

            // Insert failure.
            if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
                return ReturnUtils.S_returnCreateFailureInfo("club_badge_consignee(saveOrUpdateConsigneeInfo)");
            // Insert success.
        } else { // The user has saved its information. The older information should be updated.
            insertedOrUpdatedBadgeConsigneeEntity.setBadgeConsigneeId(badgeConsigneeId);
            int updateRowsInt = badgeConsigneeService.updateBadgeConsignee(insertedOrUpdatedBadgeConsigneeEntity);

            // Update failure.
            if (!Objects.equals(INTEGER_NUMBER_1, updateRowsInt))
                return ReturnUtils.S_returnUpdateFailureInfo("club_badge_consignee(saveOrUpdateConsigneeInfo)");
            // Update success.
        }

        /*BadgeConsignee searchedBadgeConsigneeEntity = badgeConsigneeService.selectBadgeConsigneeByUserId(userId);

        if (null == searchedBadgeConsigneeEntity) { // The user didn't save its information. Insert it into our database.
            int insertRowsInt = badgeConsigneeService.insertBadgeConsignee(insertedOrUpdatedBadgeConsigneeEntity);

            // Insert failure.
            if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
                return ReturnUtils.S_returnCreateFailureInfo("club_badge_consignee(saveOrUpdateConsigneeInfo)");
            // Insert success.

        } else { // The user has saved its information. The older information should be updated.
            int updateRowsInt = badgeConsigneeService.updateBadgeConsigneeByUserId(insertedOrUpdatedBadgeConsigneeEntity);

            // Update failure.
            if (!Objects.equals(INTEGER_NUMBER_1, updateRowsInt))
                return ReturnUtils.S_returnUpdateFailureInfo("club_badge_consignee(saveOrUpdateConsigneeInfo)");
            // Update success.

        }*/

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, insertedOrUpdatedBadgeConsigneeEntity.toJson(insertedOrUpdatedBadgeConsigneeEntity));

    }

    @ApiOperation(value = "生成活动内容图片", httpMethod = "GET")
    @RequestMapping(value = "/generateActivityPicture")
    @ResponseBody
    public String generateActivityPicture(Long activityId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");

        // Initialization.
        ActivityDetail searchedActivityDetailEntity = new ActivityDetail();
        // Assign variables.
        searchedActivityDetailEntity.setActivityId(activityId);

        // Search for the activity and detail's information.
        ActivityDetail returnedActivityDetailEntity = activityDetailService.selectActivityDetailById(searchedActivityDetailEntity);
        // There's no such activity in our database. Something wrong here.
        if (EmptyUtils.isEmpty(returnedActivityDetailEntity))
            return ReturnUtils.S_returnNoSuchInformationFailureInfo("club_activity(activityDetail)");
        // Read successfully. We've found the activity and detail.

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, returnedActivityDetailEntity.toJson(returnedActivityDetailEntity));

    }

    @ApiOperation(value = "活动感兴趣/不感兴趣", httpMethod = "GET")
    @RequestMapping(value = "/likeActivity")
    @ResponseBody
    public String likeActivity(Long activityId, Long userId) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(activityId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("activityId(Long)");
        if (EmptyUtils.isEmpty(userId)) return ReturnUtils.S_returnParameterIsEmptyFailureInfo("userId(Long)");

        ActivityLike searchedActivityLikeEntity = new ActivityLike();
        searchedActivityLikeEntity.setActivityId(activityId);
        searchedActivityLikeEntity.setUserId(userId);

        List<ActivityLike> activityLikesList = activityLikeService.selectActivityLike(searchedActivityLikeEntity);
        if (INTEGER_NUMBER_0 == activityLikesList.size()) {
            int insertRowsInt = activityLikeService.insertActivityLike(searchedActivityLikeEntity);
            // Insert failure.
            if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
                return ReturnUtils.S_returnCreateFailureInfo("club_activity_like(likeActivity)");
            // Insert success.
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, searchedActivityLikeEntity.toJson(searchedActivityLikeEntity));
        } else {
            ActivityLike updatedActivityLikeEntity = activityLikesList.get(0);

            updatedActivityLikeEntity.setState(INTEGER_NUMBER_2);
            int updateRowsInt = activityLikeService.updateActivityLike(updatedActivityLikeEntity); // Update failure.
            if (!Objects.equals(INTEGER_NUMBER_1, updateRowsInt))
                return ReturnUtils.S_returnUpdateFailureInfo("club_activity_like(likeActivity)");
            // Update success.
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, updatedActivityLikeEntity.toJson(updatedActivityLikeEntity));
        }

    }

    /**
     * @param routePicturesList
     * @param concatenatedString
     * @return concatenatedString
     * @Description Concatenate route pictures' url string with concatenatedString.
     */
    private String P_concatenateStringWithCharacters(List<RoutePicture> routePicturesList, String concatenatedString) {

        StringBuilder stringBuilder = new StringBuilder();
        routePicturesList.stream().forEach(eachRoutePictureEntity -> {
            String routePictureUrlString = eachRoutePictureEntity.getRoutePictureUrl();
            if (0 == stringBuilder.length()) {
                stringBuilder.append(routePictureUrlString);
            } else {
                stringBuilder.append(concatenatedString).append(routePictureUrlString);
            }
        });

        return stringBuilder.toString();

    }

    /**
     * @param activityPicturesList
     * @param concatenatedString
     * @return concatenatedString
     * @Description Concatenate activity pictures' url string with concatenatedString.
     */
    private String P_concatenateStringWithCharacters2(List<ActivityPicture> activityPicturesList, String concatenatedString) {

        StringBuilder stringBuilder = new StringBuilder();
        activityPicturesList.stream().forEach(eachActivityPictureEntity -> {
            String activityPictureUrlString = eachActivityPictureEntity.getActivityPictureUrl();
            if (0 == stringBuilder.length()) {
                stringBuilder.append(activityPictureUrlString);
            } else {
                stringBuilder.append(concatenatedString).append(activityPictureUrlString);
            }
        });

        return stringBuilder.toString();

    }

    /**
     * @param badgeCarouselPicturesList
     * @param concatenatedString
     * @return concatenatedString
     * @Description Concatenate badge carousel pictures' url string with concatenatedString.
     */
    private String P_concatenateStringWithCharacters3(List<BadgeCarouselPicture> badgeCarouselPicturesList, String concatenatedString) {

        StringBuilder stringBuilder = new StringBuilder();
        badgeCarouselPicturesList.stream().forEach(eachBadgeCarouselPictureEntity -> {
            String badgePictureUrlString = eachBadgeCarouselPictureEntity.getBadgePictureUrl();
            if (0 == stringBuilder.length()) {
                stringBuilder.append(badgePictureUrlString);
            } else {
                stringBuilder.append(concatenatedString).append(badgePictureUrlString);
            }
        });

        return stringBuilder.toString();

    }

    /**
     * @param downloadUrl
     * @param userEmail
     * @return
     * @Description Send email with attached file(s).
     */
    private void P_sendEmailWithAttachedFiles(String downloadUrl, String userEmail) {

        // Initialization.
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(EMAIL_FROM);
            helper.setTo(userEmail);
            helper.setSubject(EMAIL_SUBJECT);
            helper.setText(EMAIL_TEXT);
            URL url = new URL(downloadUrl);
            helper.addAttachment("gpxFile.gpx", new ByteArrayResource(IOUtils.toByteArray(url.openStream())));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Thread(() -> javaMailSender.send(mimeMessage)).start();

    }

    /**
     * @param routeClickCount
     * @param routeId
     * @return updateRowsInt
     * @Description Count route clicks.
     */
    private int P_countRouteClicks(Integer routeClickCount, Long routeId) {

        // Initialization.
        Route updatedRouteEntity = new Route();
        // Assign variables.
        updatedRouteEntity.setRouteId(routeId);
        updatedRouteEntity.setRouteClickCount(routeClickCount + 1);

        // Update route clicks.
        int updateRowsInt = routeService.updateRoute(updatedRouteEntity);

        return updateRowsInt;

    }

    /**
     * @return citiesHashMap
     * @Description Get city map(cityId and cityName).
     */
    private Map<Integer, String> P_getCityMap() {

        List<City> returnedCitiesList = cityService.selectCityDropdownList();
        Map<Integer, String> citiesHashMap = new HashMap<>();
        returnedCitiesList.forEach(eachCityEntity -> citiesHashMap.put(eachCityEntity.getCityId().intValue(), eachCityEntity.getCityNameCH()));

        return citiesHashMap;

    }

    private String _GetAccessToken() {

        String accessTokenString = "";
        try {
            String getResponseString = sendGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxad24a51a97d67b92&secret=08ab3c84f258f787d0302c6194b0850e");
            log.info("getResponseString: {}", getResponseString);
            JSONObject jsonObject = JSONObject.parseObject(getResponseString);
            accessTokenString = jsonObject.getString("access_token");
        } catch (Exception e) {
            log.error("_GetAccessToken Exception!!!");
            e.printStackTrace();
        }

        return accessTokenString;

    }

    private String _SendSubscribeMessage(String accessTokenString, String parametersJsonString) {

        String postResponseString = "";
        try {
            postResponseString = sendPost(accessTokenString, parametersJsonString, "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token={accessToken}");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return postResponseString;

    }

    /**
     * @param urlString
     * @return getResponseString
     * @Description GET request.
     */
    private String sendGet(String urlString) {

        log.info("urlString: {}", urlString);
        return restTemplate.getForObject(urlString, String.class);

    }

    /**
     * @param accessTokenString
     * @param urlString
     * @return postResponseBodyString
     * @Description POST request
     */
    private String sendPost(String accessTokenString, String parametersJsonString, String urlString) {

        // 创建请求头。
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Map<String, String> params = new HashMap<>();
        params.put("accessToken", accessTokenString);

        HttpEntity<String> entity = new HttpEntity<>(parametersJsonString, headers);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlString, entity, String.class, params);

        return responseEntity.getBody();

    }

}

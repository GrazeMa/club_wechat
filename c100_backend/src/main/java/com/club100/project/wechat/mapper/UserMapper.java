package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.User;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:35:50
 * @Description: 用户 Mapper 接口。
 */
public interface UserMapper {

    int insertUser(User user);

    User selectOneById(User user);

    User selectUser(User user);

    int updateUserCity(User user);

    int updateUserMarkTimes(User user);

    int deleteByUserIds(Long... id);

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 21:32:44
 * @Description: 活动路线 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityRoute extends BaseEntity implements Serializable {

    private Long activityRouteId; // Primary Key ID。

    private Double routeClimb; // 路线爬升（单位：m）。
    private Double routeDifficultyLevel; // 路线难度（单位：颗星）。
    private Double routeDistance; // 路线距离（单位：km）。
    private Integer routeDayN; // 第几天活动。
    private Integer state; // 路线状态（1：可用；2：废弃。）。
    private Long activityId; // 活动主键。
    private String routeGpxFileUrl; // GPX 文件链接 URL。
    private String routeJsonFileUrl; // Json 文件链接 URL。
    private Long sysUserId;
    private String activityName;

    public String toJson(ActivityRoute activityRoute) {

        // ActivityRoute 对象转 JSON。
        return JSON.toJSONString(activityRoute);

    }

}

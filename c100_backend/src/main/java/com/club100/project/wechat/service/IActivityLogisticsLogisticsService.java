package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityLogisticsLogistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/17 11:18:06
 * @Description: 活动后勤保障服务类。
 */
public interface IActivityLogisticsLogisticsService {

    /**
     * 查询活动后勤保障信息。
     *
     * @param activityLogisticsLogistics
     * @return
     */
    List<ActivityLogisticsLogistics> selectActivityLogisticsLogisticsList(ActivityLogisticsLogistics activityLogisticsLogistics);

}

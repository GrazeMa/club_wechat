package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Product;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/12 23:28:06
 * @Description: 商品服务类。
 */
public interface IProductService {

    /**
     * 插入一条商品信息。
     *
     * @param product
     * @return
     */
    int insertProduct(Product product);

    /**
     * 批量插入多条商品信息。
     *
     * @param productList
     * @return
     */
    int insertProductInBatch(List<Product> productList);

    /**
     * 查询商品信息。
     *
     * @param product
     * @return
     */
    List<Product> selectProductList(Product product);

    /**
     * 查询商品信息。
     *
     * @param product
     * @return
     */
    List<Product> selectProductListWithPrivilege(Product product);

    /**
     * 删除商品关联信息。
     *
     * @param activityId
     * @return
     */
    int deleteProduct(Long... activityId);

}

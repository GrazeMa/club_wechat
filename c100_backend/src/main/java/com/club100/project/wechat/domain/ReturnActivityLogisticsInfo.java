package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/08 17:42:18
 * @Description: 活动后勤保障返回值。
 */
@AllArgsConstructor
public class ReturnActivityLogisticsInfo implements Serializable {

    public Long returnActivityLogisticsInfoId;
    public String returnActivityNameInfo;
    public Long[] returnActivityLogisticsInfo;
    
    public ReturnActivityLogisticsInfo() {}

    public String toJson(ReturnActivityLogisticsInfo returnActivityLogisticsInfo) {

        // ReturnActivityLogisticsInfo 对象转 JSON。
        return JSON.toJSONString(returnActivityLogisticsInfo);

    }

}

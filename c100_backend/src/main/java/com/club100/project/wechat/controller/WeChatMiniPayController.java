package com.club100.project.wechat.controller;

import com.alibaba.fastjson.JSONObject;
import com.club100.common.constant.WeChatMiniConstants;
import com.club100.common.constant.WechatConstant;
import com.club100.common.utils.ip.IpUtils;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.*;
import com.club100.project.wechat.utils.OrderCoderUtil;
import com.club100.project.wechat.utils.ReturnUtils;
import com.club100.project.wechat.wxpay.sdk.MyWeChatMiniPayConfig;
import com.club100.project.wechat.wxpay.sdk.WXPay;
import com.club100.project.wechat.wxpay.sdk.WXPayConstants;
import com.club100.project.wechat.wxpay.sdk.WXPayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/06 16:18:38
 * @Description: 微信小程序支付接口控制器。
 * @Version: 1.0
 */
@Api("小程序徽章支付接口")
@RequestMapping("/v1/wechatMiniPay")
@RestController
@Slf4j
public class WeChatMiniPayController {

    /*
    成员变量、静态变量（类变量）。
     */
    static final Integer INTEGER_NUMBER_0 = 0;
    static final Integer INTEGER_NUMBER_1 = 1;
    static final String EMPTY_STRING = "";

    // Badge consignee information.
    @Autowired
    private IBadgeConsigneeService badgeConsigneeService;
    // Badge order information.
    @Autowired
    private IBadgeOrderService badgeOrderService;
    // Badge information.
    @Autowired
    private IBadgeService badgeService;
    // Badge type information.
    @Autowired
    private IBadgeTypeService badgeTypeService;
    // User information.
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "小程序徽章支付--创建订单", httpMethod = "POST")
    @RequestMapping(value = "/createOrder")
    String createOrder(@ApiIgnore HttpServletRequest request,
                       @ApiParam("徽章个数") @RequestParam("badgeNumber") Integer badgeNumber,
                       @ApiParam("徽章主键 ID") @RequestParam("badgeId") Long badgeId,
                       @ApiParam("徽章类型主键 ID") @RequestParam("badgeTypeId") Long badgeTypeId,
                       @ApiParam("用户主键 ID") @RequestParam("userId") Long userId)
            throws Exception {

        String appId = WeChatMiniConstants.MINI_APP_ID;

        // Get openId from user table.
        User searchedUserEntity = new User();
        searchedUserEntity.setUserId(userId);

        User userEntity = userService.selectOneById(searchedUserEntity);
        if (null == userEntity) return ReturnUtils.S_returnReadFailureInfo("club_user(createOrder)");

        String openIdString = userEntity.getWxOpenId();

        // Get badge information from badge table.
        Badge badgeEntity = badgeService.selectBadgeById(badgeId);
        if (null == badgeEntity) return ReturnUtils.S_returnReadFailureInfo("club_badge(createOrder)");

        double badgePrice = badgeEntity.getBadgePrice();
        int badgePriceInt = (int) (badgePrice * 100);
        String totalFeeString = String.valueOf(badgePriceInt * badgeNumber);

//        String productIdString = String.valueOf(badgeEntity.getBadgeId());

        String badgeNameString = badgeEntity.getBadgeName();

        MyWeChatMiniPayConfig myWeChatMiniPayConfig = new MyWeChatMiniPayConfig();
        WXPay wxPay = new WXPay(myWeChatMiniPayConfig);
        Map<String, String> data = new HashMap<>();

        /*data.put("appid", appId); // 小程序号。
        data.put("mch_id", WeChatMiniConstants.MERCHANT_ID); // 商户号。
        String nonceStr = WXPayUtil.generateNonceStr();
        data.put("nonce_str", nonceStr); // 随机字符串。*/
        data.put("body", "购买徽章: " + badgeNameString); // 商品描述。
        String outTradeNo = OrderCoderUtil.getOrderCode(); // 商户系统内部订单号，要求 32 个字符内，只能是数字、大小写字母_-|*且在同一个商户号下唯一。详见商户订单号。
        data.put("out_trade_no", outTradeNo);
        data.put("fee_type", "CNY"); // 标价币种。
        data.put("total_fee", totalFeeString); // 金额，单位为分。测试时使用 1 分钱。
        data.put("spbill_create_ip", IpUtils.getIpAddr(request)); // 支持 IPV4 和 IPV6 两种格式的 IP 地址。调用微信支付 API 的机器 IP。
        data.put("notify_url", "https://api.club100.cn/v1/wxPay/success"); // 异步接收微信支付结果通知的回调地址，通知 url 必须为外网可访问的 url，不能携带参数。
        data.put("trade_type", "JSAPI"); // 此处指定为扫码支付，小程序支付填写"JSAPI"。
//        data.put("product_id", productIdString);// 此参数为二维码中包含的商品 ID，商户自行定义。
        data.put("openid", openIdString);

        BadgeConsignee searchedBadgeConsigneeEntity = badgeConsigneeService.selectOneBadgeConsigneeByUserId(userId);
        if (null == searchedBadgeConsigneeEntity)
            return ReturnUtils.S_returnReadFailureInfo("club_badge_consignee(createOrder)");

        BadgeType searchedBadgeTypeEntity = badgeTypeService.selectBadgeTypeById(badgeTypeId);
        if (null == searchedBadgeTypeEntity)
            return ReturnUtils.S_returnReadFailureInfo("club_badge_type(createOrder)");

        BadgeOrder insertedBadgeOrderEntity = new BadgeOrder();
        insertedBadgeOrderEntity.setBadgePrice(badgePrice);
        insertedBadgeOrderEntity.setTotalFeeYuan(badgePrice * badgeNumber);
        insertedBadgeOrderEntity.setBadgeId(badgeId);
        insertedBadgeOrderEntity.setProductId(badgeId);
        insertedBadgeOrderEntity.setUserId(userId);
        insertedBadgeOrderEntity.setBadgeName(badgeNameString);
        insertedBadgeOrderEntity.setBadgePictureUrl(badgeEntity.getBadgePictureUrl());
        insertedBadgeOrderEntity.setBadgeTypeName(searchedBadgeTypeEntity.getBadgeTypeName());
        insertedBadgeOrderEntity.setBadgeTypePictureUrl(searchedBadgeTypeEntity.getBadgeTypePictureUrl());
        insertedBadgeOrderEntity.setConsigneeAddress(searchedBadgeConsigneeEntity.getConsigneeAddress());
        insertedBadgeOrderEntity.setConsigneeDistrict(searchedBadgeConsigneeEntity.getConsigneeDistrict());
        insertedBadgeOrderEntity.setConsigneeMobile(searchedBadgeConsigneeEntity.getConsigneeMobile());
        insertedBadgeOrderEntity.setConsigneeName(searchedBadgeConsigneeEntity.getConsigneeName());
        insertedBadgeOrderEntity.setOutTradeNo(outTradeNo);
        insertedBadgeOrderEntity.setPackageName("顺丰");
        insertedBadgeOrderEntity.setPackageOrderNumber(EMPTY_STRING);
        insertedBadgeOrderEntity.setProductDescription("购买徽章: " + badgeNameString);
        insertedBadgeOrderEntity.setTotalFeeString(totalFeeString);
        insertedBadgeOrderEntity.setTradeType("JSAPI");

        int insertRowsInt = badgeOrderService.insertBadgeOrder(insertedBadgeOrderEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_badge_order(createOrder)");
        // Insert success.

        try {
            Map<String, String> rMap = wxPay.unifiedOrder(data);
            String return_code = (String) rMap.get("return_code");
            String result_code = (String) rMap.get("result_code");
            Map resultMap = new HashMap();
            if ("SUCCESS".equals(return_code) && return_code.equals(result_code)) {
                String prepayId = (String) rMap.get("prepay_id");
                resultMap.put("appId", appId); // 小程序号。
                // 这边要将返回的时间戳转化成字符串，不然小程序端调用wx.requestPayment方法会报签名错误。
                resultMap.put("timeStamp", WXPayUtil.getCurrentTimestamp() + "");
                resultMap.put("nonceStr", WXPayUtil.generateNonceStr());
                resultMap.put("signType", WXPayConstants.HMACSHA256);
                resultMap.put("package", "prepay_id=" + prepayId);
                // 再次签名，这个签名用于小程序端调用 wx.requestPayment 方法。
                String paySign = WXPayUtil.generateSignature(resultMap, WeChatMiniConstants.MERCHANT_KEY, WXPayConstants.SignType.HMACSHA256);
                resultMap.put("paySign", paySign);

                resultMap.put("outTradeNo", outTradeNo);
                resultMap.put("packageStr", resultMap.get("package"));
                JSONObject jsonObject = new JSONObject(resultMap);
                return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, jsonObject.toString());
            } else {
                return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, "Fail...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, "Exception...");
        }

    }

    @ApiOperation(value = "小程序徽章支付--更新订单状态", httpMethod = "POST")
    @RequestMapping(value = "/notifyWeiXinPay")
    @ResponseBody
    String notifyWeiXinPay(@ApiIgnore HttpServletRequest res, @ApiIgnore HttpServletResponse rsp, @ApiParam("订单编号") @RequestParam("orderId") String orderId) throws Exception {

        BadgeOrder updatedBadgeOrderEntity = new BadgeOrder();
        updatedBadgeOrderEntity.setOutTradeNo(orderId);
        updatedBadgeOrderEntity.setBadgeOrderState(INTEGER_NUMBER_1);

        int insertRowsInt = badgeOrderService.updateBadgeOrderByOrderId(updatedBadgeOrderEntity);
        // Insert failure.
        if (!Objects.equals(INTEGER_NUMBER_1, insertRowsInt))
            return ReturnUtils.S_returnCreateFailureInfo("club_badge_order(notifyWeiXinPay)");
        // Insert success.

        /*InputStream inStream = res.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        String resultxml = new String(outSteam.toByteArray(), "utf-8");
        Map<String, String> params = WXPayUtil.xmlToMap(resultxml);
        outSteam.close();
        inStream.close();
        //支付结果返回，最后需要返回xml格式的数据
        Map<String, String> return_data = new HashMap<String, String>();
        if (!"SUCCESS".equals(params.get("return_code"))) {
            //没有支付成功
            return_data.put("return_code", "FAIL");
            return_data.put("return_msg", "return_code不正确");
            return WXPayUtil.mapToXml(return_data);
        } else {
            //返回成功了
            //商户订单号
            String outTradeNo = params.get("out_trade_no");
            //订单金额
            String totalFee = params.get("total_fee");
            //支付完成时间
            String timeEnd = params.get("time_end");
            //微信支付订单号
            String tradeNo = params.get("transaction_id");
            log.info(" -- 商户订单号 -- : {}", outTradeNo);
            log.info(" -- tradeNo -- : {}", tradeNo);
            return_data.put("return_code", "SUCCESS");
            return_data.put("return_msg", "OK");
            return WXPayUtil.mapToXml(return_data);
        }*/

        return ReturnUtils.S_returnJsonInfo(INTEGER_NUMBER_0, EMPTY_STRING, EMPTY_STRING);

    }

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/28 16:33:56
 * @Description: 徽章类型 Entity。
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeType extends BaseEntity implements Serializable {

    private Long badgeTypeId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long badgeId; // 徽章主键 ID。
    private String badgeTypeName; // 徽章类型名。
    private String badgeTypePictureUrl; // 徽章图片链接 URL。

    public String toJson(BadgeType badgeType) {

        // BadgeType 对象转 JSON。
        return JSON.toJSONString(badgeType);

    }

}

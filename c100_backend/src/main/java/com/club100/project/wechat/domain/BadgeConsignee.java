package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/18 15:19:28
 * @Description: 徽章收货人 Entity。
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeConsignee extends BaseEntity implements Serializable {

    private Long badgeConsigneeId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long userId; // 用户主键 ID。
    private String consigneeAddress; // 收货人详细地址。
    private String consigneeDistrict; // 收货人所在地区。
    private String consigneeMobile; // 收货人手机号。
    private String consigneeName; // 收货人姓名。

    public String toJson(BadgeConsignee badgeConsignee) {

        // BadgeConsignee 对象转 JSON。
        return JSON.toJSONString(badgeConsignee);

    }

}

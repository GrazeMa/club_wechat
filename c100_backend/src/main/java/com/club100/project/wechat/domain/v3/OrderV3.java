package com.club100.project.wechat.domain.v3;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderV3 extends BaseEntity implements Serializable {


  /**
   * time_expire : 2018-06-08T10:34:56+08:00
   * amount : {"total":100,"currency":"CNY"}
   * settle_info : {"profit_sharing":false,"subsidy_amount":10}
   * sp_mchid : 1230000109
   * description : Image形象店-深圳腾大-QQ公仔
   * sub_appid : wxd678efh567hg6999
   * notify_url :  https://www.weixin.qq.com/wxpay/pay.php
   * payer : {"sp_openid":"oUpF8uMuAJO_M2pxb1Q9zNjWeS6o","sub_openid":"oUpF8uMuAJO_M2pxb1Q9zNjWeS6o"}
   * sp_appid : wx8888888888888888
   * out_trade_no : 1217752501201407033233368018
   * goods_tag : WXG
   * sub_mchid : 1900000109
   * attach : 自定义数据说明
   * detail : {"invoice_id":"wx123","goods_detail":[{"goods_name":"iPhoneX 256G","wechatpay_goods_id":"1001","quantity":1,"merchant_goods_id":"商品编码","unit_price":828800},{"goods_name":"iPhoneX 256G","wechatpay_goods_id":"1001","quantity":1,"merchant_goods_id":"商品编码","unit_price":828800}],"cost_price":608800}
   * scene_info : {"store_info":{"address":"广东省深圳市南山区科技中一道10000号","area_code":"440305","name":"腾讯大厦分店","id":"0001"},"device_id":"013467007045764","payer_client_ip":"14.23.150.211"}
   */

  private String time_expire;
  private Amount amount;
  private SettleInfo settle_info;
  private String sp_mchid;
  private String description;
  private String sub_appid;
  private String notify_url;
  private Payer payer;
  private String sp_appid;
  private String out_trade_no;
  private String goods_tag;
  private String sub_mchid;
  private String attach;
  private Detail detail;
  private SceneInfo scene_info;

}

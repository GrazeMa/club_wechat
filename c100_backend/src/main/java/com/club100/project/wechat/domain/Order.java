package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.aspectj.lang.annotation.Excel;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/03 21:52:43
 * @Description: 订单 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order extends BaseEntity implements Serializable {

    @Excel(name = "订单序号", cellType = Excel.ColumnType.NUMERIC)
    private Long orderId; // Primary Key ID。

    @Excel(name = "订单状态", readConverterExp = "0=支付中,1=支付成功,2=已退款")
    private Integer orderStatus; // 订单状态。
    @Excel(name = "数据范围", readConverterExp = "0=女,1=男")
    private Integer user_sex; // 性别（0：女 1：男）。
    private Long activity_id; // 活动主键 ID。
    private Long sysUserId; // 管理后台登录用户主键。
    @Excel(name = "活动名称")
    private String activityName; // 活动名称。
    @Excel(name = "证件号")
    private String card_code; // 证件号。
    @Excel(name = "联系方式")
    private String mobile; // 联系方式。
    private String out_trade_no; // 订单号。
    @Excel(name = "用户姓名")
    private String user_name; // 用户姓名。

    public String toJson(Order order) {

        // Order 对象转 JSON。
        return JSON.toJSONString(order);

    }

}

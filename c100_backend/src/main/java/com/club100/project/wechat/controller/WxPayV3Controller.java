package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author
 */
@Api(tags = "微信支付接口V3")
@RestController
@RequestMapping("/v3/wxPay")
public class WxPayV3Controller extends BaseController {


}


package com.club100.project.wechat.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/31 19:30:57
 * @Description: 邮件 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mail implements Serializable {

    private String mailContent; //邮件内容。
    private String mailReceiver; //邮件接收人。
    private String mailSubject; //邮件主题。

}

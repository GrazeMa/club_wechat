package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/3/1 6:00 PM
 * @Description: 活动感兴趣 Entity。
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityLike extends BaseEntity implements Serializable {

    private Long activityLikeId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long activityId; // 活动的主键 ID。
    private Long userId; // 用户的主键 ID。

    public String toJson(ActivityLike activityLike) {

        // ActivityLike 对象转 JSON。
        return JSON.toJSONString(activityLike);

    }

}

package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Order;
import com.club100.project.wechat.mapper.OrderMapper;
import com.club100.project.wechat.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/03 22:50:05
 * @Description: 订单服务实现类。
 */
@Slf4j
@Service
public class GrazerOrderServiceImpl implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> selectOrderList(Order order) {
        return orderMapper.selectOrderList(order);
    }

}

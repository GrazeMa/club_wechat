package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.PizzaGetterUser;
import com.club100.project.wechat.mapper.PizzaGetterUserMapper;
import com.club100.project.wechat.service.IPizzaGetterUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 15:39:11
 * @Description: 已获披萨用户服务实现类。
 */
@Slf4j
@Service
public class PizzaGetterUserServiceImpl implements IPizzaGetterUserService {

    @Autowired
    private PizzaGetterUserMapper pizzaGetterUserMapper;

    @Override
    public List<PizzaGetterUser> selectPizzaGetterUser() {
        return pizzaGetterUserMapper.selectPizzaGetterUser();
    }

}

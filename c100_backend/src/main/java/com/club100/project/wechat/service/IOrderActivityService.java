package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.OrderActivity;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 18:31:48
 * @Description: 我的活动服务类。
 */
public interface IOrderActivityService {

    /**
     * 列举我的活动信息。
     *
     * @param orderActivity
     * @return
     */
    List<OrderActivity> selectOrderActivityList(OrderActivity orderActivity);

}

package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BadgeCarouselPicture;
import com.club100.project.wechat.mapper.BadgeCarouselPictureMapper;
import com.club100.project.wechat.service.IBadgeCarouselPictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/19 16:13:48
 * @Description: 徽章轮播图服务实现类。
 * @Version: 1.0
 */
@Slf4j
@Service
public class BadgeCarouselPictureServiceImpl implements IBadgeCarouselPictureService {

    @Autowired
    private BadgeCarouselPictureMapper badgeCarouselPictureMapper;

    @Override
    public List<BadgeCarouselPicture> selectBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture) {
        return badgeCarouselPictureMapper.selectBadgeCarouselPicture(badgeCarouselPicture);
    }

    @Override
    public List<BadgeCarouselPicture> selectBadgeCarouselPictureByBadgeId(Long badgeId) {
        return badgeCarouselPictureMapper.selectBadgeCarouselPictureByBadgeId(badgeId);
    }

    @Override
    public int insertBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture) {
        return badgeCarouselPictureMapper.insertBadgeCarouselPicture(badgeCarouselPicture);
    }

    @Override
    public int insertBadgeCarouselPictureInBatch(List<BadgeCarouselPicture> badgeCarouselPictureList) {
        return badgeCarouselPictureMapper.insertBadgeCarouselPictureInBatch(badgeCarouselPictureList);
    }

    @Override
    public int deleteBadgeCarouselPicture(Long... badgeId) {
        return badgeCarouselPictureMapper.deleteBadgeCarouselPicture(badgeId);
    }

}

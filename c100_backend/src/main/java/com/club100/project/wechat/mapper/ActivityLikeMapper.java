package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityLike;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/03/01 19:38:28
 * @Description: 活动感兴趣 Mapper 接口。
 */
public interface ActivityLikeMapper {

    int selectActivityLikeCountList(ActivityLike activityLike);

    List<ActivityLike> selectActivityLike(ActivityLike activityLike);

    int insertActivityLike(ActivityLike activityLike);

    int updateActivityLike(ActivityLike activityLike);

}

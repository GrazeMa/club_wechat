package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BadgeConsignee;
import com.club100.project.wechat.mapper.BadgeConsigneeMapper;
import com.club100.project.wechat.service.IBadgeConsigneeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/18 16:46:30
 * @Description: 徽章收货人服务实现类。
 * @Version: 1.0
 */
@Slf4j
@Service
public class BadgeConsigneeServicImpl implements IBadgeConsigneeService {

    @Autowired
    private BadgeConsigneeMapper badgeConsigneeMapper;

    @Override
    public List<BadgeConsignee> selectBadgeConsigneeList(BadgeConsignee badgeConsignee) {
        return badgeConsigneeMapper.selectBadgeConsigneeList(badgeConsignee);
    }

    @Override
    public BadgeConsignee selectBadgeConsigneeById(Long badgeConsigneeId) {
        return badgeConsigneeMapper.selectBadgeConsigneeById(badgeConsigneeId);
    }

    @Override
    public List<BadgeConsignee> selectBadgeConsigneeByUserId(Long userId) {
        return badgeConsigneeMapper.selectBadgeConsigneeByUserId(userId);
    }

    @Override
    public BadgeConsignee selectOneBadgeConsigneeByUserId(Long userId) {
        return badgeConsigneeMapper.selectOneBadgeConsigneeByUserId(userId);
    }

    @Override
    public int insertBadgeConsignee(BadgeConsignee badgeConsignee) {
        return badgeConsigneeMapper.insertBadgeConsignee(badgeConsignee);
    }

    @Override
    public int updateBadgeConsignee(BadgeConsignee badgeConsignee) {
        return badgeConsigneeMapper.updateBadgeConsignee(badgeConsignee);
    }

    @Override
    public int updateBadgeConsigneeByUserId(BadgeConsignee badgeConsignee) {
        return badgeConsigneeMapper.updateBadgeConsigneeByUserId(badgeConsignee);
    }

}

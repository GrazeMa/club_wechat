package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.MarkPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 12:27:16
 * @Description: 打卡图片 Mapper 接口。
 */
public interface MarkPictureMapper {

    MarkPicture selectLastPicture(MarkPicture markPicture);

    int insertMarkPicture(MarkPicture markPicture);

    int insertMarkPictureInBatch(List<MarkPicture> markPictureList);

}

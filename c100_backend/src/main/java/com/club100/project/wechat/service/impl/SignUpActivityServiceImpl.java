package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.SignUpActivity;
import com.club100.project.wechat.mapper.SignUpActivityMapper;
import com.club100.project.wechat.service.ISignUpActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/13 10:08:14
 * @Description: 报名活动服务实现类。
 */
@Slf4j
@Service
public class SignUpActivityServiceImpl implements ISignUpActivityService {

    @Autowired
    private SignUpActivityMapper signUpActivityMapper;

    @Override
    public List<SignUpActivity> selectSignUpActivity(SignUpActivity signUpActivity) {
        return signUpActivityMapper.selectSignUpActivity(signUpActivity);
    }

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 16:23:35
 * @Description: 领队 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Leader extends BaseEntity implements Serializable {

    private Long leaderId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private String leaderName; // 领队姓名。
    private String leaderPhone; // 领队号码。
    private String leaderWechat; // 领队微信号。

    public String toJson(Leader leader) {

        // Leader 对象转 JSON。
        return JSON.toJSONString(leader);

    }

}

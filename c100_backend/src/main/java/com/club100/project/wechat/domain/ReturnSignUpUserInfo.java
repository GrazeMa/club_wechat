package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 21:11:26
 * @Description: 活动报名用户返回值 Entity。
 */
public class ReturnSignUpUserInfo implements Serializable {

    public Integer signUpUserCount; // 活动报名用户人数。
    public List<SignUpOrderUser> signUpUsersList; // 活动报名用户信息。

    public String toJson(ReturnSignUpUserInfo returnSignUpUserInfo) {

        // ReturnSignUpUserInfo 对象转 JSON。
        return JSON.toJSONString(returnSignUpUserInfo);

    }

}

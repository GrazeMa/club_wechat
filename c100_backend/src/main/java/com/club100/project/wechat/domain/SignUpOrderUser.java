package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/8 20:53:23
 * @Description: 报名人数 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpOrderUser extends BaseEntity implements Serializable {

    private Long signUpOrderUserId; // Primary Key ID。

    private Long activityId; // 活动主键。
    private Long userId; // 用户主键。
    private String wxUserAvatarUrl; // 用户微信头像 URL。

    public String toJson(SignUpOrderUser signUpOrderUser) {

        // SignUpOrderUser 对象转 JSON。
        return JSON.toJSONString(signUpOrderUser);

    }

}

package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfo extends BaseEntity implements Serializable {

  /**
   * contact_type : 65
   * contact_name : pVd1HJ6zyvPedzGaV+X3qtmrq9bb9tPROvwia4ibL+F6mfjbzQIzfb3HHL  EjZ4YiR/cJiCrZxnAqi+pjeKIEdkwzXRAI7FUhrfPK3SNjaBTEu9GmsugMIA9r3x8   87Q+ODuC8HH2nzAn7NGpE/e3yiHgWhk0ps5k5DP/2qIdGdONoDzZelrxCl/NWWNUyB93K9F+  jC1JX2IMttdY+aQ6zBlw0xnOiNW6Hzy7UtC+xriudjD5APomty7 /mYNxLMpRSvWKIjO  v/69bDnuC4EL5Kz4jBHLiCyOb+tI0m2qhZ9evAM+Jv1z0NVa8MRtelw/wDa4SzfeespQO/0kjiwfqdfg==
   * contact_id_card_number : UZFETyabYFFlgvGh6R4vTzDELiPas3jC94/srAbSaPa  WSM+1rhGLcsybPLeJdVOpCeDxNBMDK+/N0nOiZZ4ka9+5RgzvA2rJx+NztYUb    N209rq0Y/NP50T9yk0m6A4xUoFWgk/8qteRHtP5VHahNhSh8nHo31V33t8edSlN9HJG6diRj7p  5JPImDyM1q56+p25edl3+cOtuZtj8TJDl/hB+GaWve9X1WUpkZbKlJgBxp+Xha W707k9XrILvfD+rSGTOeU/ev4/OiEb5W4WPGJ+3iLoQvhnz3+aQZX9+gn9uR zWcHu2Kr17fhsM+MRkgVcwzI2UqhR9iuGTunRPRVFg==
   * mobile_phone : Uy5Hb0c5Se/orEbrWze/ROHu9EPAs/CigDlJ2fnyzC1ppJNBOYGyc  89xUgZZoPIRnPWsvJ5oevXNdBK3IUz9WHs9iQKpeUksvoLQMsykc8LDu   7MMpayKWNVozldcRugH++MltTBKWTkv/oOcwkZattMGgP4CtpbN6djDK1PcAmIDgdFD2ZvCIDCtJ  g1V/YafUBJdBTvNLXa/jNzjZaypsUn1BRO6fx8aaNn7XyTv7JrfQZE4  UDH4gfMFOj8YDqQ+IvDbkuNhaLZExOEz/UcnxeN5mfGr2MdkPr  OzF+xJmUZUn1nafZxENrqcBszhYQUlu5zn6o2uZpBhAsQwd3QAjw==
   * contact_email : Uy5Hb0c5Se/orEbrWze/ROHu9EPAs/CigDlJ2fnyzC1ppJNBOY  Gyc89xUgZZoPIRnPWsvJ5oevXNdBK3IUz9WHs9iQKpeUksvoLQMsykc8LDu7MMpay   KWNVozldcRugH++MltTBKWTkv/oOcwkZattMGgP4CtpbN6djDK1PcAmIDgdFD2ZvCIDCtJg1V  /YafUBJdBTvNLXa/jNzjZaypsUn1BRO6fx8aaNn7XyTv7JrfQZE4UDH4gfMFOj8YD qQ+IvDbkuNhaLZExOEz/UcnxeN5mfGr2MdkPrOzF+xJmUZUn1nafZxENrqcBszhYQUlu5zn6o2uZpBhAsQwd3QAjw==
   */

  private String contact_type;
  private String contact_name;
  private String contact_id_card_number;
  private String mobile_phone;
  private String contact_email;
}

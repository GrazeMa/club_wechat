package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Disclaimer;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 21:31:14
 * @Description: 免责声明 Mapper 接口。
 */
public interface DisclaimerMapper {

    int insertDisclaimer(Disclaimer disclaimer);

    Disclaimer selectDisclaimerById(Long disclaimerId);

}

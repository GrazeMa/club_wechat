package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.MarkPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 14:58:19
 * @Description: 打卡图片服务类。
 */
public interface IMarkPictureService {

    /**
     * 查询一条记录。
     * @param markPicture
     * @return
     */
    MarkPicture selectLastPicture(MarkPicture markPicture);

    /**
     * 插入（一条）打卡图片信息。
     *
     * @param markPicture
     * @return
     */
    int insertMarkPicture(MarkPicture markPicture);

    /**
     * 批量插入（多条）打卡图片信息。
     *
     * @param markPictureList
     * @return
     */
    int insertMarkPictureInBatch(List<MarkPicture> markPictureList);

}

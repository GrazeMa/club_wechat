package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityLogistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 11:51:51
 * @Description: 活动后勤保障关联 Mapper 接口。
 */
public interface ActivityLogisticsMapper {

    int insertActivityLogistics(ActivityLogistics activityLogistics);

    int insertActivityLogisticsInBatch(List<ActivityLogistics> activityLogisticsList);

    List<ActivityLogistics> selectActivityLogisticsList(ActivityLogistics activityLogistics);

    List<ActivityLogistics> selectActivityLogisticsListWithPrivilege(ActivityLogistics activityLogistics);

    int deleteActivityLogistics(Long... activityId);

}

package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.User;
import com.club100.project.wechat.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 20:25:10
 * @Description: 用户控制器。
 */
@RequestMapping("/v1/user")
@RestController
public class UserController extends BaseController {

//    private String PREFIX = "/v1/user/";

    @Autowired
    private IUserService userService;

    @PostMapping
    public AjaxResult insert(@RequestBody User user) {

        userService.insert(user);
        return AjaxResult.success();

    }

    /*@GetMapping
    public TableDataInfo page(User user) {

        startPage();
        return getDataTable(userService.page(user));

    }*/

    /*@GetMapping("mp")
    public AjaxResult mpPage(User user) {

        startPage();
        return AjaxResult.success(getPageTable(userService.page(user)));

    }*/

    @DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(userService.deleteByUserIds(id));

    }

}

package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityLike;
import com.club100.project.wechat.mapper.ActivityLikeMapper;
import com.club100.project.wechat.service.IActivityLikeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/03/01 19:49:25
 * @Description: 活动感兴趣服务实现类。
 * @Version: 1.0
 */
@Slf4j
@Service
public class ActivityLikeServiceImpl implements IActivityLikeService {

    @Autowired
    private ActivityLikeMapper activityLikeMapper;

    @Override
    public int selectActivityLikeCountList(ActivityLike activityLike) {
        return activityLikeMapper.selectActivityLikeCountList(activityLike);
    }

    @Override
    public List<ActivityLike> selectActivityLike(ActivityLike activityLike) {
        return activityLikeMapper.selectActivityLike(activityLike);
    }

    @Override
    public int insertActivityLike(ActivityLike activityLike) {
        return activityLikeMapper.insertActivityLike(activityLike);
    }

    @Override
    public int updateActivityLike(ActivityLike activityLike) {
        return activityLikeMapper.updateActivityLike(activityLike);
    }

}

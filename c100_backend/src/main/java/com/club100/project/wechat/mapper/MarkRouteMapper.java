package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:38:42
 * @Description: 打卡路线 Mapper 接口。
 */
public interface MarkRouteMapper {

    int insertMarkRoute(MarkRoute markRoute);

    List<MarkRoute> selectMarkRouteByUserIDList(MarkRoute markRoute);

    Integer selectMarkRouteCountList(MarkRoute markRoute);

    int selectMarkRouteCountByUserId(@Param("userId") Long userId);

    MarkRoute selectMarkRouteOne(MarkRoute markRoute);

    List<MarkRoute> selectMarkRouteList(MarkRoute markRoute);

    Integer updateMarkRouteById(MarkRoute markRoute);

}

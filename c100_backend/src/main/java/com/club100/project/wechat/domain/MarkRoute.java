package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:32:26
 * @Description: 打卡路线 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkRoute extends BaseEntity implements Serializable {

    private Long markRouteId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer isNewestMark; // 是否最新一次打卡（1：是；2：不是。）。
    private Integer state; // 打卡路线状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主表主键。
    private Long userId; // 用户主键。

    public String toJson(MarkRoute markRoute) {

        // MarkRoute 对象转 JSON。
        return JSON.toJSONString(markRoute);

    }

}

package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BadgeType;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/28 16:50:00
 * @Description: 徽章类型 Mapper 接口。
 */
public interface BadgeTypeMapper {

    List<BadgeType> selectBadgeTypeList(BadgeType badgeType);

    BadgeType selectBadgeTypeById(Long badgeTypeId);

    List<BadgeType> selectBadgeTypeByBadgeId(Long badgeId);

    int insertBadgeType(BadgeType badgeType);

}

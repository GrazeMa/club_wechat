package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BadgeOrder;
import com.club100.project.wechat.mapper.BadgeOrderMapper;
import com.club100.project.wechat.service.IBadgeOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/20 15:57:18
 * @Description: 徽章订单服务实现类。
 * @Version: 1.0
 */
@Slf4j
@Service
public class BadgeOrderServiceImpl implements IBadgeOrderService {

    @Autowired
    private BadgeOrderMapper badgeOrderMapper;

    @Override
    public BadgeOrder selectBadgeById(Long badgeOrderId) {
        return badgeOrderMapper.selectBadgeById(badgeOrderId);
    }

    @Override
    public List<BadgeOrder> selectBadgeOrderList(BadgeOrder badgeOrder) {
        return badgeOrderMapper.selectBadgeOrderList(badgeOrder);
    }

    @Override
    public BadgeOrder selectOneBadgeOrderByBadgeIdAndUserId(BadgeOrder badgeOrder) {
        return badgeOrderMapper.selectOneBadgeOrderByBadgeIdAndUserId(badgeOrder);
    }

    @Override
    public int insertBadgeOrder(BadgeOrder badgeOrder) {
        return badgeOrderMapper.insertBadgeOrder(badgeOrder);
    }

    @Override
    public int updateBadgeOrder(BadgeOrder badgeOrder) {
        return badgeOrderMapper.updateBadgeOrder(badgeOrder);
    }

    @Override
    public int updateBadgeOrderByOrderId(BadgeOrder badgeOrder) {
        return badgeOrderMapper.updateBadgeOrderByOrderId(badgeOrder);
    }

}

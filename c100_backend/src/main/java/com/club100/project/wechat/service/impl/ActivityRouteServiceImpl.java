package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityRoute;
import com.club100.project.wechat.mapper.ActivityRouteMapper;
import com.club100.project.wechat.service.IActivityRouteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 21:54:12
 * @Description: 活动路线服务实现类。
 */
@Slf4j
@Service
public class ActivityRouteServiceImpl implements IActivityRouteService {

    @Autowired
    private ActivityRouteMapper activityRouteMapper;

    @Override
    public int insertActivityRoute(ActivityRoute activityRoute) {
        return activityRouteMapper.insertActivityRoute(activityRoute);
    }

    @Override
    public ActivityRoute selectActivityRouteById(Long activityRouteId) {
        return activityRouteMapper.selectActivityRouteById(activityRouteId);
    }

    @Override
    public ActivityRoute selectActivityRouteByActivityId(Long activityId) {
        return activityRouteMapper.selectActivityRouteByActivityId(activityId);
    }

    @Override
    public List<ActivityRoute> selectActivityRoute(ActivityRoute activityRoute) {
        return activityRouteMapper.selectActivityRoute(activityRoute);
    }

    @Override
    public List<ActivityRoute> selectActivityRouteWithPrivilege(ActivityRoute activityRoute) {
        return activityRouteMapper.selectActivityRouteWithPrivilege(activityRoute);
    }

    @Override
    public int updateActivityRoute(ActivityRoute activityRoute) {
        return activityRouteMapper.updateActivityRoute(activityRoute);
    }

    @Override
    public int deleteByActivityRouteIds(Long[] activityRouteIds) {
        return activityRouteMapper.deleteByActivityRouteIds(activityRouteIds);
    }

}

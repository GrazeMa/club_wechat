package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusinessLicenseInfo extends BaseEntity implements Serializable {

  /**
   * business_license_copy : 47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2  XAUf-4KGprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4
   * business_license_number : 123456789012345678
   * merchant_name : 腾讯科技有限公司
   * legal_person : 张三
   */

  private String business_license_copy;
  private String business_license_number;
  private String merchant_name;
  private String legal_person;

  public String getBusiness_license_copy() {
    return business_license_copy;
  }

  public void setBusiness_license_copy(String business_license_copy) {
    this.business_license_copy = business_license_copy;
  }

  public String getBusiness_license_number() {
    return business_license_number;
  }

  public void setBusiness_license_number(String business_license_number) {
    this.business_license_number = business_license_number;
  }

  public String getMerchant_name() {
    return merchant_name;
  }

  public void setMerchant_name(String merchant_name) {
    this.merchant_name = merchant_name;
  }

  public String getLegal_person() {
    return legal_person;
  }

  public void setLegal_person(String legal_person) {
    this.legal_person = legal_person;
  }
}

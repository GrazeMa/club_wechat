package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 16:00:03
 * @Description: 获得披萨奖励用户返回值 Entity。
 */
public class ReturnPizzaGetterUserInfo implements Serializable {

    public Integer pizzaGetterUserCount; // 获得披萨奖励用户人数。
    public List<PizzaGetterUser> pizzaGetterUsersList; // 获得披萨奖励用户信息。

    public String toJson(ReturnPizzaGetterUserInfo returnPizzaGetterUserInfo) {

        // ReturnPizzaGetterUserInfo 对象转 JSON。
        return JSON.toJSONString(returnPizzaGetterUserInfo);

    }

}

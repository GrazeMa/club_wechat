package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Inspire;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:31:56
 * @Description: 激励 Mapper 接口。
 */
public interface InspireMapper {

    int insertInspire(Inspire inspire);

    List<Inspire> selectInspireAtMostTwoList(Inspire inspire);

    List<Inspire> selectInspireList();

    int deleteByInspireIds(Long... id);

}

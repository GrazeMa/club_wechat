package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BadgeCarouselPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/19 16:10:28
 * @Description: 徽章轮播图服务类。
 */
public interface IBadgeCarouselPictureService {

    /**
     * @param badgeCarouselPicture
     * @return
     * @Description 查询活徽章轮播图信息。
     */
    List<BadgeCarouselPicture> selectBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture);

    /**
     * @param badgeId
     * @return
     * @Description 根据徽章主键查询活徽章轮播图信息。
     */
    List<BadgeCarouselPicture> selectBadgeCarouselPictureByBadgeId(Long badgeId);

    /**
     * @param badgeCarouselPicture
     * @return
     * @Description 插入一条徽章轮播图信息。
     */
    int insertBadgeCarouselPicture(BadgeCarouselPicture badgeCarouselPicture);

    /**
     * @param badgeCarouselPictureList
     * @return
     * @Description 批量插入徽章轮播图信息。
     */
    int insertBadgeCarouselPictureInBatch(List<BadgeCarouselPicture> badgeCarouselPictureList);

    /**
     * @param badgeId
     * @return
     * @Description 根据徽章主键删除活徽章轮播图信息。
     */
    int deleteBadgeCarouselPicture(Long... badgeId);

}

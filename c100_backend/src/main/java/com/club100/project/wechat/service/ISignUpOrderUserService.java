package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.SignUpOrderUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/08 21:27:58
 * @Description: 报名人数服务类。
 */
public interface ISignUpOrderUserService {

    /**
     * 列举报名人数信息。
     *
     * @param signUpOrderUser
     * @return
     */
    List<SignUpOrderUser> selectSignUpOrderUser(SignUpOrderUser signUpOrderUser);

    /**
     * 获得报名人数数量。
     *
     * @param signUpOrderUser
     * @return
     */
    int selectSignUpOrderUserCount(SignUpOrderUser signUpOrderUser);

}

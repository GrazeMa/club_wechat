package com.club100.project.wechat.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.IPizzaGetterService;
import com.club100.project.wechat.service.IPizzaService;
import com.club100.project.wechat.utils.PrimaryKeyIdUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/25 21:41:27
 * @Description: Pizza 审核控制器。
 */
@RequestMapping("/v1/pizza")
@RestController
@Slf4j
public class PizzaController extends BaseController {

    /*
     * 属性中的变量。
     */
    // 小程序 APP_ID。
    @Value("${wechat.app_id}")
    private String APP_ID;
    // 小程序 APP_SECRET。
    @Value("${wechat.app_secret}")
    private String APP_SECRET;

    private final String GRANT_TYPE = "client_credential";
    private final String ENVIRONMENT_ID = "prod-gpkx";

    /*
    All services.
     */
    // Pizza information.
    @Autowired
    private IPizzaService pizzaService;
    // Pizza getter information.
    @Autowired
    private IPizzaGetterService pizzaGetterService;
    @Autowired
    private QiniuService qiniuService;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * @param pizza
     * @return
     * @Description 获取披萨审核列表。
     */
    @GetMapping("/listPizza")
    public TableDataInfo listPizza(Pizza pizza) {

//        startPage();
        List<ReturnPizzaInfo> returnedPizzaInfoList = new ArrayList<>();

        List<Pizza> pizzaList = pizzaService.selectPizzaList(pizza);
//        log.info(" === pizzaList: {}", pizzaList);

        // 分组。
        Map<Long, List<Pizza>> groupByUserIdMap = pizzaList.stream().collect(Collectors.groupingBy(Pizza::getUserId));
//        log.info(" === groupByUserIdMap: {}", groupByUserIdMap);

        // 遍历分组并处理数据。
        for (Map.Entry<Long, List<Pizza>> entryPizza : groupByUserIdMap.entrySet()) {
//            log.info(" === entryPizza: {}", entryPizza);
            Long key = entryPizza.getKey();
            List<Pizza> entryPizzaList = entryPizza.getValue();

            StringBuilder pizzaPictureStringBuilder = new StringBuilder();
            StringBuilder returnedPizzaPictureStringBuilder = new StringBuilder();
            StringBuilder returnedPizzaDownloadUrlStringBuilder = new StringBuilder();
            long pizzaGetterIdLong = 0;
            int stateInt = 0;
            String weChatIdString = "";

            for (Pizza eachPizza : entryPizzaList) {

                String pizzaUrlString = eachPizza.getMarkPictureUrl();

                pizzaGetterIdLong = eachPizza.getPizzaGetterId();
                stateInt = eachPizza.getState();
                weChatIdString = eachPizza.getWeChatId();

                if (pizzaPictureStringBuilder.length() == 0) {
                    pizzaPictureStringBuilder.append("{\"fileid\":\"").append(pizzaUrlString).append("\",\"max_age\":7200}");
                } else {
                    pizzaPictureStringBuilder.append(",{\"fileid\":\"").append(pizzaUrlString).append("\",\"max_age\":7200}");
                }

            }

            returnedPizzaPictureStringBuilder.append("{\"env\":\"prod-gpkxr\",\"file_list\":[").append(pizzaPictureStringBuilder).append("]}");

            String parametersJsonString = returnedPizzaPictureStringBuilder.toString();

            String returnedString = _getPizzaFileUrls(parametersJsonString);

            JSONObject jsonObject = JSONObject.parseObject(returnedString);
            String fileListString = jsonObject.getString("file_list");

            JSONArray jsonArray = JSONArray.parseArray(fileListString);

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                String downloadUrlString = tempJsonObject.getString("download_url");

                if (returnedPizzaDownloadUrlStringBuilder.length() == 0) {
                    returnedPizzaDownloadUrlStringBuilder.append(downloadUrlString);
                } else {
                    returnedPizzaDownloadUrlStringBuilder.append(",").append(downloadUrlString);
                }

            }

            String[] pizzaStringArray = String.valueOf(returnedPizzaDownloadUrlStringBuilder).split(",");

            ReturnPizzaInfo returnPizzaInfo = new ReturnPizzaInfo(pizzaGetterIdLong, stateInt, key, weChatIdString, pizzaStringArray);

            returnedPizzaInfoList.add(returnPizzaInfo);

        }

        Collections.sort(returnedPizzaInfoList,
                (ReturnPizzaInfo returnPizzaInfo1, ReturnPizzaInfo returnPizzaInfo2) -> returnPizzaInfo2.pizzaGetterId.compareTo(returnPizzaInfo1.pizzaGetterId));

        return getDataTable(returnedPizzaInfoList);

    }

    @RequestMapping(value = "/auditPizzaSuccess")
    public int auditPizzaSuccess(@RequestBody Long pizzaGetterId) {

        PizzaGetter updatedPizzaGetterEntity = new PizzaGetter();
        updatedPizzaGetterEntity.setPizzaGetterId(pizzaGetterId);
        updatedPizzaGetterEntity.setState(1);

        return pizzaGetterService.updatePizzaGetter(updatedPizzaGetterEntity);

    }

    @RequestMapping(value = "/auditPizzaFail")
    public int auditPizzaFail(@RequestBody Long pizzaGetterId) {

        PizzaGetter updatedPizzaGetterEntity = new PizzaGetter();
        updatedPizzaGetterEntity.setPizzaGetterId(pizzaGetterId);
        updatedPizzaGetterEntity.setState(2);

        return pizzaGetterService.updatePizzaGetter(updatedPizzaGetterEntity);

    }

    @RequestMapping(value = "/uploadPicture")
    public String uploadPicture(@RequestParam("file") MultipartFile file) throws IOException {

        String uploadFileName = qiniuService.upload(file);

        return "https://images.club100.cn/" + uploadFileName;

    }

    private String _getPizzaFileUrls(String parametersJsonString) {

        // Get access token with AppID and AppSecret of wechat Mini Program.
        String accessTokenString = _GetAccessToken();

        // Batch download files with given fileId(s) and access token.
        String fileUrlsString = _BatchDownloadFiles(accessTokenString, parametersJsonString);

        return fileUrlsString;

    }

    private String _BatchDownloadFiles(String accessTokenString, String parametersJsonString) {

        String postResponseString = "";
        try {
            postResponseString = sendPost(accessTokenString, parametersJsonString, "https://api.weixin.qq.com/tcb/batchdownloadfile?access_token={accessToken}");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return postResponseString;

    }

    private String _GetAccessToken() {

        String accessTokenString = "";
        try {
            String getResponseString = sendGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=" + GRANT_TYPE + "&appid=" + APP_ID + "&secret=" + APP_SECRET);
            JSONObject jsonObject = JSONObject.parseObject(getResponseString);
            accessTokenString = jsonObject.getString("access_token");
        } catch (Exception e) {
            log.error("_GetAccessToken Exception!!!");
            e.printStackTrace();
        }

        return accessTokenString;

    }

    /**
     * @param urlString
     * @return getResponseString
     * @Description GET request.
     */
    private String sendGet(String urlString) {

        return restTemplate.getForObject(urlString, String.class);

    }

    /**
     * @param accessTokenString
     * @param urlString
     * @return postResponseBodyString
     * @Description POST request
     */
    private String sendPost(String accessTokenString, String parametersJsonString, String urlString) {

        // 创建请求头。
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Map<String, String> params = new HashMap<>();
        params.put("accessToken", accessTokenString);

        HttpEntity<String> entity = new HttpEntity<>(parametersJsonString, headers);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlString, entity, String.class, params);

        return responseEntity.getBody();

    }

}

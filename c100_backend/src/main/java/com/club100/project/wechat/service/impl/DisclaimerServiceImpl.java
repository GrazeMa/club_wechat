package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Disclaimer;
import com.club100.project.wechat.mapper.DisclaimerMapper;
import com.club100.project.wechat.service.IDisclaimerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 21:39:09
 * @Description: 免责声明服务实现类。
 */
@Slf4j
@Service
public class DisclaimerServiceImpl implements IDisclaimerService {

    @Autowired
    private DisclaimerMapper disclaimerMapper;

    @Override
    public int insertDisclaimer(Disclaimer disclaimer) {
        return disclaimerMapper.insertDisclaimer(disclaimer);
    }

    @Override
    public Disclaimer selectDisclaimerById(Long disclaimerId) {
        return disclaimerMapper.selectDisclaimerById(disclaimerId);
    }

}

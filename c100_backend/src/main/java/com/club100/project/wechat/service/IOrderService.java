package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Order;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/03 22:47:25
 * @Description: 订单服务类。
 */
public interface IOrderService {

    /**
     * 查询订单信息。
     *
     * @param order
     * @return
     */
    List<Order> selectOrderList(Order order);

}

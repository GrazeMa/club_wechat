package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RoutePicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/08/10 17:02:01
 * @Description: 路线详情图片 Mapper 接口。
 */
public interface RoutePictureMapper {

    List<RoutePicture> selectRoutePicture(RoutePicture routePicture);

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityExt extends BaseEntity implements Serializable {

  private Long id; // Primary Key ID。

  private Integer cityType; // 关联城市类型（详情见 club_city 表）。
  private Integer state; // 状态（1：可用；2：废弃。）。
  private Integer activityDays; // 活动天数。
  private Integer activityFeeType; // 费用类型（1：免费；2：收费。）。
  private Integer activityFrequency; // 活动频率。
  private Integer activityType; // 活动类型（）。
  private Integer limitPeopleNumber; // 人数上限。
  private LocalDateTime activityBeginTime; // 活动开始时间。
  private LocalDateTime activityEndTime; // 活动结束时间。
  private LocalDateTime assembleTime; // 集合时间。
  private LocalDateTime signUpEndTime; // 报名截止时间。
  private Long bikeShopId; // 车店主键。
  private Long leaderId; // 领队主键。
  private String activityLocation; // 活动地点。
  private String activityName; // 活动名称。
  private String activityPictureUrl; // 活动图片。
  private String assembleLocation; // 集合地点。
  private String bikeShopName; // 车店地点。
  private String bikeShopAvatarUrl; // 车店头像
  private String bikeShopPictureUrl; // 车店图片


  public String toJson(ActivityExt activity) {

    // Activity 对象转 JSON。
    return JSON.toJSONString(activity);

  }

}

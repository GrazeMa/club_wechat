package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BadgeConsignee;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/18 16:41:00
 * @Description: 徽章收货人服务类。
 */
public interface IBadgeConsigneeService {

    /**
     * @param badgeConsignee
     * @return
     * @Description 查询徽章收货人信息。
     */
    List<BadgeConsignee> selectBadgeConsigneeList(BadgeConsignee badgeConsignee);

    /**
     * @param badgeConsigneeId
     * @return
     * @Description 通过主键查询一条徽章收货人信息。
     */
    BadgeConsignee selectBadgeConsigneeById(Long badgeConsigneeId);

    /**
     * @param userId
     * @return
     * @Description 通过用户主键查询徽章收货人信息。
     */
    List<BadgeConsignee> selectBadgeConsigneeByUserId(Long userId);

    /**
     * @param userId
     * @return
     * @Description 通过用户主键查询一条徽章收货人信息。
     */
    BadgeConsignee selectOneBadgeConsigneeByUserId(Long userId);

    /**
     * @param badgeConsignee
     * @return
     * @Description 插入（一条）徽章收货人信息。
     */
    int insertBadgeConsignee(BadgeConsignee badgeConsignee);

    /**
     * @param badgeConsignee
     * @return
     * @Description 通过主键修改（一条）徽章收货人信息。
     */
    int updateBadgeConsignee(BadgeConsignee badgeConsignee);

    /**
     * @param badgeConsignee
     * @return
     * @Description 通过用户主键修改（一条）徽章收货人信息。
     */
    int updateBadgeConsigneeByUserId(BadgeConsignee badgeConsignee);

}

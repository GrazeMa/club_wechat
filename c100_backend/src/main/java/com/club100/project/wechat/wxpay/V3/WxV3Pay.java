package com.club100.project.wechat.wxpay.V3;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.club100.common.constant.WxV3Constant;
import com.club100.project.wechat.domain.AccountInfo;
import com.club100.project.wechat.domain.Applyments;
import com.club100.project.wechat.domain.ContactInfo;
import com.club100.project.wechat.domain.SalesSceneInfo;
import com.club100.project.wechat.domain.v3.Amount;
import com.club100.project.wechat.domain.v3.OrderV3;
import com.club100.project.wechat.domain.v3.Payer;
import com.club100.project.wechat.domain.v3.SettleInfo;
import com.club100.project.wechat.wxpay.V3.enums.WxApiType;
import com.club100.project.wechat.wxpay.V3.vo.PlatformCert;
import com.ijpay.core.IJPayHttpResponse;
import com.ijpay.core.enums.RequestMethod;
import com.ijpay.core.kit.AesUtil;
import com.ijpay.core.kit.PayKit;
import com.ijpay.wxpay.WxPayApi;
import com.ijpay.wxpay.enums.WxDomain;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;

public class WxV3Pay {

  public static void main(String[] args) {
    //
    try {
      new WxV3Pay().queryApplyments();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  //下单
  public void order() throws Exception {
    String body = JSON.toJSONString(getOrderV3());
    IJPayHttpResponse response = unifiedV3(RequestMethod.POST,"/v3/pay/partner/transactions/jsapi",body);

    System.out.println(JSON.toJSONString(response));
  }

  public OrderV3 getOrderV3() throws Exception {
    OrderV3 orderV3 = new OrderV3();
    orderV3.setSp_appid("");
    orderV3.setSp_mchid("");
    //进件接口返回
    orderV3.setSub_mchid("");
    //商品描述
    orderV3.setDescription("");
    //商户订单号
    orderV3.setOut_trade_no("");
    orderV3.setNotify_url("https://www.baidu.com");

    //结算信息
    SettleInfo settleInfo = new SettleInfo();
    //是否分帐 true:是 false:否
    settleInfo.setProfit_sharing(true);
    //补差金额
    settleInfo.setSubsidy_amount(10);
    orderV3.setSettle_info(settleInfo);

    //订单金额
    Amount amount = new Amount();
    //订单金额单位分
    amount.setTotal(1000);
    orderV3.setAmount(amount);

    Payer payer = new Payer();
    //用户服务标识
    payer.setSp_openid("");
    orderV3.setPayer(payer);

    return orderV3;
  }

  //通过商户订单号 查询订单信息
  public void queryOrder() throws Exception {
    String url = "api.mch.weixin.qq.com/v3/pay/partner/transactions/out-trade-no/";
    //服务商户号
    String sp_mchid = "666";
    //子商户号
    String sub_mchid = "888";
    //商户订单号
    String out_trade_no = "1235";
    url = url + out_trade_no + "?" + "sp_mchid=" + sp_mchid + "&sub_mchid=" + sub_mchid;

    IJPayHttpResponse response = unifiedV3(RequestMethod.GET,url,"");

    System.out.println(JSON.toJSONString(response));
  }



  //进件申请
  public void applyments() throws Exception {
    String body = JSON.toJSONString(getApplyments());
    IJPayHttpResponse response = unifiedV3(RequestMethod.POST,"/v3/ecommerce/applyments/",body);

    System.out.println(JSON.toJSONString(response));
  }

  //查询进件状态
  public void queryApplyments() throws Exception {
    String applyment_id = "2000002158634812";
    IJPayHttpResponse response = unifiedV3(RequestMethod.GET,"/v3/applyment4sub/applyment/applyment_id/" + applyment_id,"");

    System.out.println(JSON.toJSONString(response));
  }

  public Applyments getApplyments() throws Exception {
    Applyments applyments = new Applyments();
    applyments.setOut_request_no("APPLYMENT_00000000001");
    applyments.setOrganization_type("2401");
    applyments.setNeed_account_info(true);
    applyments.setMerchant_shortname("银河系天蓬车店");

    AccountInfo accountInfo = new AccountInfo();
    accountInfo.setBank_account_type("75");
    accountInfo.setAccount_bank("中国银行");
    //需要加密
    accountInfo.setAccount_name(jiami("胡继伟"));
    accountInfo.setBank_address_code("310104");
    accountInfo.setAccount_number(jiami("6216690800002107419"));
    applyments.setAccount_info(accountInfo);

    ContactInfo contactInfo = new ContactInfo();
    contactInfo.setContact_type("65");
    contactInfo.setContact_name(jiami("胡继伟"));
    contactInfo.setContact_id_card_number(jiami("410381199012166617"));
    contactInfo.setMobile_phone(jiami("13167105382"));
    applyments.setContact_info(contactInfo);

    SalesSceneInfo salesSceneInfo = new SalesSceneInfo();
    salesSceneInfo.setMerchant_shortname("天蓬车店");
    salesSceneInfo.setStore_name("天蓬车店");
    salesSceneInfo.setStore_url("http://www.qq.com");
    applyments.setSales_scene_info(salesSceneInfo);

    return applyments;
  }







  /**
   * 统一请求v3接口
   *
   * @param method    请求类型
   * @param url       请求路径
   * @param body      请求body
   * @return          返回请求结果
   * @throws Exception
   */
  public IJPayHttpResponse unifiedV3(RequestMethod method, String url,String body) throws Exception {
    return WxPayApi.v3(method,WxDomain.CHINA.toString(),url,WxV3Constant.MCH_ID,getSerialNo(),getPlatSerialNo(),WxV3Constant.KeyPath,body);
  }


  private String jiami(String data) throws Exception {
    X509Certificate certificate = PayKit.getCertificate(FileUtil.getInputStream(WxV3Constant.PlatCertPath));
    return PayKit.rsaEncryptOAEP(data,certificate);
  }

  /**
   * 获取/更新证书
   *
   * @return
   * @throws Exception
   */
  public IJPayHttpResponse certificates() throws Exception {

    IJPayHttpResponse response = WxPayApi.v3(RequestMethod.GET, WxDomain.CHINA.toString(), WxApiType.GET_CERTIFICATES.toString(), WxV3Constant.MCH_ID,getSerialNo(),"",WxV3Constant.KeyPath,"");

    System.out.println(JSONUtil.toJsonStr(response));
   if(response.getStatus() == 200){
     PlatformCert platformCert = JSONObject.parseObject(response.getBody(), PlatformCert.class);
     PlatformCert.DataBean.EncryptCertificateBean encryptCertificateBean = platformCert.getData().get(0).getEncrypt_certificate();
     savePlatformCert(encryptCertificateBean.getAssociated_data(),encryptCertificateBean.getNonce(),encryptCertificateBean.getCiphertext(),WxV3Constant.PlatCertPath);
    }
    return response;
  }


  /**
   * 获取api证书编号
   *
   * @return
   */
  public String getSerialNo(){
    X509Certificate certificate = PayKit.getCertificate(FileUtil.getInputStream(WxV3Constant.CertPath));
    String serialNo  = certificate.getSerialNumber().toString(16).toUpperCase();
    System.out.println(serialNo);
    return serialNo;
  }

  /**
   * 获取平台证书编号
   *
   * @return
   */
  public String getPlatSerialNo(){
    X509Certificate certificate = PayKit.getCertificate(FileUtil.getInputStream(WxV3Constant.PlatCertPath));
    String serialNo  = certificate.getSerialNumber().toString(16).toUpperCase();
    System.out.println(serialNo);
    return serialNo;
  }

  /**
   * 证书内容
   *
   * @param associatedData
   * @param nonce
   * @param cipherText
   * @param platformCertPath 平台证书保存路径
   * @return
   */
  private String savePlatformCert(String associatedData, String nonce, String cipherText, String platformCertPath) {
    try {
      AesUtil aesUtil = new AesUtil(WxV3Constant.ApiV3.getBytes(StandardCharsets.UTF_8));
      // 平台证书密文解密
      // encrypt_certificate 中的  associated_data nonce  ciphertext
      String publicKey = aesUtil.decryptToString(
              associatedData.getBytes(StandardCharsets.UTF_8),
              nonce.getBytes(StandardCharsets.UTF_8),
              cipherText
      );
      System.out.println("publicKey:" + publicKey);
      // 保存证书
      FileOutputStream fos = new FileOutputStream(platformCertPath);
      fos.write(publicKey.getBytes());
      fos.close();

      // 获取平台证书序列号
      X509Certificate certificate = PayKit.getCertificate(new ByteArrayInputStream(publicKey.getBytes()));
      System.out.println(certificate.getSerialNumber().toString(16).toUpperCase());

      X509Certificate certificate1 = PayKit.getCertificate(FileUtil.getInputStream(platformCertPath));
      String platSerialNo = certificate1.getSerialNumber().toString(16).toUpperCase();
      System.out.println("platform:" + platSerialNo);

      return certificate.getSerialNumber().toString(16).toUpperCase();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}

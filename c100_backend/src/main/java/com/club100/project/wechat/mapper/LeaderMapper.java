package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Leader;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 16:28:03
 * @Description: 领队 Mapper 接口。
 */
public interface LeaderMapper {

    int insertLeader(Leader leader);

    Leader selectLeaderById(long leaderId);

    List<Leader> selectLeaderDropdownList();

}

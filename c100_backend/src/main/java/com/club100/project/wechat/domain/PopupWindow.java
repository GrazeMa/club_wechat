package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/28 21:19:21
 * @Description: 是否弹出弹窗 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PopupWindow extends BaseEntity implements Serializable {

    private Boolean popFlag;

    public String toJson(PopupWindow popupWindow) {

        // PopupWindow 对象转 JSON。
        return JSON.toJSONString(popupWindow);

    }

}

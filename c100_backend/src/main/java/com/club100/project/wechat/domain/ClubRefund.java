package com.club100.project.wechat.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * club_refund
 * @author 
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClubRefund implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 用户id(club_user表主键)
     */
    private Long userId;

    /**
     * 订单号（club_order表）
     */
    private String outTradeNo;

    /**
     * 退款单号(唯一)
     */
    private String outRefundNo;

    /**
     * 订单总金额(单位元保留两位小数)
     */
    private BigDecimal totalFee;

    /**
     * 退款金额
     */
    private BigDecimal refundFee;

    /**
     * 退款笔数
     */
    private Integer refundCount;

    /**
     * 退款状态(SUCCESS—退款成功,REFUNDCLOSE—退款关闭,PROCESSING—退款处理中,CHANGE—退款异常)
     */
    private String refundStatus;

    /**
     * 退款成功时间
     */
    private String refundSuccessTime;

    /**
     * 返回信息
     */
    private String returnMsg;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态（0：退款中 1：成功 2：失败）
     */
    private Integer status;

    /**
     * 分帐退款状态（1：成功 2：失败）
     */
    private Integer profitRefundStatus;

    /**
     * 分帐退款金额
     */
    private String profitRefundFee;

    private String profitRefundResponse;

    private static final long serialVersionUID = 1L;
}
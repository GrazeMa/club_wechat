package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RouteV2;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 16:49:02
 * @Description: 路线V2服务类。
 */
public interface IRouteV2Service {

    /**
     * 查询路线V2信息（最多两条）。
     *
     * @param routeV2
     * @return
     */
    List<RouteV2> selectRouteV2AtMostTwoList(RouteV2 routeV2);

    /**
     * 查询路线V2信息。
     *
     * @param cityType
     * @param routeDistance1Begin
     * @param routeDistance1End
     * @param routeDistance2Begin
     * @param routeDistance2End
     * @param routeDistance3Begin
     * @param routeDistance3End
     * @param routeDistance4Begin
     * @param routeDistance4End
     * @param routeDistance5Begin
     * @param routeDistance5End
     * @param routeDistance6Begin
     * @param routeDistance6End
     * @param routeTerrain
     * @param routeType
     * @return
     */
    List<RouteV2> selectRouteV2List(Integer cityType, Integer routeDistance1Begin, Integer routeDistance1End, Integer routeDistance2Begin, Integer routeDistance2End, Integer routeDistance3Begin, Integer routeDistance3End, Integer routeDistance4Begin, Integer routeDistance4End, Integer routeDistance5Begin, Integer routeDistance5End, Integer routeDistance6Begin, Integer routeDistance6End, Integer[] routeTerrain, Integer[] routeType);

}

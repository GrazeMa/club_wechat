package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BadgeType;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2021/01/28 16:54:22
 * @Description: 徽章类型服务类。
 */
public interface IBadgeTypeService {

    /**
     * @param badgeType
     * @return
     * @Description 查询徽章类型信息。
     */
    List<BadgeType> selectBadgeTypeList(BadgeType badgeType);

    /**
     * @param badgeTypeId
     * @return
     * @Description 根据主键 ID 查询徽章类型信息。
     */
    BadgeType selectBadgeTypeById(Long badgeTypeId);

    /**
     * @param badgeId
     * @return
     * @Description 根据徽章主键 ID 查询徽章类型信息。
     */
    List<BadgeType> selectBadgeTypeByBadgeId(Long badgeId);

    /**
     * @param badgeType
     * @return
     * @Description 插入一条徽章类型信息。
     */
    int insertBadgeType(BadgeType badgeType);

}

package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/17 09:51:16
 * @Description: 徽章 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Badge extends BaseEntity implements Serializable {

    private Long badgeId; // Primary Key ID。

    private Boolean ifBadgeOnline; // 徽章是否可以出售。
    private Boolean ifUserGetBadge; // 用户是否获得此徽章。
    private Double badgePrice; // 徽章价格。
    private Integer badgeBuyState; // 徽章购买状态（0：敬请期待；1：购买实体徽章；2：购买记录；3：未获得。）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主键 ID。
    private String badgeName; // 徽章名称。
    private String badgeDescription; // 徽章描述。
    private String badgePictureUrl; // 徽章图片链接 URL。
    private String cityName; // 城市名。
    private String badgeAccessories; // 徽章配件。
    private String badgeCarouselPictureUrl; // 徽章轮播图链接 URL。
    private String badgeDesignInspiration; // 徽章设计灵感。
    private String badgeMaterial; // 徽章材质。
    private String badgePigment; // 徽章颜料。
    private String badgeProcess; // 徽章工艺。
    private String badgeSpecifications; // 徽章规格。
    private String badgeTechnology; // 徽章技术。
    private String badgeTexture; // 徽章质感。

    public String toJson(Badge badge) {

        // Badge 对象转 JSON。
        return JSON.toJSONString(badge);

    }

}

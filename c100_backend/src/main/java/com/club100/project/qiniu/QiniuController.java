package com.club100.project.qiniu;

import com.alibaba.fastjson.JSONObject;
import com.club100.framework.web.domain.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @auther: zhangqiang
 * @date: 2019/2/21 11:34
 * @description:
 */
@RequestMapping("qiniu")
@RestController
public class QiniuController {

    @Resource
    private QiniuService qiniuService;

    @GetMapping("upload/token")
    public AjaxResult token() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", qiniuService.getToken());
        return AjaxResult.success(jsonObject);
    }

}

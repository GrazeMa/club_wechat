package com.club100.project.atricle.mapper;

import com.club100.project.atricle.domain.Atricle;
import com.club100.project.atricle.domain.AtricleQueryRequest;
import com.club100.project.atricle.domain.AtricleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @auther: zhangqiang
 * @date: 2020/6/21 15:05
 * @description:
 */
public interface AtricleMapper {

    int insertAtricle(Atricle atricle);

    List<Atricle> selectArticleList(AtricleQueryRequest atricleQueryRequest, Integer preference, Integer videoTime1Begin, Integer videoTime1End, Integer videoTime2Begin, Integer videoTime2End, Integer videoTime3Begin, Integer videoTime3End, Integer videoTime4Begin, Integer videoTime4End, Integer[] videoType);

    List<Atricle> selectAtricleforList(AtricleQueryRequest atricleQueryRequest);

    int delteById(Long... id);

    AtricleVo selectArticleById(Long id);

    int updateArticle(Atricle atricle);

}

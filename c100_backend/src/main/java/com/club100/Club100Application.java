package com.club100;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/15 10:34:35
 * @Description: SpringBoot 启动类。
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) // 作用：排除自动注入数据源的配置（取消数据库配置），一般使用在客户端（消费者）服务中。
@Slf4j
@EnableScheduling
public class Club100Application {

    public static void main(String[] args) {

        /*
         * 设置指定键对值的系统属性
         * setProperty (String prop, String value);
         *
         * 参数：
         * prop - 系统属性的名称。
         * value - 系统属性的值。
         *
         * 返回：
         * 系统属性以前的值，如果没有以前的值，则返回 null。
         *
         * 抛出：
         * SecurityException - 如果安全管理器存在并且其 checkPermission 方法不允许设置指定属性。
         * NullPointerException - 如果 key 或 value 为 null。
         * IllegalArgumentException - 如果 key 为空。
         * 注：这里的 System，系统指的是 JRE (runtime)System，不是指 OS。
         *
         * System.setProperty 相当于一个静态变量，存在内存里。
         * 这样就把第一个参数设置成为系统的全局变量，可以在项目的任何一个地方，通过 System.getProperty("变量"); 来获得。
         */
        System.setProperty("spring.devtools.restart.enabled", "false");

        /*
         * 不显示 Banner 启动。
         * Banner.Mode.OFF：关闭 Banner 显示；
         * Banner.Mode.CONSOLE：控制台输出，默认方式；
         * Banner.Mode.LOG：日志输出方式。
         */
        /*SpringApplication springApplication = new SpringApplication(Club100Application.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);*/

        /*
         * 正常启动。
         */
        SpringApplication.run(Club100Application.class, args);
        log.info("====== SpringBoot Club100Application is success. ======");

    }

}

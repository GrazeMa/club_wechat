package com.club100.framework.web.controller;

import com.club100.common.constant.HttpStatus;
import com.club100.common.utils.DateUtils;
import com.club100.common.utils.ServletUtils;
import com.club100.common.utils.StringUtils;
import com.club100.common.utils.sql.SqlUtil;
import com.club100.framework.security.LoginUser;
import com.club100.framework.security.service.TokenService;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.PageDomain;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.framework.web.page.TableSupport;
import com.club100.project.system.domain.SysUser;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 *
 * @author zhangqiang
 */
public class BaseController {

  // Login user information.
  @Autowired
  private TokenService tokenService;

  protected final Logger logger = LoggerFactory.getLogger(BaseController.class);

  /** 将前台传递过来的日期格式的字符串，自动转化为Date类型 */
  @InitBinder
  public void initBinder(WebDataBinder binder) {
    // Date 类型转换
    binder.registerCustomEditor(
        Date.class,
        new PropertyEditorSupport() {
          @Override
          public void setAsText(String text) {
            setValue(DateUtils.parseDate(text));
          }
        });
  }

  /** 设置请求分页数据 */
  protected void startPage() {
    PageDomain pageDomain = TableSupport.buildPageRequest();
    Integer pageNum = pageDomain.getPageNum();
    Integer pageSize = pageDomain.getPageSize();
    if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
      String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
      PageHelper.startPage(pageNum, pageSize, orderBy);
    }
  }

  /** 响应请求分页数据 */
  @SuppressWarnings({"rawtypes", "unchecked"})
  protected TableDataInfo getDataTable(List<?> list) {
    TableDataInfo rspData = new TableDataInfo();
    rspData.setCode(HttpStatus.SUCCESS);
    rspData.setMsg("查询成功");
    rspData.setRows(list);
    rspData.setTotal(new PageInfo(list).getTotal());
    return rspData;
  }

  protected PageInfo getPageTable(List<?> list) {
    TableDataInfo rspData = new TableDataInfo();
    rspData.setCode(HttpStatus.SUCCESS);
    rspData.setMsg("查询成功");
    rspData.setRows(list);
    rspData.setTotal(new PageInfo(list).getTotal());
    return new PageInfo(list);
  }

  /**
   * 响应返回结果
   *
   * @param rows 影响行数
   * @return 操作结果
   */
  protected AjaxResult toAjax(int rows) {
    return rows > 0 ? AjaxResult.success() : AjaxResult.error();
  }

  /**
   * @return loginSystemUserIdLong
   * @Description Get login system user's Id.
   */
  protected Long P_GetLoginSystemUserId() {

    LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
    SysUser user = loginUser.getUser();
    Long loginSystemUserIdLong = user.getUserId();

    return loginSystemUserIdLong;

  }
}

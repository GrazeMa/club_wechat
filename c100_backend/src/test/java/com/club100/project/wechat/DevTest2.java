package com.club100.project.wechat;

import com.alibaba.fastjson.JSONObject;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.*;
import com.club100.project.wechat.utils.EmptyUtils;
import com.club100.project.wechat.utils.JsonUtils;
import com.club100.project.wechat.utils.ReturnUtils;
import com.club100.project.wechat.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.JDOMException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/8/28 10:24:43
 * @Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DevTest2 {

    @Autowired
    private IActivityService activityService;
    @Autowired
    private ILeaderService leaderService;
    @Autowired
    private IProductService productService;
    @Autowired
    private IRouteCorrectionPictureService routeCorrectionPictureService;
    @Autowired
    private IRouteService routeService;
    @Autowired
    private ISignUpActivityService signUpActivityService;
    @Autowired
    private ISignUpService signUpService;

    @Test
    public void printReturn() {

        System.out.println(ReturnUtils.S_returnJsonInfo(0, "", String.valueOf(2008261202121570104L)));

    }

    @Test
    public void getLatestRouteId() {
        System.out.println("====getLatestRouteId: " + (routeService.getLatestRouteId().getRouteId() + 1L));
    }

//    @Test
//    public void selectActivityList() {
//
//        Activity searchedActivityEntity = new Activity();
//
//        List<Activity> returnedActivitiesList = activityService.selectActivityList(searchedActivityEntity);
//
//        System.out.println("=== selectActivityList: " + JsonUtils.S_listToJsonArray(returnedActivitiesList));
//
//    }

    @Test
    public void activityDetail() {

        Activity searchedActivityEntity = new Activity();
        searchedActivityEntity.setActivityId(1L);

        Activity activity = activityService.selectActivityById(1L);

        System.out.println("=== activityDetail: " + activity.toJson(activity));

    }

    @Test
    public void signUp() {

        SignUp searchedSignUpEntity = new SignUp();
        searchedSignUpEntity.setActivityId(1L);
        searchedSignUpEntity.setUserId(666L);

        SignUp ifEmptySignUpEntity = signUpService.selectSignUp(searchedSignUpEntity);

        if (EmptyUtils.isNotEmpty(ifEmptySignUpEntity))
            System.out.println("=== Grazer_Tao");

//        int insertRowsInt = signUpService.insertSignUp(searchedSignUpEntity);
//        System.out.println("=== insertRowsInt: " + insertRowsInt);
//
//        // Insert failure.
//        if (!Objects.equals(1, insertRowsInt))
//            System.out.println("=== Fail...");
//        // Insert success.
//        System.out.println("=== Success...");

    }

    @Test
    public void listMyActivity() {

        SignUpActivity searchedSignUpActivityEntity = new SignUpActivity();
        searchedSignUpActivityEntity.setUserId(666L);


        //
        List<SignUpActivity> signUpActivitiesList = signUpActivityService.selectSignUpActivity(searchedSignUpActivityEntity);

        System.out.println("=== listMyActivity: " + JsonUtils.S_listToJsonArray(signUpActivitiesList));

    }

    @Test
    public void getLeaderInfo() {

        Leader returnedLeaderEntity = leaderService.selectLeaderById(2L);

        System.out.println("=== getLeaderInfo: " + returnedLeaderEntity.toJson(returnedLeaderEntity));

    }

    @Test
    public void listProducts() {

        Product searchedProductEntity = new Product();
        searchedProductEntity.setActivityId(1L);

        List<Product> returnedProductsList = productService.selectProductList(searchedProductEntity);

        System.out.println("=== listProducts: " + JsonUtils.S_listToJsonArray(returnedProductsList));

    }

    /*@Test
    public void getInsertLastId() {

        RouteCorrectionPicture routeCorrectionPicture = new RouteCorrectionPicture();
        routeCorrectionPicture.setRouteCorrectionId(1234L);
        routeCorrectionPicture.setCorrectionPictureUrl("12341234");
        routeCorrectionPicture.setCorrectionPictureSequence(12);

        int pkId = routeCorrectionPictureService.insertRouteCorrectionPicture(routeCorrectionPicture);
        System.out.println("=== item: " + pkId);
        System.out.println("=== last: " + routeCorrectionPicture.getRouteCorrectionPictureId());

    }*/

    @Test
    public void xml2Json() {

        String xmlStr = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<gpx version=\"1.1\" creator=\"https://www.komoot.de\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n" +
                "  <metadata>\n" +
                "    <name>Road Ride</name>\n" +
                "    <author>\n" +
                "      <link href=\"https://www.komoot.de\">\n" +
                "        <text>komoot</text>\n" +
                "        <type>text/html</type>\n" +
                "      </link>\n" +
                "    </author>\n" +
                "  </metadata>\n" +
                "  <trk>\n" +
                "    <name>Road Ride</name>\n" +
                "    <trkseg>\n" +
                "      <trkpt lat=\"31.133085\" lon=\"121.072116\">\n" +
                "        <ele>4.378656</ele>\n" +
                "        <time>2020-10-13T11:32:47.325Z</time>\n" +
                "      </trkpt>\n" +
                "      <trkpt lat=\"31.131913\" lon=\"121.070032\">\n" +
                "        <ele>3.888500</ele>\n" +
                "        <time>2020-10-13T11:32:47.325Z</time>\n" +
                "      </trkpt>\n" +
                "      <trkpt lat=\"31.131913\" lon=\"121.070032\">\n" +
                "        <ele>3.888500</ele>\n" +
                "        <time>2020-10-13T11:32:47.325Z</time>\n" +
                "      </trkpt>\n" +
                "      <trkpt lat=\"31.131468\" lon=\"121.068251\">\n" +
                "        <ele>3.645312</ele>\n" +
                "        <time>2020-10-13T11:32:47.325Z</time>\n" +
                "      </trkpt>\n" +
                "      <trkpt lat=\"31.131468\" lon=\"121.068251\">\n" +
                "        <ele>3.645312</ele>\n" +
                "        <time>2020-10-13T11:32:47.325Z</time>\n" +
                "      </trkpt>\n" +
                "    </trkseg>\n" +
                "  </trk>\n" +
                "</gpx>";
        try {
            JSONObject jsonObject = XmlUtils.xml2Json(xmlStr);
            System.out.println("jsonObject: " + jsonObject);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

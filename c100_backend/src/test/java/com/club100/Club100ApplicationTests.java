package com.club100;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Auther: Grazer_Ma
 * @Date: 2020/7/22 10:46:55
 * @Description: Club100ApplicationTests 测试类。
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Club100ApplicationTests {

//    @Test
//    void contextLoads() {
//    }

    /*@Test
    public void test() {
        Connection con = null;
        try {
            // 1. 加载驱动（Java6以上版本可以省略）
            Class.forName("com.mysql.cj.jdbc.Driver");
//            Class.forName("com.mysql.jdbc.Driver");
            // 2. 建立连接
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/grazer", "root", "Hzxh921225.1");
            // 3. 创建语句对象
            PreparedStatement ps = con.prepareStatement("insert into tb_user values (default, ?, ?)");
            ps.setString(1, "CC");              // 将SQL语句中第一个占位符换成字符串
            try (InputStream in = new FileInputStream("/Users/grazerma/IdeaProjects/grazerJavaWorld/src/xiaotao.png")) {    // Java 7的TWR
                ps.setBinaryStream(2, in);      // 将SQL语句中第二个占位符换成二进制流
                // 4. 发出SQL语句获得受影响行数
                System.out.println(ps.executeUpdate() == 1 ? "插入成功" : "插入失败");
            } catch (IOException e) {
                System.out.println("读取照片失败!");
            }
        } catch (SQLException | ClassNotFoundException e) {     // Java 7的多异常捕获
            e.printStackTrace();
        } finally { // 释放外部资源的代码都应当放在finally中保证其能够得到执行
            try {
                if (con != null && !con.isClosed()) {
                    con.close();    // 5. 释放数据库连接
                    con = null;     // 指示垃圾回收器可以回收该对象
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }*/

}
